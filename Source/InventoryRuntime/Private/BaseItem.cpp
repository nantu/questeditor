// Copyright (c) 2020 Gil Engel

#include "BaseItem.h"

void ABaseItem::IncreaseStackSize(int32 IncreaseValue)
{
	if (ItemData.IsStackable)
	{
		ItemData.StackSize += IncreaseValue;
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ABaseItem::DecreaseStackSize(int32 DecreaseValue)
{
	if (ItemData.IsStackable)
	{
		ItemData.StackSize -= DecreaseValue;
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

// Sets default values
ABaseItem::ABaseItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

// Called when the game starts or when spawned
void ABaseItem::BeginPlay()
{
	Super::BeginPlay();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

// Called every frame
void ABaseItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

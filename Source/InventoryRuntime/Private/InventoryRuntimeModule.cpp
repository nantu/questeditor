// Copyright (c) 2020 Gil Engel

#include "InventoryRuntimeModule.h"

#include "AssetToolsModule.h"
#include "IAssetTools.h"

#define LOCTEXT_NAMESPACE "FInventoryRuntimeModule"

void FInventoryRuntimeModule::StartupModule()
{
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FInventoryRuntimeModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FInventoryRuntimeModule, InventoryRuntime)

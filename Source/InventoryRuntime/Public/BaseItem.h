// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"


#include "Engine/Texture2D.h"
#include "BaseItem.generated.h"

USTRUCT(BlueprintType)
struct FItemStruct
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName Name;

	UPROPERTY()
	UTexture2D* Icon;

	UPROPERTY(meta = (DisplayName = "IsStackable?"))
	bool IsStackable;

	UPROPERTY()
	int32 StackSize;

	UPROPERTY()
	int32 Index;

	UPROPERTY()
	int32 Weight;

	UPROPERTY()
	ABaseItem* Item;
};


UCLASS()
class INVENTORYRUNTIME_API ABaseItem : public AActor
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintNativeEvent, meta = (DisplayName = "IsUsable?"))
	bool IsUsable() const;

	UFUNCTION(BlueprintNativeEvent, meta = (DisplayName = "IsSellable?"))
	bool IsSellable() const;

	UFUNCTION(BlueprintCallable)
	const FItemStruct& GetItemData() const
	{
		return ItemData;
	}

	UFUNCTION(BlueprintCallable)
	void IncreaseStackSize(int32 IncreaseValue);

	UFUNCTION(BlueprintCallable)
	void DecreaseStackSize(int32 DecreaseValue);

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FItemStruct ItemData;

public:
	// Sets default values for this actor's properties
	ABaseItem();

	/**
	 *
	 *	Default implementation for the native event IsUsable
	 *
	 */
	virtual bool IsUsable_Implementation() const
	{
		return true;
	}

	/**
	 *
	 *	Default implementation for the native event IsSellable
	 *
	 */
	virtual bool IsSellable_Implementation() const
	{
		return true;
	}

protected:
	// Called when the game starts or when spawned
	void BeginPlay() override;

public:
	// Called every frame
	void Tick(float DeltaTime) override;
};

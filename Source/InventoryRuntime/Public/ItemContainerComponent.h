// Copyright (c) 2020 Gil Engel

#pragma once

#include "BaseItem.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ItemContainerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FItemAddedEventSignature, const ABaseItem*, Item);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FItemRemovedEventSignature, const ABaseItem*, Item);

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class INVENTORYRUNTIME_API UItemContainerComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UItemContainerComponent();

	// Called every frame
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	bool ContainsItem(ABaseItem* Item, int32 Amount);

	bool ContainsItem_Implementation(ABaseItem* Item, int32 Amount)
	{
		return false;
	}

	UFUNCTION(BlueprintCallable)
	void BroadcastItemAdded(ABaseItem* Item)
	{
		OnItemAdded.Broadcast(Item);
	}

	UFUNCTION(BlueprintCallable)
	void BroadcastItemRemoved(ABaseItem* Item)
	{
		OnItemRemoved.Broadcast(Item);
	}

protected:
	// Called when the game starts
	void BeginPlay() override;

public:
	UPROPERTY(BlueprintAssignable)
	FItemAddedEventSignature OnItemAdded;

	UPROPERTY(BlueprintAssignable)
	FItemRemovedEventSignature OnItemRemoved;
};

using UnrealBuildTool;
using System.IO;

public class QuestEditor : ModuleRules
{
	public QuestEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		bEnforceIWYU = true;

		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
			}
			);

		string PrivateDirectory = Path.Combine(ModuleDirectory, "Private");
		PrivateIncludePaths.AddRange(
			new string[] {
				PrivateDirectory
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
                "CoreUObject",
                "Engine",
                "UnrealEd",
				"QuestRuntime"
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
                "CoreUObject",
                "Engine",
				"Projects", // IPluginManager
                "Slate",
                "SlateCore",
				"InputCore",
				"EditorStyle",
                "UnrealEd",
                "AssetTools",
                "GraphEditor",
                "ApplicationCore",
                "ToolMenus",
				"BlueprintGraph",
				"PropertyEditor",
				"QuestRuntime",
				"KismetWidgets",
				"PlacementMode"
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}

// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "HitProxies.h"
#include "ComponentVisualizer.h"

class ULocationPolygonComponent;

struct HPolygonVisProxy : public HComponentVisProxy
{
	DECLARE_HIT_PROXY();

	HPolygonVisProxy(const UActorComponent* InComponent) : HComponentVisProxy(InComponent, HPP_Wireframe)
	{
	}
};

/** Proxy for a spline key */
struct HPolygonKeyProxy : public HPolygonVisProxy
{
	DECLARE_HIT_PROXY();

	HPolygonKeyProxy(const UActorComponent* InComponent, int32 InKeyIndex) : HPolygonVisProxy(InComponent), KeyIndex(InKeyIndex)
	{
	}

	int32 KeyIndex;
};

struct HPolygonSegmentProxy : public HPolygonVisProxy
{
	DECLARE_HIT_PROXY();

	HPolygonSegmentProxy(const UActorComponent* InComponent, int32 InSegmentIndex)
		: HPolygonVisProxy(InComponent), SegmentIndex(InSegmentIndex)
	{
	}

	int32 SegmentIndex;
};
/**
 * 
 */
class QUESTEDITOR_API FLocationPolygonVisualizer : public FComponentVisualizer
{
public:
	FLocationPolygonVisualizer();
	virtual ~FLocationPolygonVisualizer();

	//~ Begin FComponentVisualizer Interface
	virtual void OnRegister() override;
	virtual void DrawVisualization(const UActorComponent* Component, const FSceneView* View, FPrimitiveDrawInterface* PDI) override;
	virtual bool VisProxyHandleClick(
		FEditorViewportClient* InViewportClient, HComponentVisProxy* VisProxy, const FViewportClick& Click) override;
	virtual void DrawVisualizationHUD(
		const UActorComponent* Component, const FViewport* Viewport, const FSceneView* View, FCanvas* Canvas) override;
	virtual void EndEditing() override;
	virtual bool GetWidgetLocation(const FEditorViewportClient* ViewportClient, FVector& OutLocation) const override;
	virtual bool GetCustomInputCoordinateSystem(const FEditorViewportClient* ViewportClient, FMatrix& OutMatrix) const override;
	virtual bool HandleInputDelta(FEditorViewportClient* ViewportClient, FViewport* Viewport, FVector& DeltaTranslate,
		FRotator& DeltaRotate, FVector& DeltaScale) override;
	virtual bool HandleInputKey(FEditorViewportClient* ViewportClient, FViewport* Viewport, FKey Key, EInputEvent Event) override;
	//virtual UActorComponent* GetEditedComponent() const override;
	virtual TSharedPtr<SWidget> GenerateContextMenu() const override;
	//~ End FComponentVisualizer Interface

	/** Add menu sections to the context menu */
	virtual void GenerateContextMenuSections(FMenuBuilder& InMenuBuilder) const;


	/** Get the polygon component we are currently editing */
	ULocationPolygonComponent* GetEditedPolygonComponent() const;

protected: 
	/** Property path from the parent actor to the component */
	FComponentPropertyPath PolygonPropertyPath;

	/** commands */
	TSharedPtr<FUICommandList> PolygonComponentVisualizerActions;

	/** Syncs changes made by the visualizer in the actual component */
	void NotifyComponentModified();

	/** Index of key we have selected */
	int32 SelectedKey;

	/** Index of segment we have selected */
	int32 SelectedSegment;

	FVector SelectionPosition;

	void OnDeleteKey();
	bool CanDeleteKey() const;

	#if WITH_EDITOR	
	void OnAddKeyToSegment();
	#endif
	bool CanAddKeyToSegment() const;

	//void OnSelectAllPolygonPoints();
	//bool CanSelectAllPolygonPoints() const;

	FVector MousePosition;
	FVector MouseDirection;

	FPlane MousePlane;

	FBox Box;

	FVector BoxHeightOffsetVector;

private:
	void DrawPolygonSegment(FPrimitiveDrawInterface* PDI, const ULocationPolygonComponent* Polygon, int StartIndex, int EndIndex);
};

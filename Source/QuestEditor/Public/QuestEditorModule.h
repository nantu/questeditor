// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"
#include "Toolkits/AssetEditorToolkit.h"

struct FQuestGraphConnectionDrawingPolicyFactory;
class UGenericContainer;
class IQuestEditor;
class FQuestStyle;
class IAssetTypeActions;

struct FExtensibilityManagers
{
    TSharedPtr<FExtensibilityManager> MenuExtensibilityManager;
    TSharedPtr<FExtensibilityManager> ToolBarExtensibilityManager;

    void Init()
    {
        MenuExtensibilityManager = MakeShareable(new FExtensibilityManager);
        ToolBarExtensibilityManager = MakeShareable(new FExtensibilityManager);
    }

    void Reset()
    {
        MenuExtensibilityManager.Reset();
        ToolBarExtensibilityManager.Reset();
    }
};

class FQuestEditorModule : public IModuleInterface
{
public:
    /** IModuleInterface implementation */
    void StartupModule() override;
    void ShutdownModule() override;

    virtual void RegisterAssetActions();

    TSharedRef<IQuestEditor> CreateQuestEditor(const EToolkitMode::Type Mode, const TSharedPtr< IToolkitHost >& InitToolkitHost, UGenericContainer* Quest);


    /** Gets the extensibility managers for outside entities to extend blueprint editor's menus and toolbars */
    FExtensibilityManagers& GetExtensibilityManagers()
    {
        return QuestExtensibility;
    }

private:
    void RegisterCustomPlacementCategory();

private:
    TArray< TSharedPtr<IAssetTypeActions> > CreatedAssetTypeActions;

    FExtensibilityManagers QuestExtensibility;
    TSharedPtr<FQuestGraphConnectionDrawingPolicyFactory> QuestGraphConnectionFactory;
};

// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Styling/CoreStyle.h"
#include "Framework/Commands/Commands.h"

/*-----------------------------------------------------------------------------
   FQuestSystemGraphEditorCommands
-----------------------------------------------------------------------------*/

class FQuestGraphEditorCommands : public TCommands<FQuestGraphEditorCommands>
{
public:
    /** Constructor */
    FQuestGraphEditorCommands()
        : TCommands<FQuestGraphEditorCommands>("QuestGraphEditorCommands",
              NSLOCTEXT("QuestGraphEditorCommands", "QuestGraphEditor Commands", "QuestGraphEditor Commands"), NAME_None, FCoreStyle::Get().GetStyleSetName())
    {
    }

        /** Initialize commands */
    void RegisterCommands() override;

    /** Compiles the quest */
    TSharedPtr<FUICommandInfo> Compile;
    TSharedPtr<FUICommandInfo> SaveOnCompile_Never;
    TSharedPtr<FUICommandInfo> SaveOnCompile_SuccessOnly;
    TSharedPtr<FUICommandInfo> SaveOnCompile_Always;
    TSharedPtr<FUICommandInfo> JumpToErrorNode;


    /** Breaks the node input/output link */
    TSharedPtr<FUICommandInfo> BreakLink;

    /** Adds an input to the node */
    TSharedPtr<FUICommandInfo> AddOutput;

    /** Removes an input from the node */
    TSharedPtr<FUICommandInfo> DeleteInput;

	/** Removes an input from the node */
	TSharedPtr<FUICommandInfo> DeleteOutput;
};

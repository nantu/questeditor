// Copyright (c) 2020 Gil Engel

#pragma once

#include "KismetPins/SGraphPinExec.h"

class QUESTEDITOR_API SGraphPinOptionalExec : public SGraphPinExec
{
public:
    SLATE_BEGIN_ARGS(SGraphPinOptionalExec) {}
    SLATE_END_ARGS()

    void Construct(const FArguments& InArgs, UEdGraphPin* InPin)
    {
        SGraphPin::Construct(SGraphPin::FArguments(), InPin);

        // Call utility function so inheritors can also call it since arguments can't be passed through
        CachePinIcons();
    }
};

// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GenericGraphNode.h"
#include "GenericGraphNode_Result.generated.h"

/**
 * 
 */
UCLASS()
class QUESTEDITOR_API UGenericGraphNode_Result : public UGenericGraphNode
{
    GENERATED_BODY()

    // UEdGraphNode interface
    FText GetTooltipText() const override;
    bool CanUserDeleteNode() const override { return true; }
    bool CanDuplicateNode() const override { return true; }
    // End of UEdGraphNode interface

    // UQuestGraphNode_Base interface
    void CreateInputPins() override;
};

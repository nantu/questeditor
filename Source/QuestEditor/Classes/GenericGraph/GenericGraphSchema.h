// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "AssetData.h"
#include "EdGraph/EdGraphSchema.h"
#include "Actions/NewCommentAction.h"
#include "Actions/NewNodeAction.h"
#include "Actions/PasteAction.h"


#include "GenericGraphSchema.generated.h"



class UEdGraph;
class UGenericNode;


namespace Utils
{
void GetAllNativeSubclasses(TArray<TAssetSubclassOf<UObject>>& Subclasses, TSubclassOf<UObject> Base, bool bAllowAbstract, const TArray<UClass*>& IgnoreList);
void GetAllBlueprintSubclasses(TArray<TAssetSubclassOf<UObject>>& Subclasses, TSubclassOf<UObject> Base, bool bAllowAbstract, const TArray<UClass*>& IgnoreList);
};	  // namespace Utils


/**
 * 
 */
UCLASS(MinimalAPI)
class UGenericGraphSchema : public UEdGraphSchema
{
	GENERATED_UCLASS_BODY()

	/** Check whether connecting these pins would cause a loop */
	bool ConnectionCausesLoop(const UEdGraphPin* InputPin, const UEdGraphPin* OutputPin) const;

	/** Helper method to add items valid to the palette list */
	QUESTEDITOR_API void GetPaletteActions(FGraphActionMenuBuilder& ActionMenuBuilder, const TArray<UClass*>& Filter) const;

	/** Attempts to connect the output of multiple nodes to the inputs of a single one */
	void TryConnectNodes(const TArray<UGenericNode*>& OutputNodes, UGenericNode* InputNode) const;

	//~ Begin EdGraphSchema Interface
	void GetGraphContextActions(FGraphContextMenuBuilder& ContextMenuBuilder) const override;
	void GetContextMenuActions(class UToolMenu* Menu, class UGraphNodeContextMenuContext* Context) const override;
	FLinearColor GetPinTypeColor(const FEdGraphPinType& PinType) const override;
	const FPinConnectionResponse CanCreateConnection(const UEdGraphPin* A, const UEdGraphPin* B) const override;
	bool ShouldHidePinDefaultValue(UEdGraphPin* Pin) const override;
	
	void BreakNodeLinks(UEdGraphNode& TargetNode) const override;
	void BreakPinLinks(UEdGraphPin& TargetPin, bool bSendsNodeNotifcation) const override;
	int32 GetNodeSelectionCount(const UEdGraph* Graph) const override;
	//~ End EdGraphSchema Interface

protected:
	/** Adds actions for creating every type of Quest */
	void GetAllGenericNodeActions(FGraphActionMenuBuilder& ActionMenuBuilder, bool bShowSelectedActions, const TArray<UClass*>& IgnoreList) const;
	/** Adds action for creating a comment */
	void GetCommentAction(FGraphActionMenuBuilder& ActionMenuBuilder, const UEdGraph* CurrentGraph = nullptr) const;

	virtual void GetPlacableNodes(TArray<TAssetSubclassOf<UObject>>& Nodes, const TArray<UClass*>& IgnoreList) const;

private:
	static FText GetFirstNonAbstractSuperClassDescription(UClass* Class);

	/** A list of all available QuestNode classes */
	static TArray<UClass*> QuestNodeClasses;

	/** Whether the list of QuestNode classes has been populated */
	static bool bQuestNodeClassesInitialized;
};

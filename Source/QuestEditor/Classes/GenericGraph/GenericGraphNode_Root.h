// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GenericGraphNode.h"
#include "GenericGraphNode_Root.generated.h"

/**
 * 
 */
UCLASS()
class QUESTEDITOR_API UGenericGraphNode_Root : public UGenericGraphNode
{
    GENERATED_UCLASS_BODY()

    // UEdGraphNode interface
    FText GetTooltipText() const override;
    FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;
    bool CanUserDeleteNode() const override { return false; }
    bool CanDuplicateNode() const override { return false; }
    // End of UEdGraphNode interface

    // UQuestGraphNode_Base interface
    void CreateInputPins() override;
    void CreateOutputPins() override;
};

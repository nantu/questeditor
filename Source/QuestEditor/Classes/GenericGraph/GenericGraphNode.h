// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "EdGraph/EdGraphNode.h"
#include "GenericGraphNode.generated.h"

class UGenericNode;

/**
 * 
 */
UCLASS(Abstract)
class UGenericGraphNode : public UEdGraphNode
{
	GENERATED_BODY()

public:
	/** Create all of the input pins required */
	virtual void CreateInputPins()
	{
	}

	/** Create all of the output pins required */
	virtual void CreateOutputPins()
	{
	}

	/** 
	 * Checks if a pin can be removed from the node
	 * 
	 * @param Pin		The pin which shall be removed
	 * @return			True if the pin can be removed, false otherwise
	 */
	virtual bool CanRemovePin(const UEdGraphPin* Pin) const;

	/** Remove a specific input pin from this node */
	QUESTEDITOR_API void RemoveInputPin(UEdGraphPin* InGraphPin);

	/** Remove a specific output pin from this node */
	QUESTEDITOR_API void RemoveOutputPin(UEdGraphPin* OutGraphPin);

    /**
	 * Handles inserting the node between the FromPin and what the FromPin was original connected to
	 *
	 * @param FromPin			The pin this node is being spawned from
	 * @param NewLinkPin		The new pin the FromPin will connect to
	 * @param OutNodeList		Any nodes that are modified will get added to this list for notification purposes
	 */
	void InsertNewNode(UEdGraphPin* FromPin, UEdGraphPin* NewLinkPin, TSet<UEdGraphNode*>& OutNodeList);
		
    /** Get a single Input Pin via its index */
	QUESTEDITOR_API class UEdGraphPin* GetInputPin(int32 InputIndex);

	/** Get all of the Input Pins */
	QUESTEDITOR_API TArray<class UEdGraphPin*> GetInputPins() const;

	/** Get all of the Output Pins */
	QUESTEDITOR_API TArray<class UEdGraphPin*> GetOutputPins() const;

    /** Get the current Input Pin count */
	QUESTEDITOR_API int32 GetInputCount() const;
	/** Get the current Output Pin count */
	QUESTEDITOR_API int32 GetOutputCount() const;

    /** Set the QUestNode this represents (also assigns this to the QuestNode in Editor)*/
	QUESTEDITOR_API void SetGenericNode(UGenericNode* InQuestNode);


	void GetNodeContextMenuActions(class UToolMenu* Menu, class UGraphNodeContextMenuContext* Context) const override;

    // UEdGraphNode interface.
	void AllocateDefaultPins() final;
	void ReconstructNode() override;
	void AutowireNewNode(UEdGraphPin* FromPin) override;
	bool CanCreateUnderSpecifiedSchema(const UEdGraphSchema* Schema) const override;
	FString GetDocumentationLink() const override;
	// End of UEdGraphNode interface.

	virtual void AddInputPin();
	virtual void AddOutputPin();

	virtual bool CanAddInputPin()
	{
		return false;
	}

private:
	int32 GetPinsCountForDirection(EEdGraphPinDirection direction) const;
	TArray<class UEdGraphPin*> GetPinsForDirection(EEdGraphPinDirection direction) const;

public:
	/** The GenericNode this represents */
	UPROPERTY(VisibleAnywhere, instanced)
	UGenericNode* GenericNode;
	
};

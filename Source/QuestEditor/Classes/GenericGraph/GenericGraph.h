// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "EdGraph/EdGraph.h"
#include "GenericGraph.generated.h"

/**
 * 
 */
UCLASS()
class UGenericGraph : public UEdGraph
{
    GENERATED_UCLASS_BODY()

    /** Returns the Quest that contains this graph */
    QUESTEDITOR_API class UGenericContainer* GetModel() const;
};

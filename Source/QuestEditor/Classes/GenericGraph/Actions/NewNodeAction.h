// Copyright (c) 2020 Gil Engel

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "AssetData.h"
#include "EdGraph/EdGraphSchema.h"
#include "NewNodeAction.generated.h"

#pragma once

class UGenericNode;

/** Action to add a node to the graph */
USTRUCT()
struct QUESTEDITOR_API FGenericGraphSchemaAction_NewNode : public FEdGraphSchemaAction
{
    GENERATED_USTRUCT_BODY();

    /** Class of node we want to create */
    UPROPERTY()
    class UClass* GenericNodeClass;

    FGenericGraphSchemaAction_NewNode() : FEdGraphSchemaAction(), GenericNodeClass(nullptr)
    {
    }

    FGenericGraphSchemaAction_NewNode(FText InNodeCategory, FText InMenuDesc, FText InToolTip, const int32 InGrouping)
        : FEdGraphSchemaAction(MoveTemp(InNodeCategory), MoveTemp(InMenuDesc), MoveTemp(InToolTip), InGrouping)
        , GenericNodeClass(nullptr)
    {
    }

    //~ Begin FEdGraphSchemaAction Interface
    UEdGraphNode* PerformAction(
        class UEdGraph* ParentGraph, UEdGraphPin* FromPin, const FVector2D Location, bool bSelectNewNode = true) override;
    //~ End FEdGraphSchemaAction Interface

    void ConnectToSelectedNodes(UGenericNode* NewNode, class UEdGraph* ParentGraph) const;
};

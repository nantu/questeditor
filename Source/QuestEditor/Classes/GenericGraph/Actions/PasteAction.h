// Copyright (c) 2020 Gil Engel

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "AssetData.h"
#include "EdGraph/EdGraphSchema.h"
#include "PasteAction.generated.h"

#pragma once

/** Action to paste clipboard contents into the graph */
USTRUCT()
struct QUESTEDITOR_API FGenericGraphSchemaAction_Paste : public FEdGraphSchemaAction
{
    GENERATED_USTRUCT_BODY();

    FGenericGraphSchemaAction_Paste()
        : FEdGraphSchemaAction()
    {}

    FGenericGraphSchemaAction_Paste(FText InNodeCategory, FText InMenuDesc, FText InToolTip, const int32 InGrouping)
        : FEdGraphSchemaAction(MoveTemp(InNodeCategory), MoveTemp(InMenuDesc), MoveTemp(InToolTip), InGrouping)
    {}

    //~ Begin FEdGraphSchemaAction Interface
    UEdGraphNode* PerformAction(class UEdGraph* ParentGraph, UEdGraphPin* FromPin, const FVector2D Location, bool bSelectNewNode = true) override;
    //~ End FEdGraphSchemaAction Interface
};

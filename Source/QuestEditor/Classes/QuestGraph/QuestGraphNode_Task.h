// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "QuestGraphNode.h"
#include "QuestGraphNode_Task.generated.h"

/**
 * 
 */
UCLASS()
class QUESTEDITOR_API UQuestGraphNode_Task : public UQuestGraphNode
{
    GENERATED_BODY()

    // UEdGraphNode interface
    FText GetTooltipText() const override;
    bool CanUserDeleteNode() const override { return true; }
    bool CanDuplicateNode() const override { return true; }

    FLinearColor GetNodeTitleColor() const override;
    // End of UEdGraphNode interface

    void CreateInputPins() override;
	void CreateOutputPins() override;

public:
	static const FName FailureConditionPinName;
	static const FName EnableConditionPinName;
};

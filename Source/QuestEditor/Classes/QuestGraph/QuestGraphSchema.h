// Copyright (c) 2020 Gil Engel

#pragma once

#include "GenericGraph/GenericGraphSchema.h"
#include "QuestGraphSchema.generated.h"

/**
 * 
 */
UCLASS()
class QUESTEDITOR_API UQuestGraphSchema : public UGenericGraphSchema
{
	GENERATED_BODY()

public:
	/** Custom optional exec pin for optional sidequest or alternative solutions */
	static const FName PC_OptionalExec;

	//~ Begin EdGraphSchema Interface
	FLinearColor GetPinTypeColor(const FEdGraphPinType& PinType) const override;
	void CreateDefaultNodesForGraph(UEdGraph& Graph) const override;
	const FPinConnectionResponse CanCreateConnection(const UEdGraphPin* A, const UEdGraphPin* B) const override;
	bool TryCreateConnection(UEdGraphPin* PinA, UEdGraphPin* PinB) const override;
	void BreakPinLinks(UEdGraphPin& TargetPin, bool bSendsNodeNotifcation) const override;
	void GetContextMenuActions(class UToolMenu* Menu, class UGraphNodeContextMenuContext* Context) const override;
	//~ End EdGraphSchema Interface

protected:
	//~ Begin UGenericGraphSchema Interface
	void GetPlacableNodes(TArray<TAssetSubclassOf<UObject>>& Nodes, const TArray<UClass*>& IgnoreList) const override;
	//~ End UGenericGraphSchema Interface

};

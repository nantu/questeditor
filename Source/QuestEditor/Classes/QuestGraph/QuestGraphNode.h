// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "QuestGraph/QuestGraphNode_Base.h"
#include "QuestGraphNode.generated.h"

class UEdGraphPin;
class UQuestNode;

/**
 * 
 */
UCLASS()
class UQuestGraphNode : public UQuestGraphNode_Base
{
    GENERATED_BODY()

public:
    /** Set the QUestNode this represents (also assigns this to the QuestNode in Editor)*/
    QUESTEDITOR_API void SetQuestNode(UQuestNode* InQuestNode);
    /** Fix up the node's owner after being copied */
    QUESTEDITOR_API void PostCopyNode();
    /** Remove a specific input pin from this node and recompile the Quest */
    QUESTEDITOR_API void RemoveOutputPin(UEdGraphPin* InGraphPin);
    /** Estimate the width of this Node from the length of its title */
    QUESTEDITOR_API int32 EstimateNodeWidth() const;

    void AddOutputPin() override;

    
    // UEdGraphNode interface
    void PrepareForCopying() override;
    void GetNodeContextMenuActions(class UToolMenu* Menu, class UGraphNodeContextMenuContext* Context) const override;
    FText GetTooltipText() const override;
    FString GetDocumentationExcerptName() const override;

    virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const;
    // End of UEdGraphNode interface

    
    // UObject interface
    void PostLoad() override;
    void PostEditImport() override;
    void PostDuplicate(bool bDuplicateForPIE) override;
    // End of UObject interface
    


    QUESTEDITOR_API void SwitchPinTypes();

private:
    /** Make sure the QuestNode is owned by the Quest */
    void ResetQuestNodeOwner();
};

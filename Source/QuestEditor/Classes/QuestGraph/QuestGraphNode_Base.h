// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "EdGraph/EdGraphNode.h"
#include "Layout/Visibility.h"
#include "GenericGraph/GenericGraphNode.h"
#include "QuestGraphNode_Base.generated.h"

class UQuestNode;
class UEdGraphPin;
class UEdGraphSchema;

/**
 * 
 */
UCLASS()
class UQuestGraphNode_Base : public UGenericGraphNode
{
    GENERATED_UCLASS_BODY()

	// The condition graph this quest node has
	UPROPERTY()
	class UEdGraph* BoundGraph;

public:
	//~ Begin UObject interface
	void PostLoad() override;
    //~ End UObject interface

    /** Is this the undeletable root node */
    virtual bool IsRootNode() const { return false; }

    /** Is this a result of the quest */
    virtual bool IsResultNode() const { return false;  }

    void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
};

// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "QuestGraphNode.h"
#include "QuestGraphNode_Logic.generated.h"

/**
 * 
 */
UCLASS()
class QUESTEDITOR_API UQuestGraphNode_Logic : public UQuestGraphNode
{
    GENERATED_UCLASS_BODY()

    //~ Begin UEdGraphNode interface
    FText GetTooltipText() const override;
	FLinearColor GetNodeTitleColor() const;
    //~ End of UEdGraphNode interface

    //~ Begin UQuestGraphNode_Base interface
    bool IsRootNode() const override { return false; }
    bool IsResultNode() const override { return true; }
    //~ End of UQuestGraphNode_Base interface

    void CreateInputPins() override;
	void CreateOutputPins() override;

    bool CanAddInputPin() override
	{
		return true;
	}
};

// Copyright (c) 2020 Gil Engel

#pragma once

#include "GenericGraph/GenericGraphSchema.h"
#include "ConditionSchema.generated.h"

/**
 * 
 */
UCLASS()
class QUESTEDITOR_API UConditionSchema : public UGenericGraphSchema
{
	GENERATED_BODY()
	
public:
	//~ Begin EdGraphSchema Interface
	void CreateDefaultNodesForGraph(UEdGraph& Graph) const override;
	void BreakPinLinks(UEdGraphPin& TargetPin, bool bSendsNodeNotifcation) const override;
	//~ End EdGraphSchema Interface


protected:
	void GetPlacableNodes(TArray<TAssetSubclassOf<UObject>>& Nodes, const TArray<UClass*>& IgnoreLis) const override;

};

// Copyright (c) 2020 Gil Engel

#pragma once

#include "Styling/SlateStyle.h"

// how everything looks, fancy stuff
class FQuestStyle
{
public:
	static void Initialize();

	static void Shutdown();

	static TSharedPtr<ISlateStyle> Get()
	{
		return StyleSet;
	}

	/** Gets the style name. */
	static FName GetStyleSetName()
	{
		return TEXT("QuestStyle");
	}

	/** Gets the small property name variant */
	static FName GetSmallProperty(const FName& PropertyName)
	{
		return FName(*(PropertyName.ToString() + TEXT(".Small")));
	}

	/** Get the RelativePath to the DlgSystem Content Dir */
	static FString GetPluginContentPath(const FString& RelativePath)
	{
		return PluginContentRoot / RelativePath;
	}

	/** Get the RelativePath to the Engine Content Dir */
	static FString GetEngineContentPath(const FString& RelativePath)
	{
		return EngineContentRoot / RelativePath;
	}

public:
	static const FName PROPERTY_GraphNodeColorSpill;

	static const FName PROPERTY_QuestClassIcon;
	static const FName PROPERTY_QuestClassThumbnail;
	static const FName PROPERTY_QuestNodeTaskClassIcon;
	static const FName PROPERTY_QuestNodeTaskClassThumbnail;

	static const FName PROPERTY_GraphNodeCircleBox;
	static const FName PROPERTY_ConditionIcon;
	static const FName PROPERTY_EventIcon;
	static const FName PROPERTY_VoiceIcon;
	static const FName PROPERTY_GenericIcon;
	static const FName PROPERTY_QuestionMarkIcon;

	static const FName PROPERTY_ShowPrimarySecondaryEdgesIcon;
	static const FName PROPERTY_ReloadAssetIcon;
	static const FName PROPERTY_OpenAssetIcon;
	static const FName PROPERTY_FindAssetIcon;

	static const FName PROPERTY_SaveAllQuestsIcon;
	static const FName PROPERTY_DeleteAllQuestsTextFilesIcon;
	static const FName PROPERTY_FindQuestIcon;
	static const FName PROPERTY_BrowseQuestIcon;

	static const FName PROPERTY_FindInQuestEditorIcon;
	static const FName PROPERTY_FindInAllQuestEditorIcon;

	static const FName PROPERTY_CommentBubbleOn;

private:
	/** Singleton instance. */
	static TSharedPtr<FSlateStyleSet> StyleSet;

	/** Engine content root. */
	static FString EngineContentRoot;

	/** DlgSystem Content Root */
	static FString PluginContentRoot;
};

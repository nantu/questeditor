// Copyright (c) 2020 Gil Engel

#include "QuestStyle.h"
#include "QuestEditorModule.h"
#include "Interfaces/IPluginManager.h"
#include "Styling/SlateStyle.h"
#include "Styling/SlateStyleRegistry.h"

// Const icon sizes
static const FVector2D Icon16x16(16.0f, 16.0f);
static const FVector2D Icon20x20(20.0f, 20.0f);
static const FVector2D Icon24x24(24.0f, 24.0f);
static const FVector2D Icon40x40(40.0f, 40.0f);
static const FVector2D Icon64x64(64.0f, 64.0f);
static const FVector2D Icon96x96(96.0f, 96.0f);

// Tied with SGraphNodeWithTitle
const FName FQuestStyle::PROPERTY_GraphNodeColorSpill(TEXT("Graph.Node.ColorSpill"));

// Initialize static variables
const FName FQuestStyle::PROPERTY_QuestClassIcon(TEXT("ClassIcon.Quest"));
const FName FQuestStyle::PROPERTY_QuestClassThumbnail(TEXT("ClassThumbnail.Quest"));
const FName FQuestStyle::PROPERTY_QuestNodeTaskClassIcon(TEXT("ClassThumbnail.QuestNodeTask"));
const FName FQuestStyle::PROPERTY_QuestNodeTaskClassThumbnail(TEXT("ClassThumbnail.QuestNodeTask"));
const FName FQuestStyle::PROPERTY_GraphNodeCircleBox(TEXT("QuestEditor.Graph.Node.Circle"));
const FName FQuestStyle::PROPERTY_ConditionIcon(TEXT("QuestEditor.Condition.Icon"));
const FName FQuestStyle::PROPERTY_EventIcon(TEXT("QuestEditor.Event.Icon"));
const FName FQuestStyle::PROPERTY_VoiceIcon(TEXT("QuestEditor.Voice.Icon"));
const FName FQuestStyle::PROPERTY_GenericIcon(TEXT("QuestEditor.Generic.Icon"));
const FName FQuestStyle::PROPERTY_QuestionMarkIcon(TEXT("QuestEditor.QuestionMark.Icon"));

// Tied with FQuestEditorCommands::QuestReloadData
const FName FQuestStyle::PROPERTY_ReloadAssetIcon(TEXT("QuestEditor.QuestReloadData"));

// Tied with FQuestEditorCommands::ToggleShowPrimarySecondaryEdges
const FName FQuestStyle::PROPERTY_ShowPrimarySecondaryEdgesIcon(TEXT("QuestEditor.ToggleShowPrimarySecondaryEdges"));
const FName FQuestStyle::PROPERTY_OpenAssetIcon(TEXT("QuestEditor.OpenAsset.Icon"));
const FName FQuestStyle::PROPERTY_FindAssetIcon(TEXT("QuestEditor.FindAsset.Icon"));

// Tied with FQuestEditorCommands::SaveAllQuests
const FName FQuestStyle::PROPERTY_SaveAllQuestsIcon(TEXT("QuestEditor.SaveAllQuests"));
const FName FQuestStyle::PROPERTY_DeleteAllQuestsTextFilesIcon(TEXT("QuestEditor.DeleteAllQuestsTextFiles"));
const FName FQuestStyle::PROPERTY_FindQuestIcon(TEXT("QuestEditor.FindQuest.Icon"));
const FName FQuestStyle::PROPERTY_BrowseQuestIcon(TEXT("QuestEditor.BrowseQuest.Icon"));

// Tied with FQuestEditorCommands::FindInQuest
const FName FQuestStyle::PROPERTY_FindInQuestEditorIcon(TEXT("QuestEditor.FindInQuest"));

// Tied with FQuestEditorCommands::FindInAllQuests
const FName FQuestStyle::PROPERTY_FindInAllQuestEditorIcon(TEXT("QuestEditor.FindInAllQuests"));

const FName FQuestStyle::PROPERTY_CommentBubbleOn(TEXT("QuestEditor.CommentBubbleOn"));

// The private ones
TSharedPtr<FSlateStyleSet> FQuestStyle::StyleSet = nullptr;
FString FQuestStyle::EngineContentRoot = FString();
FString FQuestStyle::PluginContentRoot = FString();

void FQuestStyle::Initialize()
{
	// Only register once
	if (StyleSet.IsValid())
	{
		return;
	}

	StyleSet = MakeShared<FSlateStyleSet>(GetStyleSetName());
	EngineContentRoot = FPaths::EngineContentDir() / TEXT("Editor/Slate");
	TSharedPtr<IPlugin> CurrentPlugin = IPluginManager::Get().FindPlugin("QuestSystem");
	if (CurrentPlugin.IsValid())
	{
		// Replaces the Engine Content Root (Engine/Editor/Slate) with the plugin content root
		StyleSet->SetContentRoot(CurrentPlugin->GetContentDir());
		PluginContentRoot = CurrentPlugin->GetContentDir();
	}
	else
	{
		return;
	}

	StyleSet->Set(PROPERTY_GraphNodeColorSpill,
		new FSlateBoxBrush(GetPluginContentPath("UI/RegularNode_color_spill.png"), FMargin(8.0f / 64.0f)));

	StyleSet->Set(PROPERTY_QuestClassIcon, new FSlateImageBrush(GetPluginContentPath("Icons/Quest_16x.png"), Icon16x16));
	StyleSet->Set(PROPERTY_QuestClassThumbnail, new FSlateImageBrush(GetPluginContentPath("Icons/Quest_64x.png"), Icon64x64));

	StyleSet->Set(PROPERTY_QuestNodeTaskClassIcon, new FSlateImageBrush(GetPluginContentPath("Icons/Quest_16x.png"), Icon16x16));
	StyleSet->Set(PROPERTY_QuestNodeTaskClassThumbnail, new FSlateImageBrush(GetPluginContentPath("Icons/Quest_64x.png"), Icon64x64));


	StyleSet->Set(PROPERTY_ReloadAssetIcon,
		new FSlateImageBrush(GetEngineContentPath("Icons/icon_Cascade_RestartInLevel_40x.png"), Icon40x40));
	StyleSet->Set(GetSmallProperty(PROPERTY_ReloadAssetIcon),
		new FSlateImageBrush(GetEngineContentPath("Icons/icon_Refresh_16x.png"), Icon16x16));

	StyleSet->Set(PROPERTY_ShowPrimarySecondaryEdgesIcon,
		new FSlateImageBrush(GetPluginContentPath("Icons/Quest_ShowPrimarySecondaryEdges_40x.png"), Icon40x40));
	StyleSet->Set(GetSmallProperty(PROPERTY_ShowPrimarySecondaryEdgesIcon),
		new FSlateImageBrush(GetPluginContentPath("Icons/Quest_ShowPrimarySecondaryEdges_40x.png"), Icon16x16));

	StyleSet->Set(PROPERTY_FindInQuestEditorIcon,
		new FSlateImageBrush(GetEngineContentPath("Icons/icon_Blueprint_Find_40px.png"), Icon40x40));
	StyleSet->Set(GetSmallProperty(PROPERTY_FindInQuestEditorIcon),
		new FSlateImageBrush(GetEngineContentPath("Icons/icon_Blueprint_Find_40px.png"), Icon20x20));

	StyleSet->Set(PROPERTY_FindInAllQuestEditorIcon,
		new FSlateImageBrush(GetEngineContentPath("Icons/icon_FindInAnyBlueprint_40px.png"), Icon40x40));
	StyleSet->Set(GetSmallProperty(PROPERTY_FindInAllQuestEditorIcon),
		new FSlateImageBrush(GetEngineContentPath("Icons/icon_FindInAnyBlueprint_40px.png"), Icon20x20));

	// Level Editor Save All Quests
	StyleSet->Set(
		PROPERTY_SaveAllQuestsIcon, new FSlateImageBrush(GetEngineContentPath("Icons/icon_file_saveall_40x.png"), Icon40x40));
	StyleSet->Set(PROPERTY_DeleteAllQuestsTextFilesIcon,
		new FSlateImageBrush(GetEngineContentPath("Icons/Edit/icon_Edit_Delete_40x.png"), Icon40x40));

	StyleSet->Set(
		PROPERTY_FindQuestIcon, new FSlateImageBrush(GetEngineContentPath("Icons/icon_Genericfinder_16x.png"), Icon16x16));
	StyleSet->Set(PROPERTY_BrowseQuestIcon,
		new FSlateImageBrush(GetEngineContentPath("Icons/icon_tab_ContentBrowser_16x.png"), Icon16x16));

	// Set common used properties
	StyleSet->Set(PROPERTY_ConditionIcon, new FSlateImageBrush(GetPluginContentPath("Icons/Condition_96x.png"), Icon96x96));
	StyleSet->Set(PROPERTY_EventIcon, new FSlateImageBrush(GetPluginContentPath("Icons/Event_96x.png"), Icon96x96));
	StyleSet->Set(PROPERTY_VoiceIcon, new FSlateImageBrush(GetPluginContentPath("Icons/Speaker_96x.png"), Icon96x96));
	StyleSet->Set(PROPERTY_GenericIcon, new FSlateImageBrush(GetPluginContentPath("Icons/Generic_96x.png"), Icon96x96));
	StyleSet->Set(PROPERTY_QuestionMarkIcon, new FSlateImageBrush(GetPluginContentPath("Icons/QuestionMark_16x.png"), Icon16x16));
	StyleSet->Set(PROPERTY_OpenAssetIcon, new FSlateImageBrush(GetEngineContentPath("Icons/icon_asset_open_16px.png"), Icon16x16));
	StyleSet->Set(
		PROPERTY_FindAssetIcon, new FSlateImageBrush(GetEngineContentPath("Icons/icon_Genericfinder_16x.png"), Icon16x16));
	StyleSet->Set(PROPERTY_GraphNodeCircleBox,
		new FSlateBoxBrush(GetEngineContentPath("BehaviorTree/IndexCircle.png"), Icon20x20, FMargin(8.0f / 20.0f)));
	StyleSet->Set(
		PROPERTY_CommentBubbleOn, new FSlateImageBrush(GetEngineContentPath("Icons/icon_Blueprint_CommentBubbleOn_16x.png"),
									  Icon16x16, FLinearColor(1.f, 1.f, 1.f, 1.f)));

	// Register the current style
	FSlateStyleRegistry::RegisterSlateStyle(*StyleSet.Get());
}

void FQuestStyle::Shutdown()
{
	// unregister the style
	if (!StyleSet.IsValid())
	{
		return;
	}

	FSlateStyleRegistry::UnRegisterSlateStyle(*StyleSet.Get());
	ensure(StyleSet.IsUnique());
	StyleSet.Reset();
}

// Copyright (c) 2020 Gil Engel

#pragma once

#include "IQuestEditor.h"
#include "Misc/NotifyHook.h"
#include "EditorUndoClient.h"

#include "Containers/UnrealString.h"    // for FString
#include "HAL/Platform.h"               // for TEXT, int32
#include "Internationalization/Text.h"  // for FText
#include "Math/Color.h"                 // for FLinearColor
#include "Math/Vector2D.h"              // for FVector2D
#include "Toolkits/IToolkit.h"          // for Type
#include "Types/SlateEnums.h"           // for Type
#include "UObject/NameTypes.h"          // for FName
#include "UObject/UnrealType.h"         // for FPropertyChangedEvent, UProperty (ptr only)
#include "Templates/SharedPointer.h"    // for TSharedRef
#include "UObject/GCObject.h"
class UEdGraphNode;
class UGenericContainer;
class UObject;




/*-----------------------------------------------------------------------------
   FGenericEditor
-----------------------------------------------------------------------------*/

class FGenericEditor : public IQuestEditor, public FGCObject, public FNotifyHook, public FEditorUndoClient
{
public:
    FGenericEditor();

    void RegisterTabSpawners(const TSharedRef<class FTabManager>& TabManager) override;
    void UnregisterTabSpawners(const TSharedRef<class FTabManager>& TabManager) override;

    /** Destructor */
    virtual ~FGenericEditor();

    /** Edits the specified Quest object */
    void InitGenericEditor(const EToolkitMode::Type Mode, const TSharedPtr< class IToolkitHost >& InitToolkitHost, UObject* ObjectToEdit);

    /** Let the editor jump to the specific node and moves it into the users focus */
    void JumpToNode(const UEdGraphNode* Node);

    /** FGCObject interface */
	virtual void AddReferencedObjects(FReferenceCollector& Collector) override;

    /** IQuestEditor interface */
    UGenericContainer* GetModel() const override;
    void SetSelection(TArray<UObject*> SelectedObjects) override;
    bool GetBoundsForSelectedNodes(class FSlateRect& Rect, float Padding) override;
    int32 GetNumberOfSelectedNodes() const override;
    TSet<UObject*> GetSelectedNodes() const override;

    /** IToolkit interface */
    FName GetToolkitFName() const override;
    FText GetBaseToolkitName() const override;
    FString GetWorldCentricTabPrefix() const override;
    FLinearColor GetWorldCentricTabColorScale() const override;

    FString GetDocumentationLink() const override
    {
        return FString(TEXT("Engine/Audio/Quests/Editor"));
    }

    bool GetIsContextSensitive() const
	{
		return IsContextSensitive;
    }

    void SetIsContextSensitive(bool InIsContextSensitive);

    //~ Begin FEditorUndoClient Interface
    void PostUndo(bool bSuccess) override;
    void PostRedo(bool bSuccess) override { PostUndo(bSuccess); }
    // End of FEditorUndoClient

    bool CanDeleteInput() const;

    void DeleteInput();

    void DeleteOutput();

    bool CanDeleteOutput() const;

    void CompileQuest();

    bool CanCompileQuest() const;

private:
    TSharedRef<SDockTab> SpawnTab_GraphCanvas(const FSpawnTabArgs& Args);
    TSharedRef<SDockTab> SpawnTab_Properties(const FSpawnTabArgs& Args);
    TSharedRef<SDockTab> SpawnTab_Palette(const FSpawnTabArgs& Args);
	//TSharedRef<SDockTab> SpawnTab_Condition(const FSpawnTabArgs& Args);

    FActionMenuContent OnCreateGraphActionMenu(UEdGraph* InGraph, const FVector2D& InNodePosition,
		const TArray<UEdGraphPin*>& InDraggedPins, bool bAutoExpand, SGraphEditor::FActionMenuClosed InOnMenuClosed);

protected:
    /** Called when the selection changes in the GraphEditor */
    void OnSelectedNodesChanged(const TSet<class UObject*>& NewSelection);

    /**
     * Called when a node's title is committed for a rename
     *
     * @param	NewText				New title text
     * @param	CommitInfo			How text was committed
     * @param	NodeBeingChanged	The node being changed
     */
    void OnNodeTitleCommitted(const FText& NewText, ETextCommit::Type CommitInfo, UEdGraphNode* NodeBeingChanged);

    /** Select every node in the graph */
    void SelectAllNodes();
    /** Whether we can select every node */
    bool CanSelectAllNodes() const;

    /** Delete the currently selected nodes */
    void DeleteSelectedNodes();
    /** Whether we are able to delete the currently selected nodes */
    bool CanDeleteNodes() const;
    /** Delete only the currently selected nodes that can be duplicated */
    void DeleteSelectedDuplicatableNodes();

    /** Cut the currently selected nodes */
    void CutSelectedNodes();
    /** Whether we are able to cut the currently selected nodes */
    bool CanCutNodes() const;

    /** Copy the currently selected nodes */
    void CopySelectedNodes();
    /** Whether we are able to copy the currently selected nodes */
    bool CanCopyNodes() const;

    /** Paste the contents of the clipboard */
    void PasteNodes();

    /** Paste the contents of the clipboard at a specific location */
    void PasteNodesHere(const FVector2D& Location) override;
    /** Whether we are able to paste the contents of the clipboard */
    bool CanPasteNodes() const override;

    /** Duplicate the currently selected nodes */
    void DuplicateNodes();
    /** Whether we are able to duplicate the currently selected nodes */
    bool CanDuplicateNodes() const;

    /** Called to undo the last action */
    void UndoGraphAction();

    /** Called to redo the last undone action */
    void RedoGraphAction();

    void ExtendToolbar();

private:
    /** FNotifyHook interface */
    void NotifyPostChange(const FPropertyChangedEvent& PropertyChangedEvent, FProperty* PropertyThatChanged) override;

    /** Creates all internal widgets for the tabs to point at */
    void CreateInternalWidgets();

    /** Binds new graph commands to delegates */
    void BindGraphCommands();

    /** Create new graph editor widget */
    TSharedRef<SGraphEditor> CreateGraphEditorWidget();

    /** Create new condition graph editor widget */
	TSharedRef<SGraphEditor> CreateConditionGraphEditorWidget();


protected:
    /** The toolbar builder class */
    TSharedPtr<class FGenericEditorToolbar> Toolbar;

private:
	bool IsContextSensitive;

    /** The Quest asset being inspected */
    UGenericContainer* Model;

    /** List of open tool panels; used to ensure only one exists at any one time */
    TMap< FName, TWeakPtr<SDockableTab> > SpawnedToolPanels;

    /** New Graph Editor */
    TSharedPtr<SGraphEditor> QuestGraphEditor;

    /** Condition Graph Editor */
	TSharedPtr<SGraphEditor> ConditionGraphEditor;

    /** Properties tab */
    TSharedPtr<class IDetailsView> QuestProperties;

    /** Palette of Quest Node types */
    TSharedPtr<class SQuestPalette> Palette;

    /** Command list for this editor */
    TSharedPtr<FUICommandList> GraphEditorCommands;

    TArray<UObject*> Selection;

    UQuestNode* ParentConditionNode;

    /**	The tab ids for all the tabs used */
    static const FName GraphCanvasTabId;
    static const FName PropertiesTabId;
    static const FName PaletteTabId;
};

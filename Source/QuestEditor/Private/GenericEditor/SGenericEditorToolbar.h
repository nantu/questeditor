// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Misc/Attribute.h"
#include "Widgets/SWidget.h"
#include "Textures/SlateIcon.h"
#include "EditorStyleSet.h"
#include "Framework/Commands/Commands.h"
#include "WorkflowOrientedApp/SModeWidget.h"

class FGenericEditor;
class FExtender;
class FMenuBuilder;
class FToolBarBuilder;

class QUESTEDITOR_API FGenericEditorToolbar : public TSharedFromThis<FGenericEditorToolbar>
{
public:
    explicit FGenericEditorToolbar(TSharedPtr<FGenericEditor> InQuestEditor) : QuestEditor(InQuestEditor)
    {
    }

    void AddCompileToolbar(TSharedPtr<FExtender> Extender);
    void AddNewToolbar(TSharedPtr<FExtender> Extender);

    /** Returns the current status icon for the quest being edited */
    FSlateIcon GetStatusImage() const;

    /** Returns the current status as text for the quest being edited */
    FText GetStatusTooltip() const;

    /** Helper function for generating the buttons in the toolbar, reused by merge and diff tools */
    static TArray<TSharedPtr<class SWidget> > GenerateToolbarWidgets(
    const class UGenericContainer* QuestObj, TAttribute<FName> ActiveModeGetter, FOnModeChangeRequested ActiveModeSetter);

private:
    void FillCompileToolbar(FToolBarBuilder& ToolbarBuilder);
    void FillNewToolbar(FToolBarBuilder& ToolbarBuilder);

protected:
    /** Pointer back to the quest editor tool that owns us */
    TWeakPtr<FGenericEditor> QuestEditor;
};

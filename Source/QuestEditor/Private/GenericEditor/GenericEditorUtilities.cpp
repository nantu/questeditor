// Copyright (c) 2020 Gil Engel

#include "GenericEditorUtilities.h"
#include "GenericGraph/GenericGraph.h"
#include "GraphEditor.h"
#include "IQuestEditor.h"
#include "Quest.h"
#include "QuestGraph/QuestGraphNode.h"
#include "Toolkits/ToolkitManager.h"


/*-----------------------------------------------------------------------------
    FQuestEditorUtilities implementation.
-----------------------------------------------------------------------------*/

bool FQuestEditorUtilities::CanPasteNodes(const class UEdGraph* Graph)
{
    bool bCanPaste = false;
    TSharedPtr<class IQuestEditor> QuestEditor = GetIQuestEditorForObject(Graph);
    if (QuestEditor.IsValid())
    {
        bCanPaste = QuestEditor->CanPasteNodes();
    }

    return bCanPaste;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FQuestEditorUtilities::PasteNodesHere(class UEdGraph* Graph, const FVector2D& Location)
{
    TSharedPtr<class IQuestEditor> QuestEditor = GetIQuestEditorForObject(Graph);
    if (QuestEditor.IsValid())
    {
        QuestEditor->PasteNodesHere(Location);
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FQuestEditorUtilities::GetBoundsForSelectedNodes(const UEdGraph* Graph, class FSlateRect& Rect, float Padding)
{
    TSharedPtr<class IQuestEditor> QuestEditor = GetIQuestEditorForObject(Graph);
    if (QuestEditor.IsValid())
    {
        return QuestEditor->GetBoundsForSelectedNodes(Rect, Padding);
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

int32 FQuestEditorUtilities::GetNumberOfSelectedNodes(const UEdGraph* Graph)
{
    TSharedPtr<class IQuestEditor> QuestEditor = GetIQuestEditorForObject(Graph);
    if (QuestEditor.IsValid())
    {
        return QuestEditor->GetNumberOfSelectedNodes();
    }

    return 0;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TSet<UObject*> FQuestEditorUtilities::GetSelectedNodes(const UEdGraph* Graph)
{
    TSharedPtr<class IQuestEditor> QuestEditor = GetIQuestEditorForObject(Graph);
    if (QuestEditor.IsValid())
    {
        return QuestEditor->GetSelectedNodes();
    }

    return TSet<UObject*>();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TSharedPtr<class IQuestEditor> FQuestEditorUtilities::GetIQuestEditorForObject(const UObject* ObjectToFocusOn)
{
    check(ObjectToFocusOn);

    // Find the associated Quest
    auto QuestGraph = CastChecked<const UGenericGraph>(ObjectToFocusOn);
	auto Quest = QuestGraph->GetModel();

    TSharedPtr<IQuestEditor> QuestEditor;
    if (Quest != nullptr)
    {
        TSharedPtr< IToolkit > FoundAssetEditor = FToolkitManager::Get().FindEditorForAsset(Quest);
        if (FoundAssetEditor.IsValid())
        {
            QuestEditor = StaticCastSharedPtr<IQuestEditor>(FoundAssetEditor);
        }
    }

    return QuestEditor;
}

// Copyright (c) 2020 Gil Engel

#include "QuestEditorModule.h"
#include "AssetRegistryModule.h"
#include "AssetToolsModule.h"
#include "ConditionEditor/Nodes/ConditionGraphNodeFactory.h"
#include "EditorModeRegistry.h"
#include "EditorModes.h"
#include "GenericEditor.h"
#include "IAssetTools.h"
#include "QuestEditor/DetailsPanel/QuestDetailsPanel.h"
#include "QuestEditor/DetailsPanel/QuestNodeDetailsPanel.h"
#include "QuestEditor/Graph/Actions/AssetTypeActions_QuestGraph.h"
#include "QuestEditor/AssetTypeActions_QuestNodeTask.h"
#include "QuestEditor/Graph/QuestGraphConnectionDrawingPolicy.h"
#include "QuestEditor/Graph/QuestGraphPanelPinFactory.h"
#include "QuestEditor/Nodes/QuestGraphNodeFactory.h"
#include "QuestStyle.h"
#include "Styling/SlateStyle.h"
#include "Styling/SlateStyleRegistry.h"
#include "UnrealEd.h"
#include "LocationPolygonVisualizer.h"
#include "LocationPolygonComponent.h"
#include "IPlacementModeModule.h"

#include "LocationBoxTrigger.h"
#include "LocationPolygonTrigger.h"

#define LOCTEXT_NAMESPACE "FQuestEditorModule"

/*-----------------------------------------------------------------------------
    FQuestEditorModule implementation.
-----------------------------------------------------------------------------*/

TSharedPtr<FQuestGraphNodeFactory> GraphPanelNodeFactory_QuestGraph;
TSharedPtr<FQuestGraphPanelPinFactory> GraphPanelPinFactory_QuestGraph;

TSharedPtr<FConditionGraphNodeFactory> GraphPanelNodeFactory_ConditionGraph;

void FQuestEditorModule::StartupModule()
{
    QuestExtensibility.Init();

    // Register the quest graph connection policy with the graph editor
    QuestGraphConnectionFactory = MakeShared<FQuestGraphConnectionDrawingPolicyFactory>();
    FEdGraphUtilities::RegisterVisualPinConnectionFactory(QuestGraphConnectionFactory);

    GraphPanelNodeFactory_QuestGraph = MakeShared<FQuestGraphNodeFactory>();
    FEdGraphUtilities::RegisterVisualNodeFactory(GraphPanelNodeFactory_QuestGraph);

    GraphPanelNodeFactory_ConditionGraph = MakeShared<FConditionGraphNodeFactory>();
	FEdGraphUtilities::RegisterVisualNodeFactory(GraphPanelNodeFactory_ConditionGraph);

    GraphPanelPinFactory_QuestGraph = MakeShared<FQuestGraphPanelPinFactory>();
    FEdGraphUtilities::RegisterVisualPinFactory(GraphPanelPinFactory_QuestGraph);

    // Register detail customizations
    {
        auto& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");

        PropertyModule.RegisterCustomClassLayout(
			"Quest", FOnGetDetailCustomizationInstance::CreateStatic(&QuestDetailsPanel::MakeInstance));

        PropertyModule.NotifyCustomizationModuleChanged();
    }

    RegisterAssetActions();

    FQuestStyle::Initialize();

    RegisterCustomPlacementCategory();

	if (GUnrealEd)
	{
		// Make a new instance of the visualizer
		TSharedPtr<FLocationPolygonVisualizer> Visualizer = MakeShareable(new FLocationPolygonVisualizer());

		// Register it to our specific component class
		GUnrealEd->RegisterComponentVisualizer(ULocationPolygonComponent::StaticClass()->GetFName(), Visualizer);
		Visualizer->OnRegister();
	}

}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FQuestEditorModule::ShutdownModule()
{
    QuestExtensibility.Reset();

    if (QuestGraphConnectionFactory.IsValid())
    {
        FEdGraphUtilities::UnregisterVisualPinConnectionFactory(QuestGraphConnectionFactory);
    }

    // Unregister all the asset types that we registered
    if (FModuleManager::Get().IsModuleLoaded("AssetTools"))
    {
        IAssetTools& AssetTools = FModuleManager::GetModuleChecked<FAssetToolsModule>("AssetTools").Get();
        for (int32 Index = 0; Index < CreatedAssetTypeActions.Num(); ++Index)
        {
            AssetTools.UnregisterAssetTypeActions(CreatedAssetTypeActions[Index].ToSharedRef());
        }
    }

    if (GraphPanelNodeFactory_QuestGraph.IsValid())
    {
        FEdGraphUtilities::UnregisterVisualNodeFactory(GraphPanelNodeFactory_QuestGraph);
        GraphPanelNodeFactory_QuestGraph.Reset();
    }

    if (FModuleManager::Get().IsModuleLoaded("PropertyEditor"))
    {
        auto& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");

        PropertyModule.UnregisterCustomClassLayout("QuestNodeTask");
    }

    FQuestStyle::Shutdown();

    if (GUnrealEd)
	{
		// Unregister when the module shuts down
		GUnrealEd->UnregisterComponentVisualizer(ULocationPolygonComponent::StaticClass()->GetFName());
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FQuestEditorModule::RegisterCustomPlacementCategory()
{
	// Register new custom category for all location actors
	IPlacementModeModule& PlacementModeModule = IPlacementModeModule::Get();

	const FName Name(TEXT("Quest"));
	PlacementModeModule.RegisterPlacementCategory(FPlacementCategoryInfo(
		NSLOCTEXT("PlacementMode", "Quest", "Quest"), Name, TEXT("Quest"), TNumericLimits<int32>::Max(), false));

    // Add the actors to the category
	PlacementModeModule.RegisterPlaceableItem(
		Name, MakeShareable(new FPlaceableItem(nullptr, FAssetData(ALocationBoxTrigger::StaticClass()))));
	PlacementModeModule.RegisterPlaceableItem(
		Name, MakeShareable(new FPlaceableItem(nullptr, FAssetData(ALocationPolygonTrigger::StaticClass()))));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TSharedRef<IQuestEditor> FQuestEditorModule::CreateQuestEditor(const EToolkitMode::Type Mode, const TSharedPtr< IToolkitHost >& InitToolkitHost, UGenericContainer* Quest)
{
	TSharedRef<FGenericEditor> NewQuestEditor(new FGenericEditor());
    NewQuestEditor->InitGenericEditor(Mode, InitToolkitHost, Quest);
    return NewQuestEditor;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FQuestEditorModule::RegisterAssetActions()
{
    // Register the quest system with new context menu
    IAssetTools &AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();
    QuestAssetCategory = AssetTools.RegisterAdvancedAssetCategory(FName(TEXT("Quest")), LOCTEXT("Quest", "Quest"));
    {
        AssetTools.RegisterAssetTypeActions(MakeShareable(new FAssetTypeActions_QuestGraph));
		AssetTools.RegisterAssetTypeActions(MakeShareable(new FAssetTypeActions_QuestNodeTask));
    }
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FQuestEditorModule, QuestEditor)

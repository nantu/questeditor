// Copyright (c) 2020 Gil Engel

#include "SGenericEditorToolbar.h"
#include "GenericEditor.h"
#include "QuestGraphEditorCommands.h"
#include "Framework/Commands/UICommandList.h"
#include "Framework/MultiBox/MultiBoxBuilder.h"

#include "Quest.h"

#define LOCTEXT_NAMESPACE "QuestToolbar"

/*-----------------------------------------------------------------------------
	FGenericEditorToolbar implementation.
-----------------------------------------------------------------------------*/

void FGenericEditorToolbar::AddCompileToolbar(TSharedPtr<FExtender> Extender)
{
    TSharedPtr<FGenericEditor> QuestEditorPtr = QuestEditor.Pin();

    Extender->AddToolBarExtension("Asset", EExtensionHook::Before, QuestEditorPtr->GetToolkitCommands(),
        FToolBarExtensionDelegate::CreateSP(this, &FGenericEditorToolbar::FillCompileToolbar));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditorToolbar::AddNewToolbar(TSharedPtr<FExtender> Extender)
{
    TSharedPtr<FGenericEditor> QuestEditorPtr = QuestEditor.Pin();

    Extender->AddToolBarExtension("MyQuest", EExtensionHook::After, QuestEditorPtr->GetToolkitCommands(),
        FToolBarExtensionDelegate::CreateSP(this, &FGenericEditorToolbar::FillNewToolbar));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FSlateIcon FGenericEditorToolbar::GetStatusImage() const
{
    UGenericContainer* QuestObj = QuestEditor.Pin()->GetModel();
    EBlueprintStatus Status = Cast<UQuest>(QuestObj)->Status;

    switch (Status)
    {
        default:
        case BS_Unknown:
        case BS_Dirty:
            return FSlateIcon(FEditorStyle::GetStyleSetName(), "Kismet.Status.Unknown");
        case BS_Error:
            return FSlateIcon(FEditorStyle::GetStyleSetName(), "Kismet.Status.Error");
        case BS_UpToDate:
            return FSlateIcon(FEditorStyle::GetStyleSetName(), "Kismet.Status.Good");
        case BS_UpToDateWithWarnings:
            return FSlateIcon(FEditorStyle::GetStyleSetName(), "Kismet.Status.Warning");
    }

    return FSlateIcon(FEditorStyle::GetStyleSetName(), "Kismet.Status.Good");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText FGenericEditorToolbar::GetStatusTooltip() const
{
    return FText();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditorToolbar::FillCompileToolbar(FToolBarBuilder& ToolbarBuilder)
{
    const FQuestGraphEditorCommands& Commands = FQuestGraphEditorCommands::Get();
    TSharedPtr<FGenericEditor> QuestEditorPtr = QuestEditor.Pin();
    UGenericContainer* QuestObj = QuestEditorPtr->GetModel();

    ToolbarBuilder.BeginSection("Compile");
    if (QuestObj != nullptr)
    {
        ToolbarBuilder.AddToolBarButton(Commands.Compile, NAME_None, TAttribute<FText>(),
            TAttribute<FText>(this, &FGenericEditorToolbar::GetStatusTooltip),
            TAttribute<FSlateIcon>(this, &FGenericEditorToolbar::GetStatusImage), FName(TEXT("CompileBlueprint")));
    }
    ToolbarBuilder.EndSection();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditorToolbar::FillNewToolbar(FToolBarBuilder& ToolbarBuilder)
{
}

#undef LOCTEXT_NAMESPACE

// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Layout/Visibility.h"
#include "Widgets/DeclarativeSyntaxSupport.h"
#include "Input/Reply.h"
#include "SGraphNode.h"
#include "Widgets/SBoxPanel.h"

class SWrapBox;


class SGraphNodeWithoutTitle : public SGraphNode
{
public:
	SLATE_BEGIN_ARGS(SGraphNodeWithoutTitle)
	{
	}
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, class UGenericGraphNode* InNode);

protected:
	// SGraphNode Interface
	void UpdateGraphNode() override;
	// TSharedRef<SWidget> CreateTitleWidget(TSharedPtr<SNodeTitle> NodeTitle) override;
	TSharedRef<SWidget> CreateNodeContentArea() override;
	// End of SGraphNode interface

	// virtual void CreateNodeTextContent(TSharedPtr<SHorizontalBox> MainBox) = 0;

protected:
	/** The area where input pins reside */
	TSharedPtr<SHorizontalBox> ContentNodeBox;
};

// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "GraphNodeWithoutTitle.h"

class SWrapBox;


class SGraphNodeLogical : public SGraphNodeWithoutTitle
{
public:
	SLATE_BEGIN_ARGS(SGraphNodeLogical)
	{
	}
    SLATE_END_ARGS()

    void Construct(const FArguments& InArgs, class UGenericGraphNode* InNode);

protected:
    // SGraphNode Interface
    void UpdateGraphNode() override;
    TSharedRef<SWidget> CreateNodeContentArea() override;
    // End of SGraphNode interface

    virtual void CreateNodeTextContent(TSharedPtr<SHorizontalBox> MainBox);

protected:
    /** The area where input pins reside */
    TSharedPtr<SHorizontalBox> ContentNodeBox;
};

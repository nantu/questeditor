// Copyright (c) 2020 Gil Engel

#include "GenericGraph/GenericGraphNode.h"
#include "EdGraphSchema_K2.h"
#include "GenericGraph/GenericGraph.h"
#include "GenericGraph/GenericGraphSchema.h"
#include "Quest.h"
#include "ScopedTransaction.h"
#include "ToolMenus.h"

#define LOCTEXT_NAMESPACE "GenericGraphNode"

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphNode::InsertNewNode(UEdGraphPin* FromPin, UEdGraphPin* NewLinkPin, TSet<UEdGraphNode*>& OutNodeList) 
{
	const UGenericGraphSchema* Schema = CastChecked<UGenericGraphSchema>(GetSchema());

	// The pin we are creating from already has a connection that needs to be broken. We want to "insert" the new node in between,
	// so that the output of the new node is hooked up too
	UEdGraphPin* OldLinkedPin = FromPin->LinkedTo[0];
	check(OldLinkedPin);

	FromPin->BreakAllPinLinks();

	// Hook up the old linked pin to the first valid output pin on the new node
	for (int32 OutpinPinIdx = 0; OutpinPinIdx < Pins.Num(); OutpinPinIdx++)
	{
		UEdGraphPin* OutputExecPin = Pins[OutpinPinIdx];
		check(OutputExecPin);
		if (ECanCreateConnectionResponse::CONNECT_RESPONSE_MAKE ==
			Schema->CanCreateConnection(OldLinkedPin, OutputExecPin).Response)
		{
			if (Schema->TryCreateConnection(OldLinkedPin, OutputExecPin))
			{
				OutNodeList.Add(OldLinkedPin->GetOwningNode());
				OutNodeList.Add(this);
			}
			break;
		}
	}

	if (Schema->TryCreateConnection(FromPin, NewLinkPin))
	{
		OutNodeList.Add(FromPin->GetOwningNode());
		OutNodeList.Add(this);
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

QUESTEDITOR_API UEdGraphPin* UGenericGraphNode::GetInputPin(int32 InputIndex)
{
	check(InputIndex >= 0 && InputIndex < GetInputCount());

	for (int32 PinIndex = 0, FoundInputs = 0; PinIndex < Pins.Num(); PinIndex++)
	{
		if (Pins[PinIndex]->Direction == EGPD_Input)
		{
			if (InputIndex == FoundInputs)
			{
				return Pins[PinIndex];
			}
			else
			{
				FoundInputs++;
			}
		}
	}

	return nullptr;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TArray<class UEdGraphPin*> UGenericGraphNode::GetInputPins() const
{
	return GetPinsForDirection(EGPD_Input);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TArray<class UEdGraphPin*> UGenericGraphNode::GetOutputPins() const
{
	return GetPinsForDirection(EGPD_Output);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

int32 UGenericGraphNode::GetInputCount() const
{
	return GetPinsCountForDirection(EGPD_Input);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

int32 UGenericGraphNode::GetOutputCount() const
{
	return GetPinsCountForDirection(EGPD_Output);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

QUESTEDITOR_API void UGenericGraphNode::SetGenericNode(UGenericNode* InGenericNode)
{
	GenericNode = InGenericNode;
	InGenericNode->GraphNode = this;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphNode::GetNodeContextMenuActions(UToolMenu* Menu, UGraphNodeContextMenuContext* Context) const
{
	// Ignore delete input & output actions for the root quest node
	if (GenericNode == nullptr)
	{
		return;
	}


	if (!Context->bIsDebugging)
	{
		static FName CommutativeAssociativeBinaryOperatorNodeName = FName("CommutativeAssociativeBinaryOperatorNode");
		FText CommutativeAssociativeBinaryOperatorStr = LOCTEXT("CommutativeAssociativeBinaryOperatorNode", "Operator Node");
		if (Context->Pin != NULL)
		{
			if (CanRemovePin(Context->Pin))
			{
				FToolMenuSection& Section =
					Menu->AddSection(CommutativeAssociativeBinaryOperatorNodeName, CommutativeAssociativeBinaryOperatorStr);
				Section.AddMenuEntry("RemovePin", LOCTEXT("RemovePin", "Remove pin"),
					LOCTEXT("RemovePinTooltip", "Remove this output pin"), FSlateIcon(),
					FUIAction(FExecuteAction::CreateUObject(const_cast<UGenericGraphNode*>(this),
						&UGenericGraphNode::RemoveOutputPin, const_cast<UEdGraphPin*>(Context->Pin))));
			}
		}
		else if (true)
		{
			FToolMenuSection& Section =
				Menu->AddSection(CommutativeAssociativeBinaryOperatorNodeName, CommutativeAssociativeBinaryOperatorStr);
			Section.AddMenuEntry("AddPin", LOCTEXT("AddPin", "Add pin"), LOCTEXT("AddPinTooltip", "Add another input pin"),
				FSlateIcon(),
				FUIAction(FExecuteAction::CreateUObject(
					const_cast<UGenericGraphNode*>(this), &UGenericGraphNode::AddInputPin)));
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UGenericGraphNode::CanRemovePin(const UEdGraphPin* Pin) const
{
	return this->GetPinsForDirection(EEdGraphPinDirection::EGPD_Output).Num() > GenericNode->GetMinimumChildNodesCount();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

QUESTEDITOR_API void UGenericGraphNode::RemoveInputPin(UEdGraphPin* InGraphPin)
{
	const FScopedTransaction Transaction(NSLOCTEXT("UnrealEd", "QuestEditorDeleteInput", "Delete Quest Node Output"));
	Modify();

	TArray<class UEdGraphPin*> InputPins = GetInputPins();

	for (int32 InputIndex = 0; InputIndex < InputPins.Num(); InputIndex++)
	{
		if (InGraphPin == InputPins[InputIndex])
		{
			InGraphPin->MarkPendingKill();
			Pins.Remove(InGraphPin);
			// also remove the QuestNode child node so ordering matches
			GenericNode->Modify();
			GenericNode->RemoveParentNode(InputIndex);
			break;
		}
	}

	UGenericContainer* Quest = CastChecked<UGenericGraph>(GetGraph())->GetModel();
	Quest->CompileQuestNodesFromGraphNodes();
	Quest->MarkPackageDirty();

	// Refresh the current graph, so the pins can be updated
	GetGraph()->NotifyGraphChanged();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

QUESTEDITOR_API void UGenericGraphNode::RemoveOutputPin(UEdGraphPin* OutGraphPin)
{
	const FScopedTransaction Transaction(NSLOCTEXT("UnrealEd", "GenericEditorDeleteOutput", "Delete Generic Node Output"));
	Modify();

	TArray<class UEdGraphPin*> OutputPins = GetOutputPins();

	for (int32 InputIndex = 0; InputIndex < OutputPins.Num(); InputIndex++)
	{
		if (OutGraphPin == OutputPins[InputIndex])
		{
			OutGraphPin->MarkPendingKill();
			Pins.Remove(OutGraphPin);
			// also remove the QuestNode child node so ordering matches
			GenericNode->Modify();
			GenericNode->RemoveParentNode(InputIndex);
			break;
		}
	}

	UGenericContainer* Quest = CastChecked<UGenericGraph>(GetGraph())->GetModel();
	Quest->CompileQuestNodesFromGraphNodes();
	Quest->MarkPackageDirty();

	// Refresh the current graph, so the pins can be updated
	GetGraph()->NotifyGraphChanged();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphNode::AllocateDefaultPins()
{
	check(Pins.Num() == 0);

	CreateInputPins();
	CreateOutputPins();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphNode::ReconstructNode()
{
	Modify();

	// Break any links to 'orphan' pins
	for (int32 PinIndex = 0; PinIndex < Pins.Num(); ++PinIndex)
	{
		UEdGraphPin* Pin = Pins[PinIndex];
		TArray<class UEdGraphPin*>& LinkedToRef = Pin->LinkedTo;
		for (int32 LinkIdx = 0; LinkIdx < LinkedToRef.Num(); LinkIdx++)
		{
			UEdGraphPin* OtherPin = LinkedToRef[LinkIdx];
			// If we are linked to a pin that its owner doesn't know about, break that link
			if (!OtherPin->GetOwningNode()->Pins.Contains(OtherPin))
			{
				Pin->LinkedTo.Remove(OtherPin);
			}
		}
	}

	// Store the old Input and Output pins
	TArray<UEdGraphPin*> OldInputPins = GetInputPins();
	TArray<UEdGraphPin*> OldOutputPins = GetOutputPins();

	// Move the existing pins to a saved array
	TArray<UEdGraphPin*> OldPins(Pins);
	Pins.Reset();

	// Recreate the new pins
	AllocateDefaultPins();

	// Get new Input and Output pins
	TArray<UEdGraphPin*> NewInputPins = GetInputPins();	
	for (int32 PinIndex = 0; PinIndex < OldInputPins.Num(); PinIndex++)
	{
		if (PinIndex < NewInputPins.Num())
		{
			NewInputPins[PinIndex]->MovePersistentDataFromOldPin(*OldInputPins[PinIndex]);
		}
	}

	TArray<UEdGraphPin*> NewOutputPins = GetOutputPins();
	for (int32 PinIndex = 0; PinIndex < OldOutputPins.Num(); PinIndex++)
	{
		if (PinIndex < NewOutputPins.Num())
		{
			NewOutputPins[PinIndex]->MovePersistentDataFromOldPin(*OldOutputPins[PinIndex]);
		}
	}

	// Throw away the original pins
	for (UEdGraphPin* OldPin : OldPins)
	{
		OldPin->Modify();
		UEdGraphNode::DestroyPin(OldPin);
	}

	GetGraph()->NotifyGraphChanged();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphNode::AutowireNewNode(UEdGraphPin* FromPin)
{
	if (FromPin != nullptr)
	{
		const UGenericGraphSchema* Schema = CastChecked<UGenericGraphSchema>(GetSchema());

		TSet<UEdGraphNode*> NodeList;

		// auto-connect from dragged pin to first compatible pin on the new node
		for (int32 i = 0; i < Pins.Num(); i++)
		{
			UEdGraphPin* Pin = Pins[i];
			check(Pin);
			FPinConnectionResponse Response = Schema->CanCreateConnection(FromPin, Pin);
			if (ECanCreateConnectionResponse::CONNECT_RESPONSE_MAKE == Response.Response)
			{
				if (Schema->TryCreateConnection(FromPin, Pin))
				{
					NodeList.Add(FromPin->GetOwningNode());
					NodeList.Add(this);
				}
				break;
			}
			else if (ECanCreateConnectionResponse::CONNECT_RESPONSE_BREAK_OTHERS_A == Response.Response)
			{
				InsertNewNode(FromPin, Pin, NodeList);
				break;
			}
		}

		// Send all nodes that received a new pin connection a notification
		for (auto It = NodeList.CreateConstIterator(); It; ++It)
		{
			UEdGraphNode* Node = (*It);
			Node->NodeConnectionListChanged();
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UGenericGraphNode::CanCreateUnderSpecifiedSchema(const UEdGraphSchema* Schema) const
{
	return Schema->IsA(UGenericGraphSchema::StaticClass());
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FString UGenericGraphNode::GetDocumentationLink() const
{
	return TEXT("Shared/GraphNodes/SoundCue");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphNode::AddInputPin()
{
	CreatePin(EGPD_Input, UEdGraphSchema_K2::PC_Boolean, "");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphNode::AddOutputPin()
{
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

int32 UGenericGraphNode::GetPinsCountForDirection(EEdGraphPinDirection direction) const
{
	int32 Count = 0;

	for (int32 PinIndex = 0, FoundInputs = 0; PinIndex < Pins.Num(); PinIndex++)
	{
		if (Pins[PinIndex]->Direction == direction)
		{
			Count++;
		}
	}

	return Count;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TArray<class UEdGraphPin*> UGenericGraphNode::GetPinsForDirection(EEdGraphPinDirection direction) const
{
	TArray<class UEdGraphPin*> ResultingPins;

	for (int32 PinIndex = 0; PinIndex < Pins.Num(); PinIndex++)
	{
		if (Pins[PinIndex]->Direction == direction)
		{
			ResultingPins.Add(Pins[PinIndex]);
		}
	}

	return ResultingPins;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#undef LOCTEXT_NAMESPACE
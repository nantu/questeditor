// Copyright (c) 2020 Gil Engel

#include "GenericGraph/GenericGraphNode_Root.h"
#include "EdGraphSchema_K2.h"
#include "GraphEditorSettings.h"
#include "ToolMenus.h"

#define LOCTEXT_NAMESPACE "QuestGraphNode_Root"

/*-----------------------------------------------------------------------------
    UGenericGraphNode_Root implementation.
-----------------------------------------------------------------------------*/

UGenericGraphNode_Root::UGenericGraphNode_Root(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
	CreatePin(EGPD_Output, UEdGraphSchema_K2::PC_Exec, "");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText UGenericGraphNode_Root::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
    return LOCTEXT("RootTitle", "Start");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText UGenericGraphNode_Root::GetTooltipText() const
{
    return LOCTEXT("RootToolTip", "Quest starting node");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphNode_Root::CreateInputPins()
{
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphNode_Root::CreateOutputPins()
{
    CreatePin(EGPD_Output, UEdGraphSchema_K2::PC_Exec, "");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#undef LOCTEXT_NAMESPACE

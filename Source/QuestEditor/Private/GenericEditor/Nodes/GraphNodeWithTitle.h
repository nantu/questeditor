// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Layout/Visibility.h"
#include "Widgets/DeclarativeSyntaxSupport.h"
#include "Input/Reply.h"
#include "SGraphNode.h"
#include "Widgets/SBoxPanel.h"

class SWrapBox;


class SGraphNodeWithTitle : public SGraphNode
{
public:
	SLATE_BEGIN_ARGS(SGraphNodeWithTitle)
	{
	}
    SLATE_END_ARGS()

    void Construct(const FArguments& InArgs, class UGenericGraphNode* InNode);

protected:
    // SGraphNode Interface
    void UpdateGraphNode() override;
	TSharedRef<SWidget> CreateTitleWidget(TSharedPtr<SNodeTitle> NodeTitle) override;
    TSharedRef<SWidget> CreateNodeContentArea() override;
    // End of SGraphNode interface

    /** @return the tint for the node's title image */
	FSlateColor GetNodeTitleColor() const;

    /** @return the tint for the node's title text */
	FLinearColor GetNodeTitleTextColor() const;

protected:
    /** The area where input pins reside */
    TSharedPtr<SHorizontalBox> ContentNodeBox;
	TSharedPtr<SVerticalBox> InnerVerticalBox;
};

// Copyright (c) 2020 Gil Engel

#include "GenericGraph/GenericGraphNode_Result.h"
#include "EdGraphSchema_K2.h"
#include "GraphEditorSettings.h"
#include "ToolMenus.h"

#define LOCTEXT_NAMESPACE "GenericGraphNode_Result"

/*-----------------------------------------------------------------------------
    UQuestGraphNode_Result implementation.
-----------------------------------------------------------------------------*/

FText UGenericGraphNode_Result::GetTooltipText() const
{
    return LOCTEXT("ResultToolTip", "Wire final quest tasks into this node");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphNode_Result::CreateInputPins() 
{
	CreatePin(EGPD_Input, UEdGraphSchema_K2::PC_Exec, "");
}

#undef LOCTEXT_NAMESPACE

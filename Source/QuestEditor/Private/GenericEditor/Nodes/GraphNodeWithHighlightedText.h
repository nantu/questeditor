// Copyright (c) 2020 Gil Engel

#pragma once

#include "GraphNodeWithoutTitle.h"

class SWrapBox;
class FSlateStyleSet;

class SGraphNodeWithHighlightedText : public SGraphNodeWithoutTitle
{
public:
	SLATE_BEGIN_ARGS(SGraphNodeWithHighlightedText)
	{
	}
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, class UGenericGraphNode* InNode);

protected:
	//void CreateNodeTextContent(TSharedPtr<SHorizontalBox> MainBox) override;

	virtual FText GetDescription() const;
	virtual bool HasValidDescription() const;

private:
	TSharedPtr<FSlateStyleSet> Style;
};

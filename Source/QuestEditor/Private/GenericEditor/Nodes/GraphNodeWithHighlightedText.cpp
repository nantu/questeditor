#include "GraphNodeWithHighlightedText.h"
#include "Widgets/Text/SRichTextBlock.h"
#include "Styling/SlateStyle.h"

#define DEFAULT_FONT(...) FCoreStyle::GetDefaultFontStyle(__VA_ARGS__)

/*-----------------------------------------------------------------------------
	SGraphNodeWithHighlightedText implementation.
-----------------------------------------------------------------------------*/

/*
void SGraphNodeWithHighlightedText::CreateNodeTextContent(TSharedPtr<SHorizontalBox> MainBox)
{
		Style = MakeShareable<FSlateStyleSet>(new FSlateStyleSet(TEXT("QuestNodeStyle")));

		FTextBlockStyle DefaultTextStyle;
		DefaultTextStyle.Font = DEFAULT_FONT("Regular", 12);
		DefaultTextStyle.SetColorAndOpacity(FSlateColor::UseForeground());

		FTextBlockStyle KeyTextStyle;
		KeyTextStyle.SetColorAndOpacity(FLinearColor(FColor(106, 176, 76, 255)));
		KeyTextStyle.Font = DEFAULT_FONT("Bold", 12);

		FTextBlockStyle WarningTextStyle;
		WarningTextStyle.SetColorAndOpacity(FLinearColor(FColor(240, 147, 43, 255)));
		WarningTextStyle.Font = DEFAULT_FONT("Regular", 12);

		Style->Set("Default", DefaultTextStyle);
		Style->Set("Key", KeyTextStyle);
		Style->Set("Warning", WarningTextStyle);

		// clang-format off
        MainBox->AddSlot()
            .Padding(0, 20)
            .VAlign(VAlign_Center)
            [
                SNew(SRichTextBlock)
                .Text(GetDescription())
                .DecoratorStyleSet(Style.Get())
                .TextStyle(HasValidDescription() ? &DefaultTextStyle : &WarningTextStyle)
                .Margin(FMargin(InputPins.Num() == 0 ? 20 : 0, 0, OutputPins.Num() == 0 ? 20 : 0, 0))
            ];
		// clang-format on
}
*/
//----------------------------------------------------------------------------------------------------------------------------------------------------

FText SGraphNodeWithHighlightedText::GetDescription() const
{
	return FText();
}

bool SGraphNodeWithHighlightedText::HasValidDescription() const
{
	return true;
	/*
	if (!QuestNode || !QuestNode->QuestNode)
	{
		return true;
	}

	const TCHAR OpeningBracket = (ANSI_TO_TCHAR("{"))[0];
	const TCHAR ClosingBracked = (ANSI_TO_TCHAR("}"))[0];

	int32 Count = 0;
	for (auto& Char : QuestNode->QuestNode->GetDisplayNameText().ToString())
	{
		if (Char == OpeningBracket)
		{
			if (Count >= 0 && Count <= 2)
			{
				++Count;
			}
			else
			{
				return false;
			}
		}
		else if (Char == ClosingBracked)
		{
			if (Count >= 0 && Count <= 2)
			{
				--Count;
			}
			else
			{
				return false;
			}
		}
	}

	return Count == 0;
	*/
}
// Copyright (c) 2020 Gil Engel

#include "GenericEditor/Nodes/GraphNodeWithoutTitle.h"
#include "Widgets/SBoxPanel.h"
#include "QuestGraph/QuestGraphNode.h"
#include "ScopedTransaction.h"
#include "GraphEditorSettings.h"
#include "Widgets/Layout/SSpacer.h"
#include "Widgets/Images/SImage.h"
#include "Widgets/Layout/SWrapBox.h"
#include "SCommentBubble.h"
#include "IDocumentation.h"
#include "TutorialMetaData.h"
 	
#include "Widgets/Text/SInlineEditableTextBlock.h"
#include "SLevelOfDetailBranchNode.h"
#include "QuestNode.h"

/*-----------------------------------------------------------------------------
	SGraphNodeWithoutTitle implementation.
-----------------------------------------------------------------------------*/

void SGraphNodeWithoutTitle::Construct(const FArguments& InArgs, class UGenericGraphNode* InNode)
{
	this->GraphNode = InNode;

	this->SetCursor(EMouseCursor::CardinalCross);

	this->UpdateGraphNode();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TSharedRef<SWidget> SGraphNodeWithoutTitle::CreateNodeContentArea()
{
	return SNew(SBorder)
		.BorderImage(FEditorStyle::GetBrush("NoBorder"))
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		.Padding(FMargin(0, 3))
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			.HAlign(HAlign_Left)
			.VAlign(VAlign_Center)
			.FillWidth(1.0f)
			[
				// LEFT
				SAssignNew(LeftNodeBox, SVerticalBox)
			] +
		SHorizontalBox::Slot().FillWidth(1.0)
		.AutoWidth()
		.MaxWidth(8000)
		.HAlign(HAlign_Right)
		.VAlign(VAlign_Center)
		[
			SNew(SHorizontalBox) + SHorizontalBox::Slot()
			[SAssignNew(ContentNodeBox, SHorizontalBox)
			]
			// CONTENT
		]
	+ SHorizontalBox::Slot()
		.AutoWidth()
		.HAlign(HAlign_Right)
		.VAlign(VAlign_Center)
		[
			// RIGHT
			SAssignNew(RightNodeBox, SVerticalBox)
		]
		];
}

//----------------------------------------------------------------------------------------------------------------------------------------------------


void SGraphNodeWithoutTitle::UpdateGraphNode()
{
		InputPins.Empty();
		OutputPins.Empty();

		// Reset variables that are going to be exposed, in case we are refreshing an already setup node.
		RightNodeBox.Reset();
		LeftNodeBox.Reset();

		//
		//             ______________________
		//            |      TITLE AREA      |
		//            +-------+------+-------+
		//            | (>) L |      | R (>) |
		//            | (>) E |      | I (>) |
		//            | (>) F |      | G (>) |
		//            | (>) T |      | H (>) |
		//            |       |      | T (>) |
		//            |_______|______|_______|
		//
		TSharedPtr<SVerticalBox> MainVerticalBox;
		SetupErrorReporting();

		if (!SWidget::GetToolTip().IsValid())
		{
			TSharedRef<SToolTip> DefaultToolTip =
				IDocumentation::Get()->CreateToolTip(TAttribute<FText>(this, &SGraphNode::GetNodeTooltip), NULL,
					GraphNode->GetDocumentationLink(), GraphNode->GetDocumentationExcerptName());
			SetToolTip(DefaultToolTip);
		}

		// Setup a meta tag for this node
		FGraphNodeMetaData TagMeta(TEXT("Graphnode"));
		PopulateMetaTag(&TagMeta);

		TSharedPtr<SVerticalBox> InnerVerticalBox;
		this->ContentScale.Bind(this, &SGraphNode::GetContentScale);

		InnerVerticalBox = SNew(SVerticalBox)
						   + SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Fill).VAlign(VAlign_Top)[CreateNodeContentArea()];

		TSharedPtr<SWidget> EnabledStateWidget = GetEnabledStateWidget();
		if (EnabledStateWidget.IsValid())
		{
			InnerVerticalBox->AddSlot()
				.AutoHeight()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Top)
				.Padding(FMargin(2, 0))[EnabledStateWidget.ToSharedRef()];
		}

		InnerVerticalBox->AddSlot().AutoHeight().Padding(Settings->GetNonPinNodeBodyPadding())[ErrorReporting->AsWidget()];

		this->GetOrAddSlot(ENodeZone::Center)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
				[SAssignNew(MainVerticalBox, SVerticalBox) +
					SVerticalBox::Slot().AutoHeight()
						[SNew(SOverlay).AddMetaData<FGraphNodeMetaData>(TagMeta) +
							SOverlay::Slot().Padding(Settings->GetNonPinNodeBodyPadding())
								[SNew(SImage).Image(GetNodeBodyBrush()).ColorAndOpacity(this, &SGraphNode::GetNodeBodyColor)] +
							SOverlay::Slot()[InnerVerticalBox.ToSharedRef()]]];

		// Create comment bubble
		TSharedPtr<SCommentBubble> CommentBubble;
		const FSlateColor CommentColor = GetDefault<UGraphEditorSettings>()->DefaultCommentNodeTitleColor;

		SAssignNew(CommentBubble, SCommentBubble)
			.GraphNode(GraphNode)
			.Text(this, &SGraphNode::GetNodeComment)
			.OnTextCommitted(this, &SGraphNode::OnCommentTextCommitted)
			.OnToggled(this, &SGraphNode::OnCommentBubbleToggled)
			.ColorAndOpacity(CommentColor)
			.AllowPinning(true)
			.EnableTitleBarBubble(true)
			.EnableBubbleCtrls(true)
			.GraphLOD(this, &SGraphNode::GetCurrentLOD)
			.IsGraphNodeHovered(this, &SGraphNode::IsHovered);

		GetOrAddSlot(ENodeZone::TopCenter)
			.SlotOffset(TAttribute<FVector2D>(CommentBubble.Get(), &SCommentBubble::GetOffset))
			.SlotSize(TAttribute<FVector2D>(CommentBubble.Get(), &SCommentBubble::GetSize))
			.AllowScaling(TAttribute<bool>(CommentBubble.Get(), &SCommentBubble::IsScalingAllowed))
			.VAlign(VAlign_Top)[CommentBubble.ToSharedRef()];

		CreateBelowWidgetControls(MainVerticalBox);
		CreatePinWidgets();
		CreateInputSideAddButton(LeftNodeBox);
		CreateOutputSideAddButton(RightNodeBox);
		CreateBelowPinControls(InnerVerticalBox);
		CreateAdvancedViewArrow(InnerVerticalBox);	  
}

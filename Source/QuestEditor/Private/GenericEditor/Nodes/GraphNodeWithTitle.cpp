// Copyright (c) 2020 Gil Engel

#include "GenericEditor/Nodes/GraphNodeWithTitle.h"
#include "Widgets/SBoxPanel.h"
#include "QuestGraph/QuestGraphNode.h"
#include "ScopedTransaction.h"
#include "GraphEditorSettings.h"
#include "Widgets/Layout/SSpacer.h"
#include "Widgets/Images/SImage.h"
#include "Widgets/Layout/SWrapBox.h"
#include "Widgets/Layout/SBorder.h"
#include "SCommentBubble.h"
#include "IDocumentation.h"
#include "TutorialMetaData.h"

#include "QuestStyle.h"
	
#include "Widgets/Text/SInlineEditableTextBlock.h"
#include "SLevelOfDetailBranchNode.h"
#include "QuestNode.h"

/*-----------------------------------------------------------------------------
	SGraphNodeWithTitle implementation.
-----------------------------------------------------------------------------*/

void SGraphNodeWithTitle::Construct(const FArguments& InArgs, class UGenericGraphNode* InNode)
{
	this->GraphNode = InNode;

	this->SetCursor(EMouseCursor::CardinalCross);

	this->UpdateGraphNode();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TSharedRef<SWidget> SGraphNodeWithTitle::CreateNodeContentArea()
{
	return SNew(SBorder)
		.BorderImage(FEditorStyle::GetBrush("NoBorder"))
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		//.Padding(FMargin(0, 3))
		[
			SNew(SHorizontalBox)
			+ SHorizontalBox::Slot()
			.HAlign(HAlign_Left)
			.VAlign(VAlign_Center)
			.FillWidth(1.0f)
			[
				// LEFT
				SAssignNew(LeftNodeBox, SVerticalBox)
			] 
	        + SHorizontalBox::Slot()
			.FillWidth(1.0)
			.AutoWidth()
			.MaxWidth(8000)
			.HAlign(HAlign_Right)
			.VAlign(VAlign_Center)
			[
				// CONTENT
				SNew(SHorizontalBox) + SHorizontalBox::Slot()
				[
					SAssignNew(ContentNodeBox, SHorizontalBox)
				]				
			]
			+ SHorizontalBox::Slot()
			.AutoWidth()
			.HAlign(HAlign_Right)
			.VAlign(VAlign_Center)
			.Padding(55.f, 0.f, 0.f, 0.f)
			[
				// RIGHT
				SAssignNew(RightNodeBox, SVerticalBox)
			]
		];
}

//----------------------------------------------------------------------------------------------------------------------------------------------------


void SGraphNodeWithTitle::UpdateGraphNode()
{
		InputPins.Empty();
		OutputPins.Empty();

		// Reset variables that are going to be exposed, in case we are refreshing an already setup node.
		RightNodeBox.Reset();
		LeftNodeBox.Reset();

		//
		//             ______________________
		//            |      TITLE AREA      |
		//            +-------+------+-------+
		//            | (>) L |      | R (>) |
		//            | (>) E |      | I (>) |
		//            | (>) F |      | G (>) |
		//            | (>) T |      | H (>) |
		//            |       |      | T (>) |
		//            |_______|______|_______|
		//
		TSharedPtr<SVerticalBox> MainVerticalBox;
		SetupErrorReporting();

		TSharedPtr<SNodeTitle> NodeTitle = SNew(SNodeTitle, GraphNode);


	// Get node icon
		IconColor = FLinearColor::White;
		const FSlateBrush* IconBrush = nullptr;
		if (GraphNode != NULL && GraphNode->ShowPaletteIconOnNode())
		{
			IconBrush = GraphNode->GetIconAndTint(IconColor).GetOptionalIcon();
		}

		TSharedRef<SOverlay> DefaultTitleAreaWidget =
			SNew(SOverlay) +
			SOverlay::Slot()[SNew(SImage)
								 .Image(FEditorStyle::GetBrush("Graph.Node.TitleGloss"))
								 .ColorAndOpacity(this, &SGraphNode::GetNodeTitleIconColor)] +
			SOverlay::Slot()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Center)
					[SNew(SHorizontalBox) +
						SHorizontalBox::Slot().HAlign(HAlign_Fill)
							[SNew(SBorder)
									.BorderImage(FQuestStyle::Get()->GetBrush("Graph.Node.ColorSpill"))
									// The extra margin on the right
									// is for making the color spill stretch well past the node title
									.Padding(FMargin(10, 5, 10, 3))
									.BorderBackgroundColor(this, &SGraphNode::GetNodeTitleColor)
										[SNew(SHorizontalBox) +
											SHorizontalBox::Slot()
												.VAlign(VAlign_Top)
												.Padding(FMargin(0.f, 0.f, 4.f, 0.f))
												.AutoWidth()[SNew(SImage).Image(IconBrush).ColorAndOpacity(
													this, &SGraphNode::GetNodeTitleIconColor)] +
											SHorizontalBox::Slot()[SNew(SVerticalBox) +
																   SVerticalBox::Slot().AutoHeight()[CreateTitleWidget(NodeTitle)] +
																   SVerticalBox::Slot().AutoHeight()[NodeTitle.ToSharedRef()]]]] +
						SHorizontalBox::Slot()
							.HAlign(HAlign_Right)
							.VAlign(VAlign_Center)
							.Padding(0, 0, 0, 0)
							.AutoWidth()[CreateTitleRightWidget()]] +
			SOverlay::Slot().VAlign(VAlign_Top)[SNew(SBorder)
													.Visibility(EVisibility::HitTestInvisible)
													.BorderImage(FEditorStyle::GetBrush("Graph.Node.TitleHighlight"))
													.BorderBackgroundColor(this,
														&SGraphNode::GetNodeTitleIconColor)[SNew(SSpacer).Size(FVector2D(20, 20))]];

		SetDefaultTitleAreaWidget(DefaultTitleAreaWidget);

		TSharedRef<SWidget> TitleAreaWidget = DefaultTitleAreaWidget;

		if (!SWidget::GetToolTip().IsValid())
		{
			TSharedRef<SToolTip> DefaultToolTip =
				IDocumentation::Get()->CreateToolTip(TAttribute<FText>(this, &SGraphNode::GetNodeTooltip), NULL,
					GraphNode->GetDocumentationLink(), GraphNode->GetDocumentationExcerptName());
			SetToolTip(DefaultToolTip);
		}

		// Setup a meta tag for this node
		FGraphNodeMetaData TagMeta(TEXT("Graphnode"));
		PopulateMetaTag(&TagMeta);

		this->ContentScale.Bind(this, &SGraphNode::GetContentScale);

		InnerVerticalBox = SNew(SVerticalBox) +
						   SVerticalBox::Slot()
							   .AutoHeight()
							   .HAlign(HAlign_Fill)
							   .VAlign(VAlign_Top)
							   .Padding(Settings->GetNonPinNodeBodyPadding())[TitleAreaWidget]

						   + SVerticalBox::Slot().AutoHeight().HAlign(HAlign_Fill).VAlign(VAlign_Top)[CreateNodeContentArea()];

		TSharedPtr<SWidget> EnabledStateWidget = GetEnabledStateWidget();
		if (EnabledStateWidget.IsValid())
		{
			InnerVerticalBox->AddSlot()
				.AutoHeight()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Top)
				.Padding(FMargin(2, 0))[EnabledStateWidget.ToSharedRef()];
		}

		InnerVerticalBox->AddSlot().AutoHeight().Padding(Settings->GetNonPinNodeBodyPadding())[ErrorReporting->AsWidget()];

		this->GetOrAddSlot(ENodeZone::Center)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
				[SAssignNew(MainVerticalBox, SVerticalBox) +
					SVerticalBox::Slot().AutoHeight()
						[SNew(SOverlay).AddMetaData<FGraphNodeMetaData>(TagMeta) +
							SOverlay::Slot().Padding(Settings->GetNonPinNodeBodyPadding())
								[SNew(SImage).Image(GetNodeBodyBrush()).ColorAndOpacity(this, &SGraphNode::GetNodeBodyColor)] +
							SOverlay::Slot()[InnerVerticalBox.ToSharedRef()]]];

		// Create comment bubble
		TSharedPtr<SCommentBubble> CommentBubble;
		const FSlateColor CommentColor = GetDefault<UGraphEditorSettings>()->DefaultCommentNodeTitleColor;

		SAssignNew(CommentBubble, SCommentBubble)
			.GraphNode(GraphNode)
			.Text(this, &SGraphNode::GetNodeComment)
			.OnTextCommitted(this, &SGraphNode::OnCommentTextCommitted)
			.OnToggled(this, &SGraphNode::OnCommentBubbleToggled)
			.ColorAndOpacity(CommentColor)
			.AllowPinning(true)
			.EnableTitleBarBubble(true)
			.EnableBubbleCtrls(true)
			.GraphLOD(this, &SGraphNode::GetCurrentLOD)
			.IsGraphNodeHovered(this, &SGraphNode::IsHovered);

		GetOrAddSlot(ENodeZone::TopCenter)
			.SlotOffset(TAttribute<FVector2D>(CommentBubble.Get(), &SCommentBubble::GetOffset))
			.SlotSize(TAttribute<FVector2D>(CommentBubble.Get(), &SCommentBubble::GetSize))
			.AllowScaling(TAttribute<bool>(CommentBubble.Get(), &SCommentBubble::IsScalingAllowed))
			.VAlign(VAlign_Top)[CommentBubble.ToSharedRef()];

		CreateBelowWidgetControls(MainVerticalBox);
		CreatePinWidgets();
		CreateInputSideAddButton(LeftNodeBox);
		CreateOutputSideAddButton(RightNodeBox);
		CreateBelowPinControls(InnerVerticalBox);
		CreateAdvancedViewArrow(InnerVerticalBox);
}

TSharedRef<SWidget> SGraphNodeWithTitle::CreateTitleWidget(TSharedPtr<SNodeTitle> NodeTitle)
{
	SAssignNew(InlineEditableText, SInlineEditableTextBlock)
		.Style(FEditorStyle::Get(), "Graph.Node.NodeTitleInlineEditableText")
		.Text(NodeTitle.Get(), &SNodeTitle::GetHeadTitle)
		.OnVerifyTextChanged(this, &SGraphNodeWithTitle::OnVerifyNameTextChanged)
		.OnTextCommitted(this, &SGraphNodeWithTitle::OnNameTextCommited)
		.IsReadOnly(this, &SGraphNodeWithTitle::IsNameReadOnly)
		.IsSelected(this, &SGraphNodeWithTitle::IsSelectedExclusively);
	InlineEditableText->SetColorAndOpacity(TAttribute<FLinearColor>::Create(
		TAttribute<FLinearColor>::FGetter::CreateSP(this, &SGraphNodeWithTitle::GetNodeTitleTextColor)));

	return InlineEditableText.ToSharedRef();
}

FSlateColor SGraphNodeWithTitle::GetNodeTitleColor() const
{
	FLinearColor ReturnTitleColor = GraphNode->IsDeprecated() ? FLinearColor::Red : GetNodeObj()->GetNodeTitleColor();
	if (!GraphNode->IsNodeEnabled() || GraphNode->IsDisplayAsDisabledForced() || GraphNode->IsNodeUnrelated())
	{
		ReturnTitleColor *= FLinearColor(0.5f, 0.5f, 0.5f, 0.4f);
	}
	else
	{
		ReturnTitleColor.A = FadeCurve.GetLerp();
	}

	return ReturnTitleColor;
}

FLinearColor SGraphNodeWithTitle::GetNodeTitleTextColor() const
{
	return FLinearColor::White;
}

#include "GenericContainer.h"

class UEdGraph;
class UEdGraphPin;

/*******************************************************************************
 * FBlueprintActionContext
 ******************************************************************************/

struct FGenericActionContext
{
	/**
	 * A list of all blueprints you want actions for. Generally, this will
	 * only contain a single blueprint, but it can have many (where an action
	 * has to be available for every blueprint listed to pass the filter).
	 */
	TArray<UGenericContainer*> Containers;

	/**
	 * A list of graphs you want compatible actions for. Generally, this will
	 * contain a single graph, but it can have several (where an action has to
	 * be viable for every graph to pass the filter).
	 */
	TArray<UEdGraph*> Graphs;

	/**
	 * A list of pins you want compatible actions for. Generally, this will
	 * contain a single pin, but it can have several (where an action has to
	 * be viable for every pin to pass the filter).
	 */
	TArray<UEdGraphPin*> Pins;

	/**
	 * A list of objects the user currently has selected (things like blueprint
	 * properties, level actors, content-browser assets, etc.). Bound actions
	 * have to be tied to one of these objects in order to pass the filter.
	 */
	TArray<UObject*> SelectedObjects;
};
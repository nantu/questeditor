// Copyright (c) 2020 Gil Engel

#include "GenericGraph/GenericGraphSchema.h"
#include "AssetRegistryModule.h"
#include "Condition/ConditionNode.h"
#include "EdGraph/EdGraph.h"
#include "EdGraphNode_Comment.h"
#include "Editor.h"
#include "Engine/Selection.h"
#include "Flow/QuestNodeStart.h"
#include "Framework/MultiBox/MultiBoxBuilder.h"
#include "GenericEditor/GenericEditorUtilities.h"
#include "GenericGraph/GenericGraph.h"
#include "GenericGraph/GenericGraphNode.h"
#include "GraphEditor.h"
#include "GraphEditorActions.h"
#include "Kismet2/KismetEditorUtilities.h"
#include "Layout/SlateRect.h"
#include "Quest.h"
#include "QuestNode.h"
#include "QuestNodeParallel.h"
#include "QuestNodeResult.h"
#include "ScopedTransaction.h"
#include "ToolMenus.h"
#include "UObject/Linker.h"
#include "UObject/UObjectHash.h"
#include "UObject/UObjectIterator.h"

#define LOCTEXT_NAMESPACE "GenericSchema"

/*-----------------------------------------------------------------------------
	UQuestGraphSchema implementation.
-----------------------------------------------------------------------------*/

namespace Utils
{
void GetAllNativeSubclasses(
	TArray<TAssetSubclassOf<UObject>>& Subclasses, TSubclassOf<UObject> Base, bool bAllowAbstract, const TArray<UClass*>& IgnoreList)
{
	check(Base);

	if (!Base->IsNative())
	{
		return;
	}

	for (TObjectIterator<UClass> ClassIt; ClassIt; ++ClassIt)
	{
		UClass* Class = *ClassIt;

		// Only interested in native C++ classes
		if (!Class->IsNative())
		{
			continue;
		}

		// Ignore deprecated
		if (Class->HasAnyClassFlags(CLASS_Deprecated | CLASS_NewerVersionExists))
		{
			continue;
		}

#if WITH_EDITOR
		// Ignore skeleton classes (semi-compiled versions that only exist in-editor)
		if (FKismetEditorUtilities::IsClassABlueprintSkeleton(Class))
		{
			continue;
		}
#endif

		if (IgnoreList.ContainsByPredicate(
				[Class](UClass* FilterClass) -> bool { return Class->IsChildOf(FilterClass); }))
		{
			continue;
		}


		// Optionally ignore abstract classes
		if (!bAllowAbstract && Class->HasAnyClassFlags(CLASS_Abstract))
		{
			continue;
		}

		// Check this class is a subclass of Base
		if (!Class->IsChildOf(Base))
		{
			continue;
		}

		if (Class->IsChildOf(UQuestNodeStart::StaticClass()))
		{
			continue;
		}

		Subclasses.Add(Class);
	}
}

void GetAllBlueprintSubclasses(TArray<TAssetSubclassOf<UObject>>& Subclasses, TSubclassOf<UObject> Base, bool bAllowAbstract,
	const TArray<UClass*>& IgnoreList)
{
	// Load the asset registry module
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>(FName("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();
	// The asset registry is populated asynchronously at startup, so there's no guarantee it has finished.
	// This simple approach just runs a synchronous scan on the entire content directory.
	// Better solutions would be to specify only the path to where the relevant blueprints are,
	// or to register a callback with the asset registry to be notified of when it's finished populating.
	TArray<FString> ContentPaths;
	ContentPaths.Add(TEXT("/Game"));
	AssetRegistry.ScanPathsSynchronous(ContentPaths);

	FName BaseClassName = Base->GetFName();

	// Use the asset registry to get the set of all class names deriving from Base
	TSet<FName> DerivedNames;
	{
		TArray<FName> BaseNames;
		BaseNames.Add(BaseClassName);

		TSet<FName> Excluded;
		AssetRegistry.GetDerivedClassNames(BaseNames, Excluded, DerivedNames);
	}

	FARFilter Filter;
	Filter.ClassNames.Add(UBlueprint::StaticClass()->GetFName());
	Filter.bRecursiveClasses = true;
	Filter.bRecursivePaths = true;

	TArray<FAssetData> AssetList;
	AssetRegistry.GetAssets(Filter, AssetList);

	// Iterate over retrieved blueprint assets
	for (auto const& Asset : AssetList)
	{
		// Get the the class this blueprint generates (this is stored as a full path)
		if (auto GeneratedClassPathPtr = Asset.TagsAndValues.Find(TEXT("GeneratedClass")))
		{
			// Convert path to just the name part
			const FString ClassObjectPath = FPackageName::ExportTextPathToObjectPath(*GeneratedClassPathPtr);
			const FString ClassName = FPackageName::ObjectPathToObjectName(ClassObjectPath);

			// Check if this class is in the derived set
			if (!DerivedNames.Contains(*ClassName))
			{
				continue;
			}

			// Load the asset so that it can be added to quests
			if (!Asset.IsAssetLoaded())
			{
				FStringAssetReference Path(ClassObjectPath);
				Path.TryLoad();
			}

			// Store using the path to the generated class
			Subclasses.Add(TAssetSubclassOf<UObject>(FStringAssetReference(ClassObjectPath)));
		}
	}
}
}  // namespace Utils



TArray<UClass*> UGenericGraphSchema::QuestNodeClasses;
bool UGenericGraphSchema::bQuestNodeClassesInitialized = false;

//----------------------------------------------------------------------------------------------------------------------------------------------------

UGenericGraphSchema::UGenericGraphSchema(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UGenericGraphSchema::ConnectionCausesLoop(const UEdGraphPin* InputPin, const UEdGraphPin* OutputPin) const
{
	UGenericGraphNode* InputNode = Cast<UGenericGraphNode>(InputPin->GetOwningNode());

	if (!InputNode)
	{
		return false;
	}

	// Only nodes representing QuestNodes have outputs
	UQuestGraphNode* OutputNode = Cast<UQuestGraphNode>(OutputPin->GetOwningNode());

	if (!OutputNode)
	{
		return false;
	}

	if (OutputNode->GenericNode && InputNode->GenericNode)
	{
		const auto& OutputNodeInputPins = OutputNode->GetAllPins().FilterByPredicate([](const UEdGraphPin* Pin) { return Pin->Direction == EGPD_Input;
		});
		const auto& InputNodeInputPins =
			InputNode->GetAllPins().FilterByPredicate([InputPin](const UEdGraphPin* Pin) { return Pin->Direction == EGPD_Input; });

		if (OutputNodeInputPins.Num() == 0 || OutputNodeInputPins[0]->LinkedTo.Num() == 0)
		{
			return false;
		}

		if (InputNodeInputPins.Num() == 0 || InputNodeInputPins[0]->LinkedTo.Num() == 0)
		{
			return false;
		}

		UQuestGraphNode* ParentNode = CastChecked<UQuestGraphNode>(OutputNodeInputPins[0]->LinkedTo[0]->GetOwningNode());
		if (OutputNodeInputPins[0]->LinkedTo[0]->GetOwningNode() == InputNodeInputPins[0]->LinkedTo[0]->GetOwningNode() && ParentNode->GenericNode)
		{
			const auto ParentNodeAsParallel = CastChecked<UQuestNodeParallel>(ParentNode->GenericNode);
			return ParentNodeAsParallel != nullptr;
		}
	}

	// Simple connection to root node
	return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphSchema::GetPaletteActions(FGraphActionMenuBuilder& ActionMenuBuilder, const TArray<UClass*>& Filter) const
{
	GetAllGenericNodeActions(ActionMenuBuilder, false, Filter);
	GetCommentAction(ActionMenuBuilder);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphSchema::TryConnectNodes(const TArray<UGenericNode*>& OutputNodes, UGenericNode* InputNode) const
{
	for (int32 Index = 0; Index < OutputNodes.Num(); Index++)
	{
		UQuestGraphNode* InputGraphNode = CastChecked<UQuestGraphNode>(InputNode->GraphNode);
		UQuestGraphNode* OutputGraphNode = CastChecked<UQuestGraphNode>(OutputNodes[Index]->GraphNode);

		const bool InputNodeIsParallel = CastChecked<UQuestNodeParallel>(InputGraphNode) ? true : false;
		const bool OutputNodeIsParallel = CastChecked<UQuestNodeParallel>(OutputGraphNode) ? true : false;

		// Connections between two parallel nodes are not supported
		if (!InputNodeIsParallel && !OutputNodeIsParallel)
		{
			TryCreateConnection(InputGraphNode->GetInputPin(Index),
					CastChecked<UQuestGraphNode>(OutputNodes[Index]->GraphNode)->GetOutputPins()[0]);
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphSchema::GetGraphContextActions(FGraphContextMenuBuilder& ContextMenuBuilder) const
{
	TArray<UClass*> Filter;
	GetAllGenericNodeActions(ContextMenuBuilder, true, Filter);

	GetCommentAction(ContextMenuBuilder, ContextMenuBuilder.CurrentGraph);
	

	if (!ContextMenuBuilder.FromPin && FQuestEditorUtilities::CanPasteNodes(ContextMenuBuilder.CurrentGraph))
	{
		TSharedPtr<FGenericGraphSchemaAction_Paste> NewAction(new FGenericGraphSchemaAction_Paste(FText::GetEmpty(), LOCTEXT("PasteHereAction", "Paste here"), FText::GetEmpty(), 0));
		ContextMenuBuilder.AddAction(NewAction);
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphSchema::GetContextMenuActions(class UToolMenu* Menu, class UGraphNodeContextMenuContext* Context) const
{
	if (Context->Pin)
	{
		{
			FToolMenuSection& Section = Menu->AddSection("QuestGraphSchemaPinActions", LOCTEXT("PinActionsMenuHeader", "Pin Actions"));
			// Only display the 'Break Link' option if there is a link to break!
			if (Context->Pin->LinkedTo.Num() > 0)
			{
				Section.AddMenuEntry(FGraphEditorCommands::Get().BreakPinLinks);
			}
		}
	}
	else if (Context->Node)
	{
		const UQuestGraphNode* QuestGraphNode = Cast<const UQuestGraphNode>(Context->Node);
		{
			FToolMenuSection& Section = Menu->AddSection("QuestGraphSchemaNodeActions", LOCTEXT("NodeActionsMenuHeader", "Node Actions"));
			Section.AddMenuEntry(FGraphEditorCommands::Get().BreakNodeLinks);
		}
	}

	Super::GetContextMenuActions(Menu, Context);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FLinearColor UGenericGraphSchema::GetPinTypeColor(const FEdGraphPinType& PinType) const
{
	return FLinearColor::White;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

const FPinConnectionResponse UGenericGraphSchema::CanCreateConnection(const UEdGraphPin* PinA, const UEdGraphPin* PinB) const
{
	// Make sure the pins are not on the same node
	if (PinA->GetOwningNode() == PinB->GetOwningNode())
	{
		return FPinConnectionResponse(CONNECT_RESPONSE_DISALLOW, LOCTEXT("ConnectionSameNode", "Both are on the same node"));
	}

	if (PinA->PinType.PinCategory != PinB->PinType.PinCategory)
	{
		return FPinConnectionResponse(CONNECT_RESPONSE_DISALLOW, LOCTEXT("ConnectionPinUnEqual", "Pins are different"));
	}

	// Compare the directions
	const UEdGraphPin* InputPin = nullptr;
	const UEdGraphPin* OutputPin = nullptr;

	if (!CategorizePinsByDirection(PinA, PinB, /* out */ InputPin, /* out */ OutputPin))
	{
		return FPinConnectionResponse(CONNECT_RESPONSE_DISALLOW, LOCTEXT("ConnectionIncompatible", "Directions are not compatible"));
	}

	// Break existing connections on inputs only - multiple output connections are acceptable
	if (OutputPin->LinkedTo.Num() > 0)
	{
		ECanCreateConnectionResponse ReplyBreakOutputs;
		if (OutputPin == PinA)
		{
			ReplyBreakOutputs = CONNECT_RESPONSE_BREAK_OTHERS_A;
		}
		else
		{
			ReplyBreakOutputs = CONNECT_RESPONSE_BREAK_OTHERS_B;
		}
		return FPinConnectionResponse(ReplyBreakOutputs, LOCTEXT("ConnectionReplace", "Replace existing connections"));
	}

	if (ConnectionCausesLoop(InputPin, OutputPin))
	{
		return FPinConnectionResponse(CONNECT_RESPONSE_DISALLOW, LOCTEXT("ConnectionLoop", "Connection would cause loop"));
	}

	return FPinConnectionResponse(CONNECT_RESPONSE_MAKE, TEXT(""));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UGenericGraphSchema::ShouldHidePinDefaultValue(UEdGraphPin* Pin) const
{
	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphSchema::BreakNodeLinks(UEdGraphNode& TargetNode) const
{
	Super::BreakNodeLinks(TargetNode);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphSchema::BreakPinLinks(UEdGraphPin& TargetPin, bool bSendsNodeNotifcation) const
{
	const FScopedTransaction Transaction(NSLOCTEXT("UnrealEd", "GraphEd_BreakPinLinks", "Break Pin Links"));

	Super::BreakPinLinks(TargetPin, bSendsNodeNotifcation);

	// if this would notify the node then we need to compile the QuestGraph
	if (bSendsNodeNotifcation)
	{
		CastChecked<UGenericGraph>(TargetPin.GetOwningNode()->GetGraph())->GetModel()->CompileQuestNodesFromGraphNodes();
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphSchema::GetAllGenericNodeActions(
	FGraphActionMenuBuilder& ActionMenuBuilder, bool bShowSelectedActions, const TArray<UClass*>& IgnoreList) const
{
	FText SelectedItemText;

	TArray<TAssetSubclassOf<UObject>> Subclasses;
	GetPlacableNodes(Subclasses, IgnoreList);

	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");

	for (const auto Subclass : Subclasses)
	{		
		const FText Name = FText::FromString(Subclass->GetDescription());
		{
			FFormatNamedArguments Arguments;
			Arguments.Add(TEXT("Name"), Name);
			const FText AddToolTip = FText::Format(LOCTEXT("NewQuestGraphNodeTooltip", "Adds {Name} node here"), Arguments);

			const FText Category = FText::FromString(Subclass->GetSuperClass()->GetDescription());
			TSharedPtr<FGenericGraphSchemaAction_NewNode> NewNodeAction(
				new FGenericGraphSchemaAction_NewNode(Category, Name, AddToolTip, 0));
			ActionMenuBuilder.AddAction(NewNodeAction);

			NewNodeAction->GenericNodeClass = Subclass.Get();
		}

		/*
		const FText Name = FText::FromName(Subclass->GetFName());
		FFormatNamedArguments Arguments;
		Arguments.Add(TEXT("Name"), Name);
		const FText AddToolTip = FText::Format(LOCTEXT("NewQuestGraphNodeTooltip", "Adds {Name} node here"), Arguments);


		const FText Category = FText::FromString(Subclass->GetSuperClass()->GetDescription());
																	
		TSharedPtr<FGenericGraphSchemaAction_NewNode> NewNodeAction(
			new FGenericGraphSchemaAction_NewNode(Category, Name, AddToolTip, 0));
		ActionMenuBuilder.AddAction(NewNodeAction);

		NewNodeAction->GenericNodeClass = Subclass.Get();
		*/
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericGraphSchema::GetCommentAction(FGraphActionMenuBuilder& ActionMenuBuilder, const UEdGraph* CurrentGraph) const
{
	if (!ActionMenuBuilder.FromPin)
	{
		const bool bIsManyNodesSelected = CurrentGraph ? (FQuestEditorUtilities::GetNumberOfSelectedNodes(CurrentGraph) > 0) : false;
		const FText MenuDescription = bIsManyNodesSelected ? LOCTEXT("CreateCommentAction", "Create Comment from Selection") : LOCTEXT("AddCommentAction", "Add Comment...");
		const FText ToolTip = LOCTEXT("CreateCommentToolTip", "Creates a comment.");

		TSharedPtr<FGenericGraphSchemaAction_NewComment> NewAction(new FGenericGraphSchemaAction_NewComment(FText::FromString("Structure"), MenuDescription, ToolTip, 0));
		ActionMenuBuilder.AddAction(NewAction);
	}
}

void UGenericGraphSchema::GetPlacableNodes(TArray<TAssetSubclassOf<UObject>>& Nodes, const TArray<UClass*>& IgnoreList) const
{
	Utils::GetAllBlueprintSubclasses(Nodes, UGenericNode::StaticClass(), true, IgnoreList);
	Utils::GetAllNativeSubclasses(Nodes, UGenericNode::StaticClass(), false, IgnoreList);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText UGenericGraphSchema::GetFirstNonAbstractSuperClassDescription(UClass* Class)
{
	if (Class->GetSuperClass() == nullptr)
	{
		return FText();
	}

	if (Class->GetSuperClass()->HasAnyClassFlags(CLASS_Abstract))
	{
		return GetFirstNonAbstractSuperClassDescription(Class->GetSuperClass());
	}

	return FText::FromString(Class->GetDescription());
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

int32 UGenericGraphSchema::GetNodeSelectionCount(const UEdGraph* Graph) const
{
	return FQuestEditorUtilities::GetNumberOfSelectedNodes(Graph);
}

#undef LOCTEXT_NAMESPACE

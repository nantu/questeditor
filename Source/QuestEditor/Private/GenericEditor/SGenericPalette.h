// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Widgets/DeclarativeSyntaxSupport.h"
#include "SGraphPalette.h"

//////////////////////////////////////////////////////////////////////////
class SQuestPalette : public SGraphPalette
{
public:
    SLATE_BEGIN_ARGS(SQuestPalette) {};
    SLATE_END_ARGS()

    void Construct(const FArguments& InArgs);

protected:
    /** Callback used to populate all actions list in SGraphActionMenu */
    void CollectAllActions(FGraphActionListBuilderBase& OutAllActions) override;
};

#ifdef TRUE
class SGenericPalette : public SCompoundWidget
{
	SLATE_BEGIN_ARGS(SBlueprintPalette){};
	SLATE_END_ARGS()

	/**
	 * Creates the slate widget that represents a list of available actions for
	 * the specified blueprint.
	 *
	 * @param  InArgs				A set of slate arguments, defined above.
	 * @param  InBlueprintEditor	A pointer to the blueprint editor that this palette belongs to.
	 */
	void Construct(const FArguments& InArgs, TWeakPtr<FBlueprintEditor> InBlueprintEditor);

private:
	/**
	 * Saves off the user's new sub-palette configuration (so as to not annoy
	 * them by reseting it every time they open the blueprint editor).
	 */
	void OnSplitterResized() const;

	TSharedPtr<SWidget> FavoritesWrapper;
	TSharedPtr<SSplitter> PaletteSplitter;
	TSharedPtr<SWidget> LibraryWrapper;
};
#endif
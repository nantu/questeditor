// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SGenericActionMenu.h"
#include "BlueprintActionMenuBuilder.h"
#include "BlueprintActionMenuUtils.h"
#include "BlueprintPaletteFavorites.h"
#include "Condition/ConditionNode.h"
#include "EdGraphSchema_K2.h"
#include "EditorStyleSet.h"
#include "Framework/Application/SlateApplication.h"
#include "GenericActionFilter.h"
#include "GenericEditor.h"
#include "GenericGraph/GenericGraphSchema.h"
#include "QuestNode.h"
#include "SGenericPalette.h"
#include "SGraphActionMenu.h"
#include "Widgets/Images/SImage.h"
#include "Widgets/Input/SCheckBox.h"
#include "Widgets/Input/SComboButton.h"
#include "Widgets/Layout/SBox.h"
#include "Widgets/SBoxPanel.h"
#include "Widgets/Text/STextBlock.h"

#define LOCTEXT_NAMESPACE "SBlueprintGraphContextMenu"

/*-----------------------------------------------------------------------------
	SGenericActionMenu implementation.
-----------------------------------------------------------------------------*/

SGenericActionMenu::~SGenericActionMenu()
{
	OnClosedCallback.ExecuteIfBound();
	OnCloseReasonCallback.ExecuteIfBound(
		bActionExecuted, ContextToggleIsChecked() == ECheckBoxState::Checked, DraggedFromPins.Num() > 0);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void SGenericActionMenu::Construct(const FArguments& InArgs, TSharedPtr<FGenericEditor> InEditor)
{
	bActionExecuted = false;

	this->GraphObj = InArgs._GraphObj;
	this->DraggedFromPins = InArgs._DraggedFromPins;
	this->NewNodePosition = InArgs._NewNodePosition;
	this->OnClosedCallback = InArgs._OnClosedCallback;
	this->bAutoExpandActionMenu = InArgs._AutoExpandActionMenu;
	this->EditorPtr = InEditor;
	this->OnCloseReasonCallback = InArgs._OnCloseReason;

	// Generate the context display; showing the user what they're picking something for
	//@TODO: Should probably be somewhere more schema-sensitive than the graph panel!
	FSlateColor TypeColor;
	FString TypeOfDisplay;
	const FSlateBrush* ContextIcon = nullptr;

	if (DraggedFromPins.Num() == 1)
	{
		UEdGraphPin* OnePin = DraggedFromPins[0];

		const UEdGraphSchema* Schema = OnePin->GetSchema();
		const UEdGraphSchema_K2* K2Schema = GetDefault<UEdGraphSchema_K2>();

		if (!Schema->IsA(UEdGraphSchema_K2::StaticClass()) || !K2Schema->IsExecPin(*OnePin))
		{
			// Get the type color and icon
			TypeColor = Schema->GetPinTypeColor(OnePin->PinType);
			ContextIcon =
				FEditorStyle::GetBrush(OnePin->PinType.IsArray() ? TEXT("Graph.ArrayPin.Connected") : TEXT("Graph.Pin.Connected"));
		}
	}

	FGenericActionContext MenuContext;
	ConstructActionContext(MenuContext);

	TSharedPtr<SComboButton> TargetContextSubMenuButton;
	// @TODO: would be nice if we could use a checkbox style for this, and have a different state for open/closed
	SAssignNew(TargetContextSubMenuButton, SComboButton)
		.MenuPlacement(MenuPlacement_MenuRight)
		.HasDownArrow(false)
		.ButtonStyle(FEditorStyle::Get(), "BlueprintEditor.ContextMenu.TargetsButton");

	// Build the widget layout
	SBorder::Construct(
		SBorder::FArguments()
			.BorderImage(FEditorStyle::GetBrush("Menu.Background"))
			.Padding(5)[
				// Achieving fixed width by nesting items within a fixed width box.
				SNew(SBox).WidthOverride(400).HeightOverride(
					400)[SNew(SVerticalBox)

						 // TYPE OF SEARCH INDICATOR
						 + SVerticalBox::Slot().AutoHeight().Padding(2, 2, 2,
							   5)[SNew(SHorizontalBox)

								  // Search context description
								  + SHorizontalBox::Slot().AutoWidth().VAlign(
										VAlign_Center)[SNew(STextBlock)
														   .Text(this, &SGenericActionMenu::GetSearchContextDesc)
														   .Font(FEditorStyle::GetFontStyle(
															   FName("BlueprintEditor.ActionMenu.ContextDescriptionFont")))
														   .ToolTip(IDocumentation::Get()->CreateToolTip(
															   LOCTEXT("BlueprintActionMenuContextTextTooltip",
																   "Describes the current context of the action list"),
															   nullptr, TEXT("Shared/Editors/BlueprintEditor"),
															   TEXT("BlueprintActionMenuContextText")))
														   .WrapTextAt(280)]

								  // Context Toggle
								  + SHorizontalBox::Slot()
										.HAlign(HAlign_Right)
										.VAlign(VAlign_Center)[SNew(SCheckBox)
																   .OnCheckStateChanged(
																	   this, &SGenericActionMenu::OnContextToggleChanged)
																   .IsChecked(this, &SGenericActionMenu::ContextToggleIsChecked)
																   .ToolTip(IDocumentation::Get()->CreateToolTip(
																	   LOCTEXT("BlueprintActionMenuContextToggleTooltip",
																		   "Should the list be filtered to only actions that make "
																		   "sense in the current context?"),
																	   nullptr, TEXT("Shared/Editors/BlueprintEditor"),
																	   TEXT("BlueprintActionMenuContextToggle")))
																	   [SNew(STextBlock)
																			   .Text(LOCTEXT("BlueprintActionMenuContextToggle",
																				   "Context Sensitive"))]]

								  + SHorizontalBox::Slot()
										.HAlign(HAlign_Right)
										.VAlign(VAlign_Center)
										.AutoWidth()
										.Padding(3.f, 0.f, 0.f, 0.f)[TargetContextSubMenuButton.ToSharedRef()]]

						 // ACTION LIST
						 + SVerticalBox::Slot()[SAssignNew(GraphActionMenu, SGraphActionMenu)
													.OnActionSelected(this, &SGenericActionMenu::OnActionSelected)
													.OnCollectAllActions(this, &SGenericActionMenu::CollectAllActions)
													.DraggedFromPins(DraggedFromPins)]]]);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

EVisibility SGenericActionMenu::GetTypeImageVisibility() const 
{
	if (DraggedFromPins.Num() == 1 && EditorPtr.Pin()->GetIsContextSensitive())
	{
		UEdGraphPin* OnePin = DraggedFromPins[0];

		const UEdGraphSchema* Schema = OnePin->GetSchema();
		const UEdGraphSchema_K2* K2Schema = GetDefault<UEdGraphSchema_K2>();

		if (!Schema->IsA(UEdGraphSchema_K2::StaticClass()) || !K2Schema->IsExecPin(*OnePin))
		{
			return EVisibility::Visible;
		}
	}
	return EVisibility::Collapsed;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText SGenericActionMenu::GetSearchContextDesc() const
{
	bool bIsContextSensitive = EditorPtr.Pin()->GetIsContextSensitive();
	bool bHasPins = DraggedFromPins.Num() > 0;
	if (!bIsContextSensitive)
	{
		return LOCTEXT("MenuPrompt_AllPins", "All Possible Actions");
	}
	else if (!bHasPins)
	{
		return LOCTEXT("MenuPrompt_BlueprintActions", "All Actions for this quest");
	}
	else if (DraggedFromPins.Num() == 1)
	{
		UEdGraphPin* OnePin = DraggedFromPins[0];

		// Get the type string
		const FString Type = OnePin->PinType.PinCategory == UEdGraphSchema_K2::PC_Boolean ? "Condition" : "Task";

		if (OnePin->Direction == EGPD_Input)
		{
			return FText::FromString("Actions providing a " + Type);
		}
		else
		{
			return FText::FromString("Actions taking a " + Type);
		}
	}
	else
	{
		return FText::Format(LOCTEXT("MenuPrompt_ManyPins", "Actions for {0} pins"), FText::AsNumber(DraggedFromPins.Num()));
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void SGenericActionMenu::OnContextToggleChanged(ECheckBoxState CheckState)
{
	EditorPtr.Pin()->SetIsContextSensitive(CheckState == ECheckBoxState::Checked);
	GraphActionMenu->RefreshAllActions(true, false);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void SGenericActionMenu::OnContextTargetsChanged(uint32)
{
	GraphActionMenu->RefreshAllActions(true, false);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

ECheckBoxState SGenericActionMenu::ContextToggleIsChecked() const
{
	return EditorPtr.Pin()->GetIsContextSensitive() ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void SGenericActionMenu::CollectAllActions(FGraphActionListBuilderBase& OutAllActions)
{
	check(EditorPtr.IsValid());
	TSharedPtr<FGenericEditor> GenericEditor = EditorPtr.Pin();
	bool const bIsContextSensitive = GenericEditor->GetIsContextSensitive();

	FGenericActionContext FilterContext;
	ConstructActionContext(FilterContext);

	const UGenericGraphSchema* Schema = GetDefault<UGenericGraphSchema>();

	FGraphActionMenuBuilder ActionMenuBuilder;

	// Determine all possible actions
	TArray<UClass*> Filter;
	if (FilterContext.Pins.Num() == 1)
	{
		if (FilterContext.Pins[0]->PinType.PinCategory == UEdGraphSchema_K2::PC_Boolean)
		{
			Filter.Add(UQuestNode::StaticClass());
		}
		else 
		{
			Filter.Add(UConditionNode::StaticClass());
		}
	}
	Schema->GetPaletteActions(ActionMenuBuilder, Filter);

	OutAllActions.Append(ActionMenuBuilder);	// @TODO: Avoid this copy	
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void SGenericActionMenu::ConstructActionContext(FGenericActionContext& ContextDescOut)
{
	check(EditorPtr.IsValid());
	TSharedPtr<FGenericEditor> GenericEditor = EditorPtr.Pin();
	bool const bIsContextSensitive = GenericEditor->GetIsContextSensitive();

	// we still want context from the graph (even if the user has unchecked
	// "Context Sensitive"), otherwise the user would be presented with nodes
	// that can't be placed in the graph... if the user isn't being presented
	// with a valid node, then fix it up in filtering
	ContextDescOut.Graphs.Add(GraphObj);

	UGenericContainer* Model = GenericEditor->GetModel();
	ContextDescOut.Containers.Add(Model);

	if (bIsContextSensitive)
	{
		ContextDescOut.Pins = DraggedFromPins;
		
		for (FGraphPanelSelectionSet::TConstIterator SelectedIter(GenericEditor->GetSelectedNodes()); SelectedIter; ++SelectedIter)
		{
			ContextDescOut.SelectedObjects.Add(*SelectedIter);
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TSharedRef<SEditableTextBox> SGenericActionMenu::GetFilterTextBox()
{
	return GraphActionMenu->GetFilterTextBox();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void SGenericActionMenu::OnActionSelected(
	const TArray<TSharedPtr<FEdGraphSchemaAction> >& SelectedAction, ESelectInfo::Type InSelectionType)
{
	if (InSelectionType == ESelectInfo::OnMouseClick || InSelectionType == ESelectInfo::OnKeyPress || SelectedAction.Num() == 0)
	{
		for (int32 ActionIndex = 0; ActionIndex < SelectedAction.Num(); ActionIndex++)
		{
			if (SelectedAction[ActionIndex].IsValid() && GraphObj != nullptr)
			{
				// Don't dismiss when clicking on dummy action
				if (!bActionExecuted && (SelectedAction[ActionIndex]->GetTypeId() != FEdGraphSchemaAction_Dummy::StaticGetTypeId()))
				{
					FSlateApplication::Get().DismissAllMenus();
					bActionExecuted = true;
				}

				UEdGraphNode* ResultNode = SelectedAction[ActionIndex]->PerformAction(GraphObj, DraggedFromPins, NewNodePosition);

				if (ResultNode != nullptr)
				{
					NewNodePosition.Y += UEdGraphSchema_K2::EstimateNodeHeight(ResultNode);
				}
			}
		}
	}
}

#undef LOCTEXT_NAMESPACE

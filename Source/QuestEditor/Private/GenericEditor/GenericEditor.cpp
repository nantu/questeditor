// Copyright (c) 2020 Gil Engel

#include "GenericEditor.h"
#include "ConditionEditor/Nodes/ConditionGraphNode.h"
#include "EdGraph/EdGraphNode.h"
#include "EdGraphSchema_K2_Actions.h"
#include "EdGraphUtilities.h"
#include "Editor.h"
#include "Framework/Commands/GenericCommands.h"
#include "Framework/MultiBox/MultiBoxBuilder.h"
#include "GenericGraph/GenericGraph.h"
#include "GenericGraph/GenericGraphNode_Root.h"
#include "GenericGraph/GenericGraphSchema.h"
#include "GraphEditor.h"
#include "GraphEditorActions.h"
#include "HAL/PlatformApplicationMisc.h"
#include "IDetailsView.h"
#include "Kismet2/BlueprintEditorUtils.h"
#include "PropertyEditorModule.h"
#include "Quest.h"
#include "QuestEditorModule.h"
#include "QuestGraph/QuestGraphNode.h"
#include "QuestGraph/QuestGraphSchema.h"
#include "QuestGraphEditorCommands.h"
#include "ScopedTransaction.h"
#include "SGenericActionMenu.h"
#include "SGenericEditorToolbar.h"
#include "SGenericPalette.h"
#include "SNodePanel.h"
#include "Widgets/Docking/SDockTab.h"

#define LOCTEXT_NAMESPACE "QuestEditor"

/*-----------------------------------------------------------------------------
	FGenericEditor implementation.
-----------------------------------------------------------------------------*/

const FName FGenericEditor::GraphCanvasTabId(TEXT("QuestEditor_GraphCanvas"));
const FName FGenericEditor::PropertiesTabId(TEXT("QuestEditor_Properties"));
const FName FGenericEditor::PaletteTabId(TEXT("QuestEditor_Palette"));

FGenericEditor::FGenericEditor() : IsContextSensitive(true) , Model(nullptr)
{
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FGenericEditor::~FGenericEditor()
{
	GEditor->UnregisterForUndo(this);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::RegisterTabSpawners(const TSharedRef<class FTabManager>& InTabManager)
{
	WorkspaceMenuCategory = InTabManager->AddLocalWorkspaceMenuCategory(LOCTEXT("WorkspaceMenu_QuestEditor", "Quest Editor"));
	auto WorkspaceMenuCategoryRef = WorkspaceMenuCategory.ToSharedRef();

	FAssetEditorToolkit::RegisterTabSpawners(InTabManager);

	InTabManager->RegisterTabSpawner(GraphCanvasTabId, FOnSpawnTab::CreateSP(this, &FGenericEditor::SpawnTab_GraphCanvas))
		.SetDisplayName(LOCTEXT("GraphCanvasTab", "Viewport"))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FEditorStyle::GetStyleSetName(), "GraphEditor.EventGraph_16x"));

	InTabManager->RegisterTabSpawner(PropertiesTabId, FOnSpawnTab::CreateSP(this, &FGenericEditor::SpawnTab_Properties))
		.SetDisplayName(LOCTEXT("DetailsTab", "Details"))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FEditorStyle::GetStyleSetName(), "LevelEditor.Tabs.Details"));

	InTabManager->RegisterTabSpawner(PaletteTabId, FOnSpawnTab::CreateSP(this, &FGenericEditor::SpawnTab_Palette))
		.SetDisplayName(LOCTEXT("PaletteTab", "Palette"))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FEditorStyle::GetStyleSetName(), "Kismet.Tabs.Palette"));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::UnregisterTabSpawners(const TSharedRef<class FTabManager>& InTabManager)
{
	FAssetEditorToolkit::UnregisterTabSpawners(InTabManager);

	InTabManager->UnregisterTabSpawner(GraphCanvasTabId);
	InTabManager->UnregisterTabSpawner(PropertiesTabId);
	InTabManager->UnregisterTabSpawner(PaletteTabId);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::InitGenericEditor(
	const EToolkitMode::Type Mode, const TSharedPtr<class IToolkitHost>& InitToolkitHost, UObject* ObjectToEdit)
{
	Model = CastChecked<UGenericContainer>(ObjectToEdit);

	// Support undo/redo
	Model->SetFlags(RF_Transactional);

	GEditor->RegisterForUndo(this);

	FGraphEditorCommands::Register();
	FQuestGraphEditorCommands::Register();

	BindGraphCommands();

	CreateInternalWidgets();

	// clang-format off
	const TSharedRef<FTabManager::FLayout> StandaloneDefaultLayout = FTabManager::NewLayout("Standalone_QuestEditor_Layout_v1")
		->AddArea
		(
			FTabManager::NewPrimaryArea()->SetOrientation(Orient_Vertical)
			->Split
			(
				FTabManager::NewStack()
				->SetSizeCoefficient(0.1f)
				->AddTab(GetToolbarTabId(), ETabState::OpenedTab)->SetHideTabWell(true)
			)
			->Split
			(
				FTabManager::NewSplitter()->SetOrientation(Orient_Horizontal)->SetSizeCoefficient(0.9f)
				->Split
				(
					FTabManager::NewSplitter()->SetOrientation(Orient_Vertical)
					->Split
					(
						FTabManager::NewStack()
						->SetSizeCoefficient(0.65f)
						->AddTab(PropertiesTabId, ETabState::OpenedTab)
					)
					->Split
					(
						FTabManager::NewStack()
						->SetSizeCoefficient(0.35f)
						->AddTab(PropertiesTabId, ETabState::OpenedTab)
					)
				)
				->Split
				(
					    FTabManager::NewStack()
						->SetSizeCoefficient(0.65f)
						->AddTab(GraphCanvasTabId, ETabState::OpenedTab)
						->SetHideTabWell(true)
				)
				->Split
				(
					FTabManager::NewStack()
					->SetSizeCoefficient(0.125f)
					->AddTab(PaletteTabId, ETabState::OpenedTab)
				)));

	// clang-format on

	const bool bCreateDefaultStandaloneMenu = true;
	const bool bCreateDefaultToolbar = true;
	FAssetEditorToolkit::InitAssetEditor(Mode, InitToolkitHost, TEXT("GraphEditorIdentifier"), StandaloneDefaultLayout,
		bCreateDefaultStandaloneMenu, bCreateDefaultToolbar, ObjectToEdit, false);

	ExtendToolbar();
	RegenerateMenusAndToolbars();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::ExtendToolbar()
{
	if (!Toolbar.IsValid())
	{
		Toolbar = MakeShareable(new FGenericEditorToolbar(SharedThis(this)));
	}

	TSharedPtr<FExtender> ToolbarExtender = MakeShareable(new FExtender);
	Toolbar->AddCompileToolbar(ToolbarExtender);

	AddToolbarExtender(ToolbarExtender);

	FQuestEditorModule* QuestEditorModule = &FModuleManager::LoadModuleChecked<FQuestEditorModule>("QuestEditor");
	AddToolbarExtender(QuestEditorModule->GetExtensibilityManagers().ToolBarExtensibilityManager->GetAllExtenders(
		GetToolkitCommands(), GetEditingObjects()));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

UGenericContainer* FGenericEditor::GetModel() const
{
	return Model;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::SetSelection(TArray<UObject*> SelectedObjects)
{
	if (QuestProperties.IsValid())
	{
		QuestProperties->SetObjects(SelectedObjects);
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FGenericEditor::GetBoundsForSelectedNodes(class FSlateRect& Rect, float Padding)
{
	return QuestGraphEditor->GetBoundsForSelectedNodes(Rect, Padding);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

int32 FGenericEditor::GetNumberOfSelectedNodes() const
{
	return QuestGraphEditor->GetSelectedNodes().Num();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FName FGenericEditor::GetToolkitFName() const
{
	return FName("QuestEditor");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText FGenericEditor::GetBaseToolkitName() const
{
	return LOCTEXT("AppLabel", "Quest Editor");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FString FGenericEditor::GetWorldCentricTabPrefix() const
{
	return LOCTEXT("WorldCentricTabPrefix", "Quest ").ToString();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FLinearColor FGenericEditor::GetWorldCentricTabColorScale() const
{
	return FLinearColor(0.3f, 0.2f, 0.5f, 0.5f);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TSharedRef<SDockTab> FGenericEditor::SpawnTab_GraphCanvas(const FSpawnTabArgs& Args)
{
	check(Args.GetTabId() == GraphCanvasTabId);

	TSharedRef<SDockTab> SpawnedTab = SNew(SDockTab)
		.Label(LOCTEXT("QuestGraphCanvasTitle", "Viewport"));

	if (QuestGraphEditor.IsValid())
	{
		SpawnedTab->SetContent(QuestGraphEditor.ToSharedRef());
	}

	return SpawnedTab;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TSharedRef<SDockTab> FGenericEditor::SpawnTab_Properties(const FSpawnTabArgs& Args)
{
	check(Args.GetTabId() == PropertiesTabId);

	return SNew(SDockTab)
		.Icon(FEditorStyle::GetBrush("LevelEditor.Tabs.Details"))
		.Label(LOCTEXT("QuestDetailsTitle", "Details"))
		[
			QuestProperties.ToSharedRef()
		];
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TSharedRef<SDockTab> FGenericEditor::SpawnTab_Palette(const FSpawnTabArgs& Args)
{
	check(Args.GetTabId() == PaletteTabId);

	return SNew(SDockTab)
		.Icon(FEditorStyle::GetBrush("Kismet.Tabs.Palette"))
		.Label(LOCTEXT("QuestPaletteTitle", "Palette"))
		[
			Palette.ToSharedRef()
		];
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FActionMenuContent FGenericEditor::OnCreateGraphActionMenu(UEdGraph* InGraph, const FVector2D& InNodePosition,
	const TArray<UEdGraphPin*>& InDraggedPins, bool bAutoExpand, SGraphEditor::FActionMenuClosed InOnMenuClosed)
{


	TSharedRef<SGenericActionMenu> ActionMenu = SNew(SGenericActionMenu, SharedThis(this))
													.GraphObj(InGraph)
													.NewNodePosition(InNodePosition)
													.DraggedFromPins(InDraggedPins)
													.AutoExpandActionMenu(bAutoExpand)
													.OnClosedCallback(InOnMenuClosed);

	return FActionMenuContent(ActionMenu, ActionMenu->GetFilterTextBox());
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::SetIsContextSensitive(bool InIsContextSensitive)
{
	IsContextSensitive = InIsContextSensitive;
}

void FGenericEditor::PostUndo(bool bSuccess)
{
	if (QuestGraphEditor.IsValid())
	{
		QuestGraphEditor->ClearSelectionSet();
		QuestGraphEditor->NotifyGraphChanged();
	}

	if (ConditionGraphEditor.IsValid())
	{
		ConditionGraphEditor->ClearSelectionSet();
		ConditionGraphEditor->NotifyGraphChanged();
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::NotifyPostChange(const FPropertyChangedEvent& PropertyChangedEvent, class FProperty* PropertyThatChanged)
{
	if (QuestGraphEditor.IsValid() && PropertyChangedEvent.ChangeType != EPropertyChangeType::Interactive)
	{
		QuestGraphEditor->NotifyGraphChanged();
	}

	CompileQuest();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::CreateInternalWidgets()
{
	QuestGraphEditor = CreateGraphEditorWidget();	
	
	FDetailsViewArgs Args;
	Args.bHideSelectionTip = true;
	Args.NotifyHook = this;
	Args.bAllowSearch = true;

	FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	QuestProperties = PropertyModule.CreateDetailView(Args);
	QuestProperties->SetObject(Model);

	Palette = SNew(SQuestPalette);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::BindGraphCommands()
{
	const FQuestGraphEditorCommands& Commands = FQuestGraphEditorCommands::Get();

	ToolkitCommands->MapAction(
		FGenericCommands::Get().Undo,
		FExecuteAction::CreateSP(this, &FGenericEditor::UndoGraphAction));

	ToolkitCommands->MapAction(
		FGenericCommands::Get().Redo,
		FExecuteAction::CreateSP(this, &FGenericEditor::RedoGraphAction));

	// Compilation commands
	ToolkitCommands->MapAction(Commands.Compile, FExecuteAction::CreateSP(this, &FGenericEditor::CompileQuest),
		FCanExecuteAction::CreateSP(this, &FGenericEditor::CanCompileQuest));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FGenericEditor::CanDeleteInput() const
{
	if (QuestGraphEditor.IsValid())
	{
		UEdGraphPin* SelectedPin = QuestGraphEditor->GetGraphPinForMenu();
		if (ensure(SelectedPin))
		{
			UGenericGraphNode* SelectedNode = Cast<UGenericGraphNode>(SelectedPin->GetOwningNode());
			int32 ExecPinCount = SelectedNode->GetInputPins().FilterByPredicate([](const UEdGraphPin* Pin) -> bool {
										 return Pin->PinType.PinCategory == UQuestGraphSchema::PC_OptionalExec ||
												Pin->PinType.PinCategory == UEdGraphSchema_K2::PC_Exec;
			})
			.Num();

			return ExecPinCount > 1;
		}
	}

	return false;
}

void FGenericEditor::DeleteInput()
{
	if (QuestGraphEditor.IsValid())
	{
		UEdGraphPin* SelectedPin = QuestGraphEditor->GetGraphPinForMenu();
		if (ensure(SelectedPin))
		{
			UGenericGraphNode* SelectedNode = Cast<UGenericGraphNode>(SelectedPin->GetOwningNode());

			if (SelectedNode && SelectedNode == SelectedPin->GetOwningNode())
			{
				SelectedNode->RemoveInputPin(SelectedPin);
			}
		}

	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::DeleteOutput()
{
	if (QuestGraphEditor.IsValid())
	{
		UEdGraphPin* SelectedPin = QuestGraphEditor->GetGraphPinForMenu();
		if (ensure(SelectedPin))
		{
			UQuestGraphNode* SelectedNode = Cast<UQuestGraphNode>(SelectedPin->GetOwningNode());

			if (SelectedNode && SelectedNode == SelectedPin->GetOwningNode())
			{
				SelectedNode->RemoveOutputPin(SelectedPin);
			}
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FGenericEditor::CanDeleteOutput() const
{
	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::CompileQuest()
{
	UQuest* Quest = CastChecked<UQuest>(Model);

	const auto& InvalidNodes = Model->AllNodes.FilterByPredicate([](UGenericNode* Node) -> bool {

		if (UQuestNode* QuestNode = Cast<UQuestNode>(Node))
		{
			return !QuestNode->IsValid();
		}

		return false;
	});

	if (InvalidNodes.Num() > 0)
	{
		JumpToNode(InvalidNodes[0]->GraphNode);
		
		Quest->Status = EBlueprintStatus::BS_Error;
		return;
	}

	Quest->Status =  EBlueprintStatus::BS_UpToDate;

	// Update UI
	QuestGraphEditor->NotifyGraphChanged();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FGenericEditor::CanCompileQuest() const
{
	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::JumpToNode(const UEdGraphNode* Node)
{
	if (QuestGraphEditor.IsValid())
	{
		QuestGraphEditor->JumpToNode(Node);
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::AddReferencedObjects(FReferenceCollector& Collector)
{
	Collector.AddReferencedObject(Model);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TSharedRef<SGraphEditor> FGenericEditor::CreateGraphEditorWidget()
{
	if (!GraphEditorCommands.IsValid())
	{
		GraphEditorCommands = MakeShareable(new FUICommandList);

		// Editing commands
		GraphEditorCommands->MapAction(FGenericCommands::Get().SelectAll,
			FExecuteAction::CreateSP(this, &FGenericEditor::SelectAllNodes),
			FCanExecuteAction::CreateSP(this, &FGenericEditor::CanSelectAllNodes));

		GraphEditorCommands->MapAction(FGenericCommands::Get().Delete,
			FExecuteAction::CreateSP(this, &FGenericEditor::DeleteSelectedNodes),
			FCanExecuteAction::CreateSP(this, &FGenericEditor::CanDeleteNodes));

		GraphEditorCommands->MapAction(FGenericCommands::Get().Copy,
			FExecuteAction::CreateSP(this, &FGenericEditor::CopySelectedNodes),
			FCanExecuteAction::CreateSP(this, &FGenericEditor::CanCopyNodes));

		GraphEditorCommands->MapAction(FGenericCommands::Get().Cut,
			FExecuteAction::CreateSP(this, &FGenericEditor::CutSelectedNodes),
			FCanExecuteAction::CreateSP(this, &FGenericEditor::CanCutNodes));

		GraphEditorCommands->MapAction(FGenericCommands::Get().Paste,
			FExecuteAction::CreateSP(this, &FGenericEditor::PasteNodes),
			FCanExecuteAction::CreateSP(this, &FGenericEditor::CanPasteNodes));

		GraphEditorCommands->MapAction(FGenericCommands::Get().Duplicate,
			FExecuteAction::CreateSP(this, &FGenericEditor::DuplicateNodes),
			FCanExecuteAction::CreateSP(this, &FGenericEditor::CanDuplicateNodes));

		GraphEditorCommands->MapAction(FQuestGraphEditorCommands::Get().DeleteOutput,
			FExecuteAction::CreateSP(this, &FGenericEditor::DeleteOutput),
			FCanExecuteAction::CreateSP(this, &FGenericEditor::CanDeleteOutput));

		GraphEditorCommands->MapAction(FQuestGraphEditorCommands::Get().DeleteInput,
			FExecuteAction::CreateSP(this, &FGenericEditor::DeleteInput),
			FCanExecuteAction::CreateSP(this, &FGenericEditor::CanDeleteInput));
	}

	FGraphAppearanceInfo AppearanceInfo;
	AppearanceInfo.CornerText = LOCTEXT("AppearanceCornerText_Quest", "QUEST");

	SGraphEditor::FGraphEditorEvents InEvents;
	InEvents.OnSelectionChanged = SGraphEditor::FOnSelectionChanged::CreateSP(this, &FGenericEditor::OnSelectedNodesChanged);
	InEvents.OnTextCommitted = FOnNodeTextCommitted::CreateSP(this, &FGenericEditor::OnNodeTitleCommitted);
	InEvents.OnCreateActionMenu = SGraphEditor::FOnCreateActionMenu::CreateSP(this, &FGenericEditor::OnCreateGraphActionMenu);


	return SNew(SGraphEditor)
		.AdditionalCommands(GraphEditorCommands)
		.IsEditable(true)
		.Appearance(AppearanceInfo)
		.GraphToEdit(Model->GetGraph())
		.GraphEvents(InEvents)
		.AutoExpandActionMenu(true)
		.ShowGraphStateOverlay(false);

}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FGraphPanelSelectionSet FGenericEditor::GetSelectedNodes() const
{
	FGraphPanelSelectionSet CurrentSelection;
	
	if (QuestGraphEditor.IsValid())
	{
		CurrentSelection = QuestGraphEditor->GetSelectedNodes();
	}

	return CurrentSelection;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::OnSelectedNodesChanged(const TSet<class UObject*>& NewSelection)
{
	bool ParentConditionSelectionChanged = Selection.Contains(ParentConditionNode);
	Selection.Empty();

	const int32 NumOfSelectedNodes = NewSelection.Num();
	if (NumOfSelectedNodes)
	{
		for (TSet<class UObject*>::TConstIterator SetIt(NewSelection); SetIt; ++SetIt)
		{
			if (Cast<UGenericGraphNode_Root>(*SetIt))
			{
				Selection.Add(GetModel());
			}
			else if (UQuestGraphNode* GraphNode = Cast<UQuestGraphNode>(*SetIt))
			{
				Selection.Add(GraphNode->GenericNode);
			}
			else
			{
				Selection.Add(*SetIt);
			}
		}

		SetSelection(Selection);

	}
	else if (ParentConditionSelectionChanged)
	{
		Selection.Add(GetModel());     
		SetSelection(Selection);
		return;
	}

    SetSelection(Selection);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::OnNodeTitleCommitted(const FText& NewText, ETextCommit::Type CommitInfo, UEdGraphNode* NodeBeingChanged)
{
	if (NodeBeingChanged)
	{
		const FScopedTransaction Transaction(LOCTEXT("RenameNode", "Rename Node"));
		NodeBeingChanged->Modify();
		NodeBeingChanged->OnRenameNode(NewText.ToString());
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::SelectAllNodes()
{
	QuestGraphEditor->SelectAllNodes();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FGenericEditor::CanSelectAllNodes() const
{
	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::DeleteSelectedNodes()
{
	const FScopedTransaction Transaction(NSLOCTEXT("UnrealEd", "QuestEditorDeleteSelectedNode", "Delete Selected Quest Node"));

	QuestGraphEditor->GetCurrentGraph()->Modify();

	const FGraphPanelSelectionSet SelectedNodes = GetSelectedNodes();

	QuestGraphEditor->ClearSelectionSet();

	for (FGraphPanelSelectionSet::TConstIterator NodeIt(SelectedNodes); NodeIt; ++NodeIt)
	{
		UEdGraphNode* Node = CastChecked<UEdGraphNode>(*NodeIt);

		if (Node->CanUserDeleteNode())
		{
			if (UGenericGraphNode* GenericGraphNode = Cast<UGenericGraphNode>(Node))
			{
				UGenericNode* DelNode = GenericGraphNode->GenericNode;

				FBlueprintEditorUtils::RemoveNode(nullptr, GenericGraphNode, true);

				// Make sure Quest is updated to match graph
				Model->CompileQuestNodesFromGraphNodes();

				// Remove this node from the Quest's list of all QuestNodes
				Model->AllNodes.Remove(DelNode);
				Model->MarkPackageDirty();
			}
			else
			{
				FBlueprintEditorUtils::RemoveNode(nullptr, Node, true);
			}
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FGenericEditor::CanDeleteNodes() const
{
	const FGraphPanelSelectionSet SelectedNodes = GetSelectedNodes();

	if (SelectedNodes.Num() == 1)
	{
		for (FGraphPanelSelectionSet::TConstIterator NodeIt(SelectedNodes); NodeIt; ++NodeIt)
		{
			if (Cast<UGenericGraphNode_Root>(*NodeIt))
			{
				// Return false if only root node is selected, as it can't be deleted
				return false;
			}
		}
	}

	return SelectedNodes.Num() > 0;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::DeleteSelectedDuplicatableNodes()
{
	// Cache off the old selection
	const FGraphPanelSelectionSet OldSelectedNodes = GetSelectedNodes();

	// Clear the selection and only select the nodes that can be duplicated
	FGraphPanelSelectionSet RemainingNodes;
	QuestGraphEditor->ClearSelectionSet();

	for (FGraphPanelSelectionSet::TConstIterator SelectedIter(OldSelectedNodes); SelectedIter; ++SelectedIter)
	{
		UEdGraphNode* Node = Cast<UEdGraphNode>(*SelectedIter);
		if ((Node != nullptr) && Node->CanDuplicateNode())
		{
			QuestGraphEditor->SetNodeSelection(Node, true);
		}
		else
		{
			RemainingNodes.Add(Node);
		}
	}

	// Delete the duplicatable nodes
	DeleteSelectedNodes();

	// Reselect whatever's left from the original selection after the deletion
	QuestGraphEditor->ClearSelectionSet();

	for (FGraphPanelSelectionSet::TConstIterator SelectedIter(RemainingNodes); SelectedIter; ++SelectedIter)
	{
		if (UEdGraphNode* Node = Cast<UEdGraphNode>(*SelectedIter))
		{
			QuestGraphEditor->SetNodeSelection(Node, true);
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::CutSelectedNodes()
{
	CopySelectedNodes();
	// Cut should only delete nodes that can be duplicated
	DeleteSelectedDuplicatableNodes();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FGenericEditor::CanCutNodes() const
{
	return CanCopyNodes() && CanDeleteNodes();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::CopySelectedNodes()
{
	// Export the selected nodes and place the text on the clipboard
	const FGraphPanelSelectionSet SelectedNodes = GetSelectedNodes();

	FString ExportedText;

	for (FGraphPanelSelectionSet::TConstIterator SelectedIter(SelectedNodes); SelectedIter; ++SelectedIter)
	{
		if (UQuestGraphNode* Node = Cast<UQuestGraphNode>(*SelectedIter))
		{
			Node->PrepareForCopying();
		}
	}

	FEdGraphUtilities::ExportNodesToText(SelectedNodes, ExportedText);
	FPlatformApplicationMisc::ClipboardCopy(*ExportedText);

	// Make sure Quest remains the owner of the copied nodes
	for (FGraphPanelSelectionSet::TConstIterator SelectedIter(SelectedNodes); SelectedIter; ++SelectedIter)
	{
		if (UQuestGraphNode* Node = Cast<UQuestGraphNode>(*SelectedIter))
		{
			Node->PostCopyNode();
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::PasteNodesHere(const FVector2D& Location)
{
	// Undo/Redo support
	const FScopedTransaction Transaction(NSLOCTEXT("UnrealEd", "QuestEditorPaste", "Paste Quest Node"));
	Model->GetGraph()->Modify();
	Model->Modify();

	// Clear the selection set (newly pasted stuff will be selected)
	QuestGraphEditor->ClearSelectionSet();

	// Grab the text to paste from the clipboard.
	FString TextToImport;
	FPlatformApplicationMisc::ClipboardPaste(TextToImport);

	// Import the nodes
	TSet<UEdGraphNode*> PastedNodes;
	FEdGraphUtilities::ImportNodesFromText(Model->GetGraph(), TextToImport, /*out*/ PastedNodes);

	// Average position of nodes so we can move them while still maintaining relative distances to each other
	FVector2D AvgNodePosition(0.0f, 0.0f);

	for (TSet<UEdGraphNode*>::TIterator It(PastedNodes); It; ++It)
	{
		UEdGraphNode* Node = *It;
		AvgNodePosition.X += Node->NodePosX;
		AvgNodePosition.Y += Node->NodePosY;
	}

	if (PastedNodes.Num() > 0)
	{
		float InvNumNodes = 1.0f / static_cast<float>(PastedNodes.Num());
		AvgNodePosition.X *= InvNumNodes;
		AvgNodePosition.Y *= InvNumNodes;
	}

	for (TSet<UEdGraphNode*>::TIterator It(PastedNodes); It; ++It)
	{
		UEdGraphNode* Node = *It;

		if (UGenericGraphNode* GenericGraphNode = Cast<UGenericGraphNode>(Node))
		{
			GenericGraphNode->GenericNode = DuplicateObject<UGenericNode>(GenericGraphNode->GenericNode, nullptr);
			Model->AllNodes.Add(GenericGraphNode->GenericNode);
		}

		// Select the newly pasted stuff
		QuestGraphEditor->SetNodeSelection(Node, true);

		Node->NodePosX = (Node->NodePosX - AvgNodePosition.X) + Location.X;
		Node->NodePosY = (Node->NodePosY - AvgNodePosition.Y) + Location.Y;

		Node->SnapToGrid(SNodePanel::GetSnapGridSize());

		// Give new node a different Guid from the old one
		Node->CreateNewGuid();
	}

	// Force new pasted QuestNodes to have same connections as graph nodes
	Model->CompileQuestNodesFromGraphNodes();

	// Update UI
	QuestGraphEditor->NotifyGraphChanged();

	Model->PostEditChange();
	Model->MarkPackageDirty();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FGenericEditor::CanPasteNodes() const
{
	FString ClipboardContent;
	FPlatformApplicationMisc::ClipboardPaste(ClipboardContent);

	return FEdGraphUtilities::CanImportNodesFromText(Model->GenericGraph, ClipboardContent);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FGenericEditor::CanCopyNodes() const
{
	// If any of the nodes can be duplicated then we should allow copying
	const FGraphPanelSelectionSet SelectedNodes = GetSelectedNodes();
	for (FGraphPanelSelectionSet::TConstIterator SelectedIter(SelectedNodes); SelectedIter; ++SelectedIter)
	{
		UEdGraphNode* Node = Cast<UEdGraphNode>(*SelectedIter);
		if ((Node != nullptr) && Node->CanDuplicateNode())
		{
			return true;
		}
	}

	return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::PasteNodes()
{
	PasteNodesHere(QuestGraphEditor->GetPasteLocation());
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::DuplicateNodes()
{
	// Copy and paste current selection
	CopySelectedNodes();
	PasteNodes();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FGenericEditor::CanDuplicateNodes() const
{
	return CanCopyNodes();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::UndoGraphAction()
{
	GEditor->UndoTransaction();

	CompileQuest();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericEditor::RedoGraphAction()
{
	GEditor->RedoTransaction();

	CompileQuest();
}

#undef LOCTEXT_NAMESPACE

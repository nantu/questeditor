// Copyright (c) 2020 Gil Engel


#include "SGenericPalette.h"
#include "GenericGraph/GenericGraphSchema.h"
#include "Modules/ModuleManager.h"

/*-----------------------------------------------------------------------------
    SQuestPalette implementation.
-----------------------------------------------------------------------------*/

void SQuestPalette::Construct(const FArguments& InArgs)
{
	// Auto expand the palette as there's so few nodes
	SGraphPalette::Construct(SGraphPalette::FArguments().AutoExpandActionMenu(true));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void SQuestPalette::CollectAllActions(FGraphActionListBuilderBase& OutAllActions)
{
    const UGenericGraphSchema* Schema = GetDefault<UGenericGraphSchema>();

    FGraphActionMenuBuilder ActionMenuBuilder;

    // Determine all possible actions
	TArray<UClass*> Filter;
    Schema->GetPaletteActions(ActionMenuBuilder, Filter);

    // @TODO: Avoid this copy
    OutAllActions.Append(ActionMenuBuilder);
}

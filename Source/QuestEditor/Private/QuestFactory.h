// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Factories/Factory.h"
#include "AssetTypeCategories.h"
#include "QuestFactory.generated.h"

static EAssetTypeCategories::Type QuestAssetCategory;

/**
 * 
 */
UCLASS(hidecategories = Object)
class UQuestFactory : public UFactory
{
    GENERATED_BODY()

public:
    UQuestFactory();

    UObject* FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn) override;

    UPROPERTY()
    class UQuest* InitialQuest;
};

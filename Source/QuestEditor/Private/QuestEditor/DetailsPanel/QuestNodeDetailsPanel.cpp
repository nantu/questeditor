// Copyright (c) 2020 Gil Engel

#include "QuestNodeDetailsPanel.h"
#include "GenericContainer.h"
#include "GenericEditor/GenericEditor.h"
#include "HAL/Platform.h"                      
#include "PropertyEditing.h"
#include "Task/QuestNodeTask.h"
#include "Widgets/DeclarativeSyntaxSupport.h" 
#include "Widgets/Input/SMultiLineEditableTextBox.h"

#define LOCTEXT_NAMESPACE "QuestEditorModule"

/*-----------------------------------------------------------------------------
    QuestNodeDetailsPanel implementation.
-----------------------------------------------------------------------------*/

void QuestNodeDetailsPanel::CustomizeDetails(IDetailLayoutBuilder& DetailBuilder)
{
    TArray<TWeakObjectPtr<UObject> > Objects;
    DetailBuilder.GetObjectsBeingCustomized(Objects);

    if (Objects.Num() != 1)
    {
        return;
    }

    TWeakObjectPtr<UQuestNodeTask> MyObject = Cast<UQuestNodeTask>(Objects[0].Get());

    // Flow Category
    {
        IDetailCategoryBuilder& FlowCategory = DetailBuilder.EditCategory(TEXT("Flow"));
        TArray<TSharedRef<IPropertyHandle>> PropertyHandles;
        FlowCategory.GetDefaultProperties(PropertyHandles);

        auto FilteredPropertyHandles = PropertyHandles.FilterByPredicate([](const TSharedRef<IPropertyHandle>& Handle) -> bool {
            return Handle->GetPropertyDisplayName().EqualTo(FText::FromString("IsOptional"));
        });
    }

    auto& QuestNodeCategory = DetailBuilder.EditCategory(TEXT("QuestNode"));
    TArray<TSharedRef<IPropertyHandle>> PropertyHandles;

    QuestNodeCategory.GetDefaultProperties(PropertyHandles);

    for (const auto& PropertyHandle : PropertyHandles)
    {
        if (PropertyHandle->GetPropertyDisplayName().EqualTo(FText::FromString("Description")))
        {
            // clang-format off
            IDetailPropertyRow& ShaderVersionPropertyRow = QuestNodeCategory.AddProperty(PropertyHandle);
            ShaderVersionPropertyRow
                .CustomWidget()
                .NameContent()[PropertyHandle->CreatePropertyNameWidget()]
                .ValueContent()
                .HAlign(HAlign_Fill)
                [
                    SNew(SVerticalBox) +
                     SVerticalBox::Slot()
                     .AutoHeight()
                     .Padding(2)
                     [
                         SNew(SMultiLineEditableTextBox)
                        .Text(QuestNodeDetailsPanel::GetValueFromProperty(PropertyHandle))
                        .OnTextChanged_Lambda([PropertyHandle](const FText& Text)
                        {
                            PropertyHandle->SetValue(*Text.ToString());
                        })
                        .TextStyle(FEditorStyle::Get(), "MessageLog")
                        .AutoWrapText(true)
                     ]
                ];
            // clang-format on
        }
    }

    DetailBuilder.HideCategory(TEXT("NoCompileValidation"));
	DetailBuilder.HideCategory(TEXT("ComponentReplication"));
	DetailBuilder.HideCategory(TEXT("Activation"));
	DetailBuilder.HideCategory(TEXT("Tags"));
	DetailBuilder.HideCategory(TEXT("Variable"));
	DetailBuilder.HideCategory(TEXT("Cooking"));
	DetailBuilder.HideCategory(TEXT("AssetUserData"));
	DetailBuilder.HideCategory(TEXT("Collision"));

    auto& Cat = DetailBuilder.EditCategory(TEXT("Custom"));

    auto OnGetWarningVisibility = [MyObject] {
        return MyObject.IsValid() && MyObject->RawDescription.IsEmpty() ? EVisibility::Visible : EVisibility::Collapsed;
    };
    auto WarningVisibilityAttr =
        TAttribute<EVisibility>::Create(TAttribute<EVisibility>::FGetter::CreateLambda(OnGetWarningVisibility));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TSharedRef<IDetailCustomization> QuestNodeDetailsPanel::MakeInstance()
{
    return MakeShareable(new QuestNodeDetailsPanel);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText QuestNodeDetailsPanel::GetValueFromProperty(const TSharedRef<IPropertyHandle> PropertyHandle)
{
    FText Result;
    if (PropertyHandle->GetValueAsFormattedText(Result) != FPropertyAccess::Result::Success)
    {
        return FText::FromString("Failure while getting text of property");
    }

    return Result;
}

#undef LOCTEXT_NAMESPACE

// Copyright (c) 2020 Gil Engel

#include "QuestDetailsPanel.h"
#include "Containers/Array.h"                // for TArray
#include "DetailLayoutBuilder.h"             // for IDetailLayoutBuilder
#include "HAL/Platform.h"                    // for TEXT
#include "UObject/WeakObjectPtrTemplates.h"  // for TWeakObjectPtr

class UObject;

#define LOCTEXT_NAMESPACE "QuestEditorModule"

/*-----------------------------------------------------------------------------
    QuestNodeDetailsPanel implementation.
-----------------------------------------------------------------------------*/

void QuestDetailsPanel::CustomizeDetails(IDetailLayoutBuilder& DetailBuilder)
{
    TArray<TWeakObjectPtr<UObject> > Objects;
    DetailBuilder.GetObjectsBeingCustomized(Objects);

    if (Objects.Num() != 1)
    {
        return;
    }

    DetailBuilder.HideCategory(TEXT("BlueprintOptions"));
	DetailBuilder.HideCategory(TEXT("ClassOptions"));
	DetailBuilder.HideCategory(TEXT("Thumbnail"));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TSharedRef<IDetailCustomization> QuestDetailsPanel::MakeInstance()
{
	return MakeShareable(new QuestDetailsPanel);
}

#undef LOCTEXT_NAMESPACE

// Copyright (c) 2020 Gil Engel

#pragma once

#include "IDetailCustomization.h"
#include "Internationalization/Text.h"  // for FText
#include "Templates/SharedPointer.h"    // for TSharedRef
class IDetailLayoutBuilder;
class IPropertyHandle;


/**
 * 
 */
class QuestNodeDetailsPanel : public IDetailCustomization
{
public:
    void CustomizeDetails(IDetailLayoutBuilder& DetailBuilder) override;

    static TSharedRef<IDetailCustomization> MakeInstance();

private:
    FText GetValueFromProperty(const TSharedRef<IPropertyHandle> PropertyHandle);
};

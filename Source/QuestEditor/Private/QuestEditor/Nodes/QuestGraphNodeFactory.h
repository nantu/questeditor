// Copyright (c) 2020 Gil Engel

#pragma once

#include "ConditionEditor/Nodes/ConditionGraphNode.h"
#include "ConditionEditor/Nodes/SGraphNodeCondition.h"
#include "CoreMinimal.h"
#include "EdGraphUtilities.h"
#include "GenericEditor/Nodes/GraphNodeLogical.h"
#include "GenericGraph/GenericGraphNode_Root.h"
#include "GenericGraph/GenericGraphNode_Result.h"
#include "QuestGraph/QuestGraphNode.h"
#include "QuestGraph/QuestGraphNode_Base.h"
#include "QuestGraph/QuestGraphNode_Logic.h"
#include "SGraphNode.h"
#include "SGraphNodeQuestBase.h"
#include "SGraphNodeQuestStart.h"
#include "SGraphNodeQuestResult.h"
#include "Widgets/DeclarativeSyntaxSupport.h"

class FQuestGraphNodeFactory : public FGraphPanelNodeFactory
{
	TSharedPtr<class SGraphNode> CreateNode(class UEdGraphNode* InNode) const override
	{
		if (UGenericGraphNode* BaseQuestNode = Cast<UGenericGraphNode>(InNode))
		{
			if (UGenericGraphNode_Root* RootQuestNode = Cast<UGenericGraphNode_Root>(InNode))
			{
				return SNew(SGraphNodeQuestStart, RootQuestNode);
			}
			else if (UGenericGraphNode_Result* ResultQuestNode = Cast<UGenericGraphNode_Result>(InNode))
			{
				return SNew(SGraphNodeQuestResult, ResultQuestNode);
			}
			else if (UQuestGraphNode_Logic* QuestLogicNode = Cast<UQuestGraphNode_Logic>(InNode))
			{
				return SNew(SGraphNodeQuestBase, QuestLogicNode);
			}
			else if (UConditionGraphNode* ConditionNode = Cast<UConditionGraphNode>(InNode))
			{
				return SNew(SGraphNodeCondition, ConditionNode);
			}
			else if (UQuestGraphNode* QuestNode = Cast<UQuestGraphNode>(InNode))
			{
				return SNew(SGraphNodeQuestBase, QuestNode);
			}
		}

		return nullptr;
	}
};

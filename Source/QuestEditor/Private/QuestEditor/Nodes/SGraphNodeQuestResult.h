// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Input/Reply.h"
#include "Layout/Visibility.h"
#include "GenericEditor/Nodes/GraphNodeWithHighlightedText.h"
#include "Widgets/DeclarativeSyntaxSupport.h"
#include "Widgets/SBoxPanel.h"

class SGraphNodeQuestResult : public SGraphNodeWithoutTitle
{
public:
	SLATE_BEGIN_ARGS(SGraphNodeQuestResult)
    {
    }
    SLATE_END_ARGS()

    void Construct(const FArguments& InArgs, class UGenericGraphNode_Result* InNode);
	void UpdateGraphNode() override;
};

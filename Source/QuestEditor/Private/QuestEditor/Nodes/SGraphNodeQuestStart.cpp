// Copyright (c) 2020 Gil Engel

#include "SGraphNodeQuestStart.h"
#include "QuestGraph/QuestGraphNode.h"
#include "Widgets/Layout/SWrapBox.h"

#define LOCTEXT_NAMESPACE "SGraphNodeQuestStart"

/*-----------------------------------------------------------------------------
    SGraphNodeQuestStart implementation.
-----------------------------------------------------------------------------*/

void SGraphNodeQuestStart::Construct(const FArguments& InArgs, UGenericGraphNode_Root* InNode)
{
    this->GraphNode = InNode;

    this->SetCursor(EMouseCursor::CardinalCross);

    this->UpdateGraphNode();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void SGraphNodeQuestStart::UpdateGraphNode()
{
	SGraphNodeWithoutTitle::UpdateGraphNode();
	ContentNodeBox->AddSlot().Padding(0, 20).VAlign(
		VAlign_Center)[
            SNew(STextBlock)
                .Text(FText::FromString("Start"))
                .Margin(FMargin(20.0, 0.0, 0.0, 0.0))
                .Font(FCoreStyle::GetDefaultFontStyle("Bold", 12))
        ];
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#undef LOCTEXT_NAMESPACE

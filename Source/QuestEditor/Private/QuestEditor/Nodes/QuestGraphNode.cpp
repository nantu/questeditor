// Copyright (c) 2020 Gil Engel

#include "QuestGraph/QuestGraphNode.h"
#include "Algo/Count.h"
#include "EdGraphSchema_K2.h"
#include "Editor/EditorEngine.h"
#include "Engine/Font.h"
#include "Framework/Commands/GenericCommands.h"
#include "GenericGraph/GenericGraph.h"
#include "GraphEditorActions.h"
#include "Quest.h"
#include "QuestGraph/QuestGraphSchema.h"
#include "QuestGraphEditorCommands.h"
#include "ScopedTransaction.h"
#include "ToolMenus.h"

#define LOCTEXT_NAMESPACE "QuestGraphNode"

/*-----------------------------------------------------------------------------
	UQuestGraphNode implementation.
-----------------------------------------------------------------------------*/

void UQuestGraphNode::PostLoad()
{
	Super::PostLoad();

	// Fixup any QuestNode back pointers that may be out of date
	if (GenericNode)
	{
		GenericNode->GraphNode = this;
	}

	for (int32 Index = 0; Index < Pins.Num(); ++Index)
	{
		UEdGraphPin* Pin = Pins[Index];
		if (Pin->PinName.IsNone())
		{
			// Makes sure pin has a name for lookup purposes but user will never see it
			if (Pin->Direction == EGPD_Input)
			{
				Pin->PinName = CreateUniquePinName(TEXT("Input"));
			}
			else
			{
				Pin->PinName = CreateUniquePinName(TEXT("Output"));
			}
			Pin->PinFriendlyName = FText::FromString(TEXT(" "));
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode::AddOutputPin()
{
	if (Cast<UQuestNode>(GenericNode)->IsOptional)
	{
		CreatePin(EGPD_Output, UQuestGraphSchema::PC_OptionalExec, CreateUniquePinName(TEXT("Output")));
		return;
	}

	CreatePin(EGPD_Output, UEdGraphSchema_K2::PC_Exec, "");

	
    UGenericContainer* Quest = CastChecked<UGenericGraph>(GetGraph())->GetModel();
	Quest->CompileQuestNodesFromGraphNodes();
	Quest->MarkPackageDirty();

	// Refresh the current graph, so the pins can be updated
	GetGraph()->NotifyGraphChanged();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode::RemoveOutputPin(UEdGraphPin* InGraphPin)
{
	//const FScopedTransaction Transaction(NSLOCTEXT("UnrealEd", "QuestEditorDeleteOutput", "Delete Quest Output"));
	Modify();

	TArray<class UEdGraphPin*> OutputPins = GetOutputPins();

	for (int32 OutputIndex = 0; OutputIndex < OutputPins.Num(); OutputIndex++)
	{
		if (InGraphPin == OutputPins[OutputIndex])
		{
			InGraphPin->MarkPendingKill();
			Pins.Remove(InGraphPin);
			// also remove the QuestNode child node so ordering matches
			GenericNode->Modify();
			GenericNode->RemoveChildNode(OutputIndex);
			break;
		}
	}

	UGenericContainer* Quest = CastChecked<UGenericGraph>(GetGraph())->GetModel();
	Quest->CompileQuestNodesFromGraphNodes();
	Quest->MarkPackageDirty();

	// Refresh the current graph, so the pins can be updated
	GetGraph()->NotifyGraphChanged();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

int32 UQuestGraphNode::EstimateNodeWidth() const
{
	const int32 EstimatedCharWidth = 6;
	FString NodeTitle = GetNodeTitle(ENodeTitleType::FullTitle).ToString();
	UFont* Font = GetDefault<UEditorEngine>()->EditorFont;
	int32 Result = NodeTitle.Len()*EstimatedCharWidth;

	if (Font)
	{
		Result = Font->GetStringSize(*NodeTitle);
	}

	return Result;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode::PrepareForCopying()
{
	if (GenericNode)
	{
		// Temporarily take ownership of the QuestNode, so that it is not deleted when cutting
		GenericNode->Rename(nullptr, this, REN_DontCreateRedirectors);
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode::PostCopyNode()
{
	// Make sure the QuestNode goes back to being owned by the Quest after copying.
	ResetQuestNodeOwner();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode::PostEditImport()
{
	// Make sure this QuestNode is owned by the Quest it's being pasted into.
	ResetQuestNodeOwner();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode::PostDuplicate(bool bDuplicateForPIE)
{
	Super::PostDuplicate(bDuplicateForPIE);

	if (!bDuplicateForPIE)
	{
		CreateNewGuid();
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode::SwitchPinTypes()
{
	if (!GenericNode)
	{
		return;
	}

	FName PinCategory = Cast<UQuestNode>(GenericNode)->IsOptional ? UQuestGraphSchema::PC_OptionalExec : UEdGraphSchema_K2::PC_Exec;
	for (UEdGraphPin* Pin : GetAllPins())
	{
		if (Pin->PinType.PinCategory == UQuestGraphSchema::PC_OptionalExec ||
			Pin->PinType.PinCategory == UEdGraphSchema_K2::PC_Exec)
		{
			Pin->PinType.PinCategory = PinCategory;
		}		
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode::ResetQuestNodeOwner()
{
	if (GenericNode)
	{
		UGenericContainer* Quest = CastChecked<UGenericGraph>(GetGraph())->GetModel();

		if (GenericNode->GetOuter() != Quest)
		{
			// Ensures QuestNode is owned by the Quest
			GenericNode->Rename(nullptr, Quest, REN_DontCreateRedirectors);
		}

		// Set up the back pointer for newly created Quest nodes
		GenericNode->GraphNode = this;
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode::GetNodeContextMenuActions(UToolMenu* Menu, UGraphNodeContextMenuContext* Context) const
{
	UGenericGraphNode::GetNodeContextMenuActions(Menu, Context);
	
	if (Context->Node)
	{
		{
			FToolMenuSection& Section = Menu->AddSection("QuestGraphNodeAlignment");
			Section.AddSubMenu("Alignment", LOCTEXT("AlignmentHeader", "Alignment"), FText(), FNewToolMenuDelegate::CreateLambda([](UToolMenu* SubMenu)
			{
				{
					FToolMenuSection& SubMenuSection = SubMenu->AddSection("EdGraphSchemaAlignment", LOCTEXT("AlignHeader", "Align"));
					SubMenuSection.AddMenuEntry(FGraphEditorCommands::Get().AlignNodesTop);
					SubMenuSection.AddMenuEntry(FGraphEditorCommands::Get().AlignNodesMiddle);
					SubMenuSection.AddMenuEntry(FGraphEditorCommands::Get().AlignNodesBottom);
					SubMenuSection.AddMenuEntry(FGraphEditorCommands::Get().AlignNodesLeft);
					SubMenuSection.AddMenuEntry(FGraphEditorCommands::Get().AlignNodesCenter);
					SubMenuSection.AddMenuEntry(FGraphEditorCommands::Get().AlignNodesRight);
					SubMenuSection.AddMenuEntry(FGraphEditorCommands::Get().StraightenConnections);
				}

				{
					FToolMenuSection& SubMenuSection = SubMenu->AddSection("EdGraphSchemaDistribution", LOCTEXT("DistributionHeader", "Distribution"));
					SubMenuSection.AddMenuEntry(FGraphEditorCommands::Get().DistributeNodesHorizontally);
					SubMenuSection.AddMenuEntry(FGraphEditorCommands::Get().DistributeNodesVertically);
				}
			}));
		}

		{
			FToolMenuSection& Section = Menu->AddSection("QuestGraphNodeEdit");
			Section.AddMenuEntry(FGenericCommands::Get().Delete);
			Section.AddMenuEntry(FGenericCommands::Get().Cut);
			Section.AddMenuEntry(FGenericCommands::Get().Copy);
			Section.AddMenuEntry(FGenericCommands::Get().Duplicate);
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText UQuestGraphNode::GetTooltipText() const
{
	FText Tooltip;
	if (GenericNode)
	{
		Tooltip = GenericNode->GetClass()->GetToolTipText();
	}
	if (Tooltip.IsEmpty())
	{
		Tooltip = GetNodeTitle(ENodeTitleType::ListView);
	}
	return Tooltip;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FString UQuestGraphNode::GetDocumentationExcerptName() const
{
	// Default the node to searching for an excerpt named for the C++ node class name, including the U prefix.
	// This is done so that the excerpt name in the doc file can be found by find-in-files when searching for the full class name.
	UClass* MyClass = (GenericNode != nullptr) ? GenericNode->GetClass() : this->GetClass();
	return FString::Printf(TEXT("%s%s"), MyClass->GetPrefixCPP(), *MyClass->GetName());
}

FText UQuestGraphNode::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return FText::FromString(GenericNode->GetClass()->GetDescription());
}

#undef LOCTEXT_NAMESPACE

// Copyright (c) 2020 Gil Engel

#include "QuestGraph/QuestGraphNode_Logic.h"
#include "EdGraphSchema_K2.h"
#include "EdGraph/EdGraphNode.h"                       
#include "Internationalization/Internationalization.h"  
#include "Internationalization/Text.h"                  

class FObjectInitializer;

#define LOCTEXT_NAMESPACE "QuestGraphNode_Logic"

/*-----------------------------------------------------------------------------
    UQuestGraphNode_Logic implementation.
-----------------------------------------------------------------------------*/

UQuestGraphNode_Logic::UQuestGraphNode_Logic(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode_Logic::CreateInputPins()
{
	CreatePin(EGPD_Input, UEdGraphSchema_K2::PC_Exec, "");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode_Logic::CreateOutputPins()
{
	CreatePin(EGPD_Output, UEdGraphSchema_K2::PC_Exec, "");
	CreatePin(EGPD_Output, UEdGraphSchema_K2::PC_Exec, "");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText UQuestGraphNode_Logic::GetTooltipText() const
{
    return LOCTEXT("LogicToolTip", "Task the player (might) has to fulfill.");
}

FLinearColor UQuestGraphNode_Logic::GetNodeTitleColor() const
{
	return FColor(106, 176, 76);
}

#undef LOCTEXT_NAMESPACE

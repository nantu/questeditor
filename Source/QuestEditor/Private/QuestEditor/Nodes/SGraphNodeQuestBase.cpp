// Copyright (c) 2020 Gil Engel

#include "SGraphNodeQuestBase.h"
#include "Components/RichTextBlockDecorator.h"
#include "EdGraph/EdGraph.h"
#include "Engine/Font.h"
#include "GraphEditorSettings.h"
#include "IDocumentation.h"
#include "Internationalization/Regex.h"
#include "K2Node.h"
#include "K2Node_AddPinInterface.h"
#include "QuestGraph/QuestGraphNode.h"
#include "QuestNode.h"
#include "SCommentBubble.h"
#include "ScopedTransaction.h"
#include "TutorialMetaData.h"
#include "Widgets/Images/SImage.h"
#include "Widgets/Layout/SSpacer.h"
#include "Widgets/Layout/SWrapBox.h"
#include "Widgets/SBoxPanel.h"
#include "Widgets/Text/SRichTextBlock.h"

#define LOCTEXT_NAMESPACE "SGraphNodeQuestBase"

/*-----------------------------------------------------------------------------
    SGraphNodeQuestBase implementation.
-----------------------------------------------------------------------------*/

void SGraphNodeQuestBase::Construct(const FArguments& InArgs, UQuestGraphNode* InNode)
{
    GraphNode = InNode;
    QuestNode = InNode;

    SetCursor(EMouseCursor::CardinalCross);

    UpdateGraphNode();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void SGraphNodeQuestBase::CreateOutputSideAddButton(TSharedPtr<SVerticalBox> OutputBox)
{
	
    TSharedRef<SWidget> AddPinButton = AddPinButtonContent(NSLOCTEXT("QuestNode", "QuestNodeAddPinButton", "Add Pin"),
        NSLOCTEXT("QuestNode", "QuestNodeAddPinButton_Tooltip", "Adds an alternative node to the quest node"));

    FMargin AddPinPadding = Settings->GetOutputPinPadding();
    AddPinPadding.Top += 6.0f;

    OutputBox->AddSlot().AutoHeight().VAlign(VAlign_Center).Padding(AddPinPadding)[AddPinButton];
    
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

EVisibility SGraphNodeQuestBase::IsAddPinButtonVisible() const
{
    
	if(OutputPins.Num() >= QuestNode->GenericNode->GetMaximumChildNodesCount())
	{
		return EVisibility::Hidden;
    }

    return EVisibility::Visible;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FReply SGraphNodeQuestBase::OnAddPin()
{
    QuestNode->AddOutputPin();

    return FReply::Handled();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#undef LOCTEXT_NAMESPACE

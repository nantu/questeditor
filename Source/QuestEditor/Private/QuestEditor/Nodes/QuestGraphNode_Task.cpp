// Copyright (c) 2020 Gil Engel

#include "QuestGraph/QuestGraphNode_Task.h"
#include "GraphEditorSettings.h"
#include "ToolMenus.h"

#define LOCTEXT_NAMESPACE "QuestGraphNode_Task"

/*-----------------------------------------------------------------------------
    UQuestGraphNode_Task implementation.
-----------------------------------------------------------------------------*/

const FName UQuestGraphNode_Task::FailureConditionPinName(TEXT("Failure?"));
const FName UQuestGraphNode_Task::EnableConditionPinName(TEXT("Enabled?"));

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode_Task::CreateInputPins()
{
	CreatePin(EGPD_Input, UEdGraphSchema_K2::PC_Exec, "");
	CreatePin(EGPD_Input, UEdGraphSchema_K2::PC_Boolean, CreateUniquePinName(EnableConditionPinName));
	CreatePin(EGPD_Input, UEdGraphSchema_K2::PC_Boolean, CreateUniquePinName(FailureConditionPinName));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode_Task::CreateOutputPins()
{
	CreatePin(EGPD_Output, UEdGraphSchema_K2::PC_Exec, "");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText UQuestGraphNode_Task::GetTooltipText() const
{
    return LOCTEXT("TaskTooltip", "Task for the player to perform");
}

FLinearColor UQuestGraphNode_Task::GetNodeTitleColor() const
{
	return FColor(104, 109, 224);
}

#undef LOCTEXT_NAMESPACE

// Copyright (c) 2020 Gil Engel

#include "QuestGraph/QuestGraphNode_Base.h"
#include "ConditionGraph/ConditionSchema.h"
#include "EdGraph/EdGraphSchema.h"
#include "EdGraphSchema_K2.h"
#include "GenericGraph/GenericGraph.h"
#include "QuestGraph/QuestGraphSchema.h"
#include "ScopedTransaction.h"
 	
/*-----------------------------------------------------------------------------
    UQuestGraphNode_Base implementation.
-----------------------------------------------------------------------------*/

#define LOCTEXT_NAMESPACE "UQuestGraphNode_Base"

UQuestGraphNode_Base::UQuestGraphNode_Base(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
	
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode_Base::PostLoad()
{
	{
		Super::PostLoad();

		if (BoundGraph == nullptr)
		{
			BoundGraph = FBlueprintEditorUtils::CreateNewGraph(this, NAME_None, UGenericGraph::StaticClass(), UConditionSchema::StaticClass());
        }
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphNode_Base::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	FName CategoryName = (GenericNode && Cast<UQuestNode>(GenericNode)->IsOptional) ? UQuestGraphSchema::PC_OptionalExec
																					: UEdGraphSchema_K2::PC_Exec;

    auto FilterExecPins = [](const UEdGraphPin* Pin) -> bool 
    { 
        return Pin->PinType.PinCategory == UQuestGraphSchema::PC_OptionalExec ||
			   Pin->PinType.PinCategory == UEdGraphSchema_K2::PC_Exec; 
    };

    for (UEdGraphPin* Pin : GetAllPins().FilterByPredicate(FilterExecPins))
    {
        Pin->PinType.PinCategory = CategoryName;
    }
}

#undef LOCTEXT_NAMESPACE

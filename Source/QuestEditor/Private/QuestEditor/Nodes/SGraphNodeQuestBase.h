// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Layout/Visibility.h"
#include "Widgets/DeclarativeSyntaxSupport.h"
#include "Input/Reply.h"
#include "GenericEditor/Nodes/GraphNodeWithoutTitle.h"
#include "Widgets/SBoxPanel.h"
#include "Styling/SlateStyle.h"

#include "Components/RichTextBlock.h"


class SVerticalBox;
class SHorizontalBox;
class USoundCueGraphNode;

class SGraphNodeQuestBase : public SGraphNodeWithTitle
{
public:
    SLATE_BEGIN_ARGS(SGraphNodeQuestBase)
    {
    }
    SLATE_END_ARGS()

    void Construct(const FArguments& InArgs, class UQuestGraphNode* InNode);

    

protected:
    //~ Begin SGraphNode Interface	
    void CreateOutputSideAddButton(TSharedPtr<SVerticalBox> OutputBox) override;
	EVisibility IsAddPinButtonVisible() const override;
    //~ End SGraphNode Interface

    FReply OnAddPin() override;

    //FText GetDescription() const override;


protected:
    TSharedPtr<SVerticalBox> ContentBox;
    

private:
    UQuestGraphNode* QuestNode;
};

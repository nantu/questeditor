// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Input/Reply.h"
#include "Layout/Visibility.h"
#include "GenericEditor/Nodes/GraphNodeWithHighlightedText.h"
#include "Widgets/DeclarativeSyntaxSupport.h"
#include "Widgets/SBoxPanel.h"

class SGraphNodeQuestStart : public SGraphNodeWithoutTitle
{
public:
    SLATE_BEGIN_ARGS(SGraphNodeQuestStart)
    {
    }
    SLATE_END_ARGS()

    void Construct(const FArguments& InArgs, class UGenericGraphNode_Root* InNode);
	void UpdateGraphNode() override;

protected:
    //void CreateNodeTextContent(TSharedPtr<SHorizontalBox> MainBox) override;
};

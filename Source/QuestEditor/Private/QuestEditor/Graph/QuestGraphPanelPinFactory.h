// Copyright (c) 2020 Gil Engel

#include "EdGraphUtilities.h"

#pragma once


class FQuestGraphPanelPinFactory : public FGraphPanelPinFactory
{
public:
    virtual TSharedPtr<class SGraphPin> CreatePin(class UEdGraphPin* Pin) const;
};

// Copyright (c) 2020 Gil Engel

#include "GenericGraph/Actions/NewNodeAction.h"
#include "GenericGraph/GenericGraph.h"
#include "GenericGraph/GenericGraphSchema.h"
#include "GenericEditor/GenericEditorUtilities.h"
#include "Quest.h"
#include "QuestNodeResult.h"
#include "QuestNodeParallel.h"
#include "QuestGraph/QuestGraphNode.h"
#include "ScopedTransaction.h"
#include "GraphEditor.h"

#define LOCTEXT_NAMESPACE "NewNodeAction"

/*-----------------------------------------------------------------------------
    FQuestGraphSchemaAction_NewNode implementation.
-----------------------------------------------------------------------------*/

UEdGraphNode* FGenericGraphSchemaAction_NewNode::PerformAction(
    class UEdGraph* ParentGraph, UEdGraphPin* FromPin, const FVector2D Location, bool bSelectNewNode /* = true*/)
{
    check(GenericNodeClass); 

    UGenericContainer* Quest = CastChecked<UGenericGraph>(ParentGraph)->GetModel();
    const FScopedTransaction Transaction(LOCTEXT("QuestGraphEditorNewQuestNode", "Quest Editor: New Quest Node"));
    ParentGraph->Modify();
    Quest->Modify();

    UGenericNode* NewNode = nullptr;
	if (UQuestNodeResult* BaseQuestNode = Cast<UQuestNodeResult>(GenericNodeClass))
    {
		NewNode = Quest->ConstructQuestNode<UQuestNodeResult>(GenericNodeClass, bSelectNewNode);
    }
	else if (UQuestNodeParallel* ParallelQuestNode = Cast<UQuestNodeParallel>(GenericNodeClass))
    {
		NewNode = Quest->ConstructQuestNode<UQuestNodeParallel>(GenericNodeClass);
    }
	else
    {
		NewNode = Quest->ConstructQuestNode<UGenericNode>(GenericNodeClass, bSelectNewNode);
    }

    // If this node allows > 0 children but by default has zero - create a connector for starters
	if (NewNode->GetMaximumChildNodesCount() > 0 && NewNode->ChildNodes.Num() == 0)
    {
        NewNode->CreateStartingConnectors();
    }

    // Attempt to connect inputs to selected nodes, unless we're already dragging from a single output
    if (FromPin == nullptr || FromPin->Direction == EGPD_Input)
    {
        ConnectToSelectedNodes(NewNode, ParentGraph);
    }

    if (FromPin)
    {
        if (UQuestGraphNode* QuestGraphNode = Cast<UQuestGraphNode>(FromPin->GetOwningNode()))
        {
			if (UQuestNode* NewQuestNode = Cast<UQuestNode>(NewNode))
			{
				NewQuestNode->IsOptional = QuestGraphNode->GenericNode ? Cast<UQuestNode>(QuestGraphNode->GenericNode)->IsOptional : false;
				QuestGraphNode->SwitchPinTypes();			
            }
        }
    }


    NewNode->GraphNode->NodePosX = Location.X;
    NewNode->GraphNode->NodePosY = Location.Y;

    NewNode->GraphNode->AutowireNewNode(FromPin);

    Quest->PostEditChange();
    Quest->MarkPackageDirty();

    return NewNode->GraphNode;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FGenericGraphSchemaAction_NewNode::ConnectToSelectedNodes(UGenericNode* NewNode, class UEdGraph* ParentGraph) const
{
    // only connect if node can have many children
	if (NewNode->GetMaximumChildNodesCount() > 1)
    {
        const FGraphPanelSelectionSet SelectedNodes = FQuestEditorUtilities::GetSelectedNodes(ParentGraph);

        TArray<UGenericNode*> SortedNodes;
        for (FGraphPanelSelectionSet::TConstIterator NodeIt(SelectedNodes); NodeIt; ++NodeIt)
        {
            UQuestGraphNode* SelectedNode = Cast<UQuestGraphNode>(*NodeIt);

            if (SelectedNode)
            {
                // Sort the nodes by y position
                bool bInserted = false;
                for (int32 Index = 0; Index < SortedNodes.Num(); ++Index)
                {
                    if (SortedNodes[Index]->GraphNode->NodePosY > SelectedNode->NodePosY)
                    {
                        SortedNodes.Insert(SelectedNode->GenericNode, Index);
                        bInserted = true;
                        break;
                    }
                }
                if (!bInserted)
                {
                    SortedNodes.Add(SelectedNode->GenericNode);
                }
            }
        }
        if (SortedNodes.Num() > 1)
        {
            CastChecked<UGenericGraphSchema>(NewNode->GraphNode->GetSchema())->TryConnectNodes(SortedNodes, NewNode);
        }
    }
}

#undef LOCTEXT_NAMESPACE

// Copyright (c) 2020 Gil Engel

#include "GenericGraph/Actions/NewCommentAction.h"
#include "GenericEditor/GenericEditorUtilities.h"
#include "EdGraphNode_Comment.h"
#include "Layout/SlateRect.h"

/*-----------------------------------------------------------------------------
    FQuestGraphSchemaAction_NewComment implementation.
-----------------------------------------------------------------------------*/

UEdGraphNode* FGenericGraphSchemaAction_NewComment::PerformAction(class UEdGraph* ParentGraph, UEdGraphPin* FromPin, const FVector2D Location, bool bSelectNewNode)
{
    // Add menu item for creating comment boxes
    UEdGraphNode_Comment* CommentTemplate = NewObject<UEdGraphNode_Comment>();

    FVector2D SpawnLocation = Location;

    FSlateRect Bounds;
    if (FQuestEditorUtilities::GetBoundsForSelectedNodes(ParentGraph, Bounds, 50.0f))
    {
        CommentTemplate->SetBounds(Bounds);
        SpawnLocation.X = CommentTemplate->NodePosX;
        SpawnLocation.Y = CommentTemplate->NodePosY;
    }

    return FEdGraphSchemaAction_NewNode::SpawnNodeFromTemplate<UEdGraphNode_Comment>(ParentGraph, CommentTemplate, SpawnLocation);
}

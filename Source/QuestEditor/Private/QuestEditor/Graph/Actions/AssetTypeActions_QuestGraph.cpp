// Copyright (c) 2020 Gil Engel

#include "AssetTypeActions_QuestGraph.h"

#include "QuestEditorModule.h"
#include "Quest.h"

/*-----------------------------------------------------------------------------
    FAssetTypeActions_QuestGraph implementation.
-----------------------------------------------------------------------------*/

UClass* FAssetTypeActions_QuestGraph::GetSupportedClass() const
{
    return UQuest::StaticClass();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FAssetTypeActions_QuestGraph::OpenAssetEditor(const TArray<UObject*>& InObjects, TSharedPtr<class IToolkitHost> EditWithinLevelEditor)
{
    const EToolkitMode::Type Mode = EditWithinLevelEditor.IsValid() ? EToolkitMode::WorldCentric : EToolkitMode::Standalone;

    for (auto ObjIt = InObjects.CreateConstIterator(); ObjIt; ++ObjIt)
    {
		if (UQuest* Quest = Cast<UQuest>(*ObjIt))
        {
            auto QuestEditorModule = &FModuleManager::LoadModuleChecked<FQuestEditorModule>("QuestEditor");
            QuestEditorModule->CreateQuestEditor(Mode, EditWithinLevelEditor, Quest);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FAssetTypeActions_QuestGraph::GetActions(const TArray<UObject*>& InObjects, FToolMenuSection& Section)
{
    //auto Quests = GetTypedWeakObjectPtrs<UQuest>(InObjects);
    //FAssetTypeActions_Base::GetActions(InObjects, Section);
}

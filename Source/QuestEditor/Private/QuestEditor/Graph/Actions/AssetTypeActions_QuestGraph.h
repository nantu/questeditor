// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "AssetTypeActions_Base.h"
#include "QuestFactory.h"

class FAssetTypeActions_QuestGraph : public FAssetTypeActions_Base
{
public:
    // IAssetTypeActions Implementation
	FText GetName() const override
	{
		return NSLOCTEXT("AssetTypeActions", "QuestAssetTypeActions", "Quest");
	}
	FColor GetTypeColor() const override
	{
		return FColor(106, 176, 76, 255);
	}
    UClass* GetSupportedClass() const override;
    bool HasActions(const TArray<UObject*>& InObjects) const override { return true; }
    void GetActions(const TArray<UObject*>& InObjects, struct FToolMenuSection& Section) override;
    uint32 GetCategories() override { return QuestAssetCategory; }
    bool CanFilter() override { return false; }

    void OpenAssetEditor(const TArray<UObject*>& InObjects, TSharedPtr<class IToolkitHost> EditWithinLevelEditor = TSharedPtr<IToolkitHost>()) override;

private:
    EAssetTypeCategories::Type AssetCategory;
};

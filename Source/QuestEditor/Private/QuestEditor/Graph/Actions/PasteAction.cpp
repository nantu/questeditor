// Copyright (c) 2020 Gil Engel

#include "GenericGraph/Actions/PasteAction.h"
#include "GenericEditor/GenericEditorUtilities.h"

/*-----------------------------------------------------------------------------
    FQuestGraphSchemaAction_Paste implementation.
-----------------------------------------------------------------------------*/

UEdGraphNode* FGenericGraphSchemaAction_Paste::PerformAction(class UEdGraph* ParentGraph, UEdGraphPin* FromPin, const FVector2D Location, bool bSelectNewNode)
{
    FQuestEditorUtilities::PasteNodesHere(ParentGraph, Location);
    return nullptr;
}

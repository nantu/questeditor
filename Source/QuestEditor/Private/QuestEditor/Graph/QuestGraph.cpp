// Copyright (c) 2020 Gil Engel

#include "Kismet2/BlueprintEditorUtils.h"
#include "K2Node_CommutativeAssociativeBinaryOperator.h"
#include "GenericGraph/GenericGraph.h"
#include "GenericEditor/GenericEditor.h"
#include "GenericGraph/GenericGraphNode_Root.h"
#include "GenericGraph/GenericGraphNode_Result.h"
#include "Quest.h"
#include "QuestNode.h"
#include "QuestNodeResult.h"
#include "QuestNodeParallel.h"
#include "QuestGraph/QuestGraphNode.h"
#include "QuestGraph/QuestGraphNode_Logic.h"
#include "QuestGraph/QuestGraphNode_Task.h"
#include "QuestGraph/QuestGraphSchema.h"
#include "ConditionGraph/ConditionSchema.h"
#include "ConditionEditor/Nodes/ConditionGraphNode.h"
#include "Condition/ConditionNode.h"

/*-----------------------------------------------------------------------------
	UQuestGraph implementation.
-----------------------------------------------------------------------------*/

class FQuestSystemEditor : public IGenericGraphEditor
{
public:
	FQuestSystemEditor()
	{
	}

	~FQuestSystemEditor()
	{
	}

	UEdGraph* CreateNewGenericGraph(UGenericContainer * InModelContainer) override
	{
		if (UQuest* Quest = Cast<UQuest>(InModelContainer))
		{
			return FBlueprintEditorUtils::CreateNewGraph(
				Quest, NAME_None, UGenericGraph::StaticClass(), UQuestGraphSchema::StaticClass());		
		}

		return nullptr;
	}

	void SetupModelNode(UEdGraph* QuestGraph, UGenericNode* InQuestNode, bool bSelectNewNode) override
	{
		if (UQuestNodeResult* BaseQuestNode = Cast<UQuestNodeResult>(InQuestNode))
		{
			FGraphNodeCreator<UGenericGraphNode_Result> NodeCreator(*QuestGraph);
			UGenericGraphNode_Result* GraphNode = NodeCreator.CreateNode(bSelectNewNode);
			GraphNode->SetGenericNode(InQuestNode);
			NodeCreator.Finalize();
		}
		else if (UQuestNodeParallel* ParallelQuestNode = Cast<UQuestNodeParallel>(InQuestNode))
		{
			FGraphNodeCreator<UQuestGraphNode_Logic> NodeCreator(*QuestGraph);
			UQuestGraphNode* GraphNode = NodeCreator.CreateNode(bSelectNewNode);
			GraphNode->SetGenericNode(InQuestNode);
			NodeCreator.Finalize();
		}
		else if (UConditionNode* ConditionNode = Cast<UConditionNode>(InQuestNode)) 
		{
			FGraphNodeCreator<UConditionGraphNode> NodeCreator(*QuestGraph);
			UConditionGraphNode* GraphNode = NodeCreator.CreateNode(bSelectNewNode);
			GraphNode->SetGenericNode(InQuestNode);
			NodeCreator.Finalize();
		}
		else
		{
			FGraphNodeCreator<UQuestGraphNode_Task> NodeCreator(*QuestGraph);
			UQuestGraphNode* GraphNode = NodeCreator.CreateNode(bSelectNewNode);
			GraphNode->SetGenericNode(InQuestNode);
			NodeCreator.Finalize();
		}
	}

#
	void LinkGraphNodesFromModelNodes(UGenericContainer* ModelContainer) override
	{
		UQuest* Quest = CastChecked<UQuest>(ModelContainer);

		// Use QuestNodes to make GraphNode Connections
		if (Quest->FirstNode != nullptr)
		{
			// Find the root node
			TArray<UGenericGraphNode_Root*> RootNodeList;
			Quest->GenericGraph->GetNodesOfClass<UGenericGraphNode_Root>(/*out*/ RootNodeList);
			check(RootNodeList.Num() == 1);

			RootNodeList[0]->Pins[0]->BreakAllPinLinks();
			RootNodeList[0]->Pins[0]->MakeLinkTo(CastChecked<UQuestGraphNode>(Quest->FirstNode->GraphNode)->GetOutputPins()[0]);
		}

		for (TArray<UGenericNode*>::TConstIterator It(Quest->AllNodes); It; ++It)
		{
			UGenericNode* QuestNode = *It;
			if (QuestNode)
			{
				TArray<UEdGraphPin*> InputPins = CastChecked<UQuestGraphNode>(QuestNode->GraphNode)->GetInputPins();
				check(InputPins.Num() == QuestNode->ChildNodes.Num());
				for (int32 ChildIndex = 0; ChildIndex < QuestNode->ChildNodes.Num(); ChildIndex++)
				{
					UGenericNode* ChildNode = QuestNode->ChildNodes[ChildIndex];
					if (ChildNode)
					{
						InputPins[ChildIndex]->BreakAllPinLinks();
						InputPins[ChildIndex]->MakeLinkTo(CastChecked<UQuestGraphNode>(ChildNode->GraphNode)->GetOutputPins()[0]);
					}
				}
			}
		}
	}

	void CompileModelNodesFromGraphNodes(UGenericContainer* GenericContainer) override
	{
		// Use GraphNodes to make QuestNode Connections
		TArray<UGenericNode*> ChildNodes;
		TArray<UEdGraphPin*> OutputPins;

		for (int32 NodeIndex = 0; NodeIndex < GenericContainer->GenericGraph->Nodes.Num(); ++NodeIndex)
		{
			UGenericGraphNode* GraphNode = Cast<UGenericGraphNode>(GenericContainer->GenericGraph->Nodes[NodeIndex]);
			if (GraphNode && GraphNode->GenericNode)
			{
				// Set ChildNodes of each GenericNode

				OutputPins = GraphNode->GetOutputPins();
				ChildNodes.Empty();
				for (int32 PinIndex = 0; PinIndex < OutputPins.Num(); ++PinIndex)
				{
					UEdGraphPin* ChildPin = OutputPins[PinIndex];                 

					for (const auto& LinkedPin : ChildPin->LinkedTo)
					{
						UGenericGraphNode* GraphChildNode = CastChecked<UGenericGraphNode>(LinkedPin->GetOwningNode());                         
						
						UConditionGraphNode* ConditionGraphNode = Cast<UConditionGraphNode>(GraphNode);
						UQuestGraphNode* QuestGraphNode = Cast<UQuestGraphNode>(GraphChildNode);

						if (ConditionGraphNode && QuestGraphNode)
						{
							if (LinkedPin->GetFName().IsEqual(UQuestGraphNode_Task::EnableConditionPinName))
							{
								Cast<UQuestNode>(QuestGraphNode->GenericNode)->Condition =
									Cast<UConditionNode>(ConditionGraphNode->GenericNode); 
								
								continue;
							}
							else if (LinkedPin->GetFName().IsEqual(UQuestGraphNode_Task::FailureConditionPinName))
							{
								Cast<UQuestNode>(QuestGraphNode->GenericNode)->FailureCondition =
									Cast<UConditionNode>(ConditionGraphNode->GenericNode); 

								continue;
							}
						}

						ChildNodes.Add(GraphChildNode->GenericNode);
						
						GraphChildNode->GenericNode->ParentNodes.AddUnique(GraphNode->GenericNode);
					}
				}

				GraphNode->GenericNode->SetFlags(RF_Transactional);
				GraphNode->GenericNode->Modify();
				GraphNode->GenericNode->SetChildNodes(ChildNodes);
				GraphNode->GenericNode->PostEditChange();
			}
			else
			{
				// Set FirstNode based on RootNode connection
				UGenericGraphNode_Root* RootNode = Cast<UGenericGraphNode_Root>(GenericContainer->GenericGraph->Nodes[NodeIndex]);
				if (RootNode)
				{
					GenericContainer->Modify();
					if (RootNode->Pins[0]->LinkedTo.Num() > 0)
					{
						const auto FirstNode = Cast<UQuestNode>(
							Cast<UQuestGraphNode>(RootNode->Pins[0]->LinkedTo[0]->GetOwningNode())->GenericNode);

						// TODO: first node of the graph has no set generic note. Therefore this check is necessary for the time being
						if (FirstNode != nullptr) 
						{
							GenericContainer->FirstNode = FirstNode;
						}						
					}
					else
					{
						GenericContainer->FirstNode = nullptr;
					}
					GenericContainer->PostEditChange();
				}
			}
		}
	}

	void RemovenullptrNodes(UGenericContainer* Quest) override
	{
		// Deal with QuestNode types being removed - iterate in reverse as nodes may be removed
		for (int32 idx = Quest->GenericGraph->Nodes.Num() - 1; idx >= 0; --idx)
		{
			UQuestGraphNode* Node = Cast<UQuestGraphNode>(Quest->GenericGraph->Nodes[idx]);

			if (Node && Node->GenericNode == nullptr)
			{
				FBlueprintEditorUtils::RemoveNode(nullptr, Node, true);
			}
		}
	}
};

//----------------------------------------------------------------------------------------------------------------------------------------------------

UGenericGraph::UGenericGraph(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	if (!UGenericContainer::GetGenericEditor().IsValid())
	{
		UGenericContainer::SetGenericEditor(TSharedPtr<IGenericGraphEditor> (new FQuestSystemEditor()));
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

UGenericContainer* UGenericGraph::GetModel() const
{
	return CastChecked<UGenericContainer>(GetOuter());
}

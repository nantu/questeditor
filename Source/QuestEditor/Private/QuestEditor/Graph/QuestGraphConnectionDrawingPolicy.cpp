// Copyright (c) 2020 Gil Engel

#include "QuestGraphConnectionDrawingPolicy.h"
#include "GenericGraph/GenericGraph.h"
#include "GenericGraph/GenericGraphNode_Root.h"
#include "GenericGraph/GenericGraphSchema.h"
#include "QuestGraph/QuestGraphNode.h"
#include "Editor.h"
#include "QuestNode.h"
#include "Quest.h"

class UGraphEditorSettings;

/*-----------------------------------------------------------------------------
    FQuestGraphConnectionDrawingPolicy implementation.
-----------------------------------------------------------------------------*/

FConnectionDrawingPolicy* FQuestGraphConnectionDrawingPolicyFactory::CreateConnectionPolicy(const class UEdGraphSchema* Schema, int32 InBackLayerID, int32 InFrontLayerID, float ZoomFactor,
    const class FSlateRect& InClippingRect, class FSlateWindowElementList& InDrawElements, class UEdGraph* InGraphObj) const
{
    if (Schema->IsA(UGenericGraphSchema::StaticClass()))
    {
        return new FQuestGraphConnectionDrawingPolicy(InBackLayerID, InFrontLayerID, ZoomFactor, InClippingRect, InDrawElements, InGraphObj);
    }

    return nullptr;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FQuestGraphConnectionDrawingPolicy::FQuestGraphConnectionDrawingPolicy(int32 InBackLayerID, int32 InFrontLayerID, float ZoomFactor, const FSlateRect& InClippingRect,
    FSlateWindowElementList& InDrawElements, UEdGraph* InGraphObj)
    : FConnectionDrawingPolicy(InBackLayerID, InFrontLayerID, ZoomFactor, InClippingRect, InDrawElements)
    , GraphObj(InGraphObj)
{
    // Cache off the editor options
    ActiveColor = Settings->TraceAttackColor;
    InactiveColor = Settings->TraceReleaseColor;

    ActiveWireThickness = Settings->TraceAttackWireThickness;
    InactiveWireThickness = Settings->TraceReleaseWireThickness;

    // Don't want to draw ending arrowheads
    ArrowImage = nullptr;
    ArrowRadius = FVector2D::ZeroVector;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FQuestGraphConnectionDrawingPolicy::Draw(TMap<TSharedRef<SWidget>, FArrangedWidget>& InPinGeometries, FArrangedChildren& ArrangedNodes)
{
    // Draw everything
    FConnectionDrawingPolicy::Draw(InPinGeometries, ArrangedNodes);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

// Give specific editor modes a chance to highlight this connection or darken non-interesting connections
void FQuestGraphConnectionDrawingPolicy::DetermineWiringStyle(UEdGraphPin* OutputPin, UEdGraphPin* InputPin, /*inout*/ FConnectionParams& Params)
{
    Params.AssociatedPin1 = OutputPin;
    Params.AssociatedPin2 = InputPin;

    // Get the schema and grab the default color from it
    check(OutputPin);
    check(GraphObj);
    const UEdGraphSchema* Schema = GraphObj->GetSchema();

    Params.WireColor = Schema->GetPinTypeColor(OutputPin->PinType);

    if (InputPin == nullptr)
    {
        return;
    }
}

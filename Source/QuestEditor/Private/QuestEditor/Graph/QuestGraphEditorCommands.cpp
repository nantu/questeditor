// Copyright (c) 2020 Gil Engel

#include "QuestGraphEditorCommands.h"

#define LOCTEXT_NAMESPACE "QuestSystemGraphEditorCommands"

/*-----------------------------------------------------------------------------
    FQuestGraphEditorCommands implementation.
-----------------------------------------------------------------------------*/

void FQuestGraphEditorCommands::RegisterCommands()
{
    UI_COMMAND(Compile, "Compile", "Compile the quest", EUserInterfaceActionType::Button, FInputChord(EKeys::F7));

    UI_COMMAND(AddOutput, "Add Output", "Adds an output to the node", EUserInterfaceActionType::Button, FInputChord());
    UI_COMMAND(DeleteOutput, "Delete Output", "Removes an output from the node", EUserInterfaceActionType::Button, FInputChord());
	UI_COMMAND(DeleteInput, "Delete Input", "Removes an input from the node", EUserInterfaceActionType::Button, FInputChord());
}

#undef LOCTEXT_NAMESPACE

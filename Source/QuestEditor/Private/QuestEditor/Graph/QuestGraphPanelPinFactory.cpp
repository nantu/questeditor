// Copyright (c) 2020 Gil Engel

#include "QuestGraphPanelPinFactory.h"

#include "GenericGraph/GenericGraphSchema.h"
#include "Pins/SGraphPinOptionalExec.h"

/*-----------------------------------------------------------------------------
    FQuestGraphPanelPinFactory implementation.
-----------------------------------------------------------------------------*/

TSharedPtr<class SGraphPin> FQuestGraphPanelPinFactory::CreatePin(UEdGraphPin* InPin) const
{
    check(InPin != nullptr);

    if (const UQuestGraphSchema* QuestSchema = Cast<const UQuestGraphSchema>(InPin->GetSchema()))
    {
        if (InPin->PinType.PinCategory == QuestSchema->PC_OptionalExec)
        {
            return SNew(SGraphPinOptionalExec, InPin);
        }
    }

    // Give other factories the chance to create the pin
	return nullptr;
}

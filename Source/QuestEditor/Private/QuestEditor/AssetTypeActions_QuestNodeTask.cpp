// Copyright (c) 2020 Gil Engel

#include "AssetTypeActions_QuestNodeTask.h"

#include "QuestEditorModule.h"
#include "Task/QuestNodeTask.h"

/*-----------------------------------------------------------------------------
    FAssetTypeActions_QuestGraph implementation.
-----------------------------------------------------------------------------*/

UClass* FAssetTypeActions_QuestNodeTask::GetSupportedClass() const
{
    return UQuestNodeTask::StaticClass();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

/*
void FAssetTypeActions_QuestNodeTask::OpenAssetEditor(
	const TArray<UObject*>& InObjects, TSharedPtr<class IToolkitHost> EditWithinLevelEditor)
{
    const EToolkitMode::Type Mode = EditWithinLevelEditor.IsValid() ? EToolkitMode::WorldCentric : EToolkitMode::Standalone;

    for (auto ObjIt = InObjects.CreateConstIterator(); ObjIt; ++ObjIt)
    {
		if (UQuest* Quest = Cast<UQuest>(*ObjIt))
        {
            auto QuestEditorModule = &FModuleManager::LoadModuleChecked<FQuestEditorModule>("QuestEditor");
            QuestEditorModule->CreateQuestEditor(Mode, EditWithinLevelEditor, Quest);
        }
    }
}
*/

//----------------------------------------------------------------------------------------------------------------------------------------------------


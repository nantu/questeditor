// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "AssetTypeActions_Base.h"
#include "QuestNodeFactory.h"

#include "AssetTypeActions/AssetTypeActions_Blueprint.h"

class FAssetTypeActions_QuestNodeTask : public FAssetTypeActions_Blueprint
{
public:
    // IAssetTypeActions Implementation
	FText GetName() const override
	{
		return NSLOCTEXT("AssetTypeActions", "QuestAssetTypeActions", "QuestNode");
	}
	FColor GetTypeColor() const override
	{
		return FColor(255, 176, 76, 255);
	}
    UClass* GetSupportedClass() const override;
    uint32 GetCategories() override { return QuestAssetCategory; }
    bool CanFilter() override { return false; }

	//void OpenAssetEditor(const TArray<UObject*>& InObjects, TSharedPtr<class IToolkitHost> EditWithinLevelEditor) override;

private:
    EAssetTypeCategories::Type AssetCategory;
};

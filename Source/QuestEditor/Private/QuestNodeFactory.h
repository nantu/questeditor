// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Factories/Factory.h"
#include "Factories/BlueprintFactory.h"
#include "AssetTypeCategories.h"
#include "QuestNodeFactory.generated.h"

//static EAssetTypeCategories::Type QuestNodeAssetCategory;

/**
 * 
 */
UCLASS(hidecategories = Object)
class UQuestNodeFactory : public UBlueprintFactory
{
    GENERATED_BODY()

public:
	UQuestNodeFactory();

    UObject* FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn) override;

    virtual bool ShouldShowInNewMenu() const override;

    //UPROPERTY()
    //class UQuestNodeTask* InitialQuestNode;
};

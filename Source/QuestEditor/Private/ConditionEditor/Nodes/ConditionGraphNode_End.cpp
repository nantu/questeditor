// Copyright (c) 2020 Gil Engel

#include "ConditionGraphNode_End.h"
#include "EdGraphSchema_K2.h"

/*-----------------------------------------------------------------------------
	UConditionGraphNode_End implementation.
-----------------------------------------------------------------------------*/

const bool UConditionGraphNode_End::IsAddPinButtonVisible() const
{
	return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UConditionGraphNode_End::CreateOutputPins()
{
}

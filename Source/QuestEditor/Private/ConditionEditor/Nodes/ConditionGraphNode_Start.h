// Copyright (c) 2020 Gil Engel

#pragma once

#include "ConditionGraphNode.h"
#include "ConditionGraphNode_Start.generated.h"

/**
 * 
 */
UCLASS()
class UConditionGraphNode_Start : public UConditionGraphNode
{
	GENERATED_BODY()	

public:
	const bool IsAddPinButtonVisible() const override;

	bool CanAddInputPin() override;

	//~Begin UGenericGraphNode
	void CreateInputPins() override;
	//~End UGenericGraphNode
};

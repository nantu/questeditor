// Copyright (c) 2020 Gil Engel

#include "ConditionGraphNode.h"
#include "Condition/UnaryConditionNode.h"
#include "EdGraphSchema_K2.h"

/*-----------------------------------------------------------------------------
	SGraphNodeCondition implementation.
-----------------------------------------------------------------------------*/

void UConditionGraphNode::AddInputPin()
{
	CreatePin(EGPD_Input, UEdGraphSchema_K2::PC_Boolean, "");

	// Refresh the current graph, so the pins can be updated
	GetGraph()->NotifyGraphChanged();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

const bool UConditionGraphNode::IsAddPinButtonVisible() const
{
	return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UConditionGraphNode::CreateInputPins()
{
	CreatePin(EGPD_Input, UEdGraphSchema_K2::PC_Boolean, "");

	if (!Cast<UUnaryConditionNode>(GenericNode))
	{
		CreatePin(EGPD_Input, UEdGraphSchema_K2::PC_Boolean, "");
	}	
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UConditionGraphNode::CreateOutputPins()
{
	CreatePin(EGPD_Output, UEdGraphSchema_K2::PC_Boolean, "");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FLinearColor UConditionGraphNode::GetNodeTitleColor() const
{
	return FColor(240, 147, 43);
}

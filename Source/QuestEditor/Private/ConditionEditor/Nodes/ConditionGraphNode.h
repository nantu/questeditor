// Copyright (c) 2020 Gil Engel
#pragma once

#include "CoreMinimal.h"
#include "EdGraph/EdGraphNode.h"
#include "Condition/ConditionNode.h"
#include "GenericGraph/GenericGraphNode.h"
#include "ConditionGraphNode.generated.h"

/**
 * 
 */
UCLASS()
class UConditionGraphNode : public UGenericGraphNode
{
	GENERATED_BODY()
public:
	void AddInputPin() override;

	virtual const bool IsAddPinButtonVisible() const;

	//~Begin UGenericGraphNode
	void CreateInputPins() override;
	void CreateOutputPins() override;
	FLinearColor GetNodeTitleColor() const override;
	//~End UGenericGraphNode
};
// Copyright (c) 2020 Gil Engel

#include "ConditionGraphNode_Start.h"
#include "EdGraphSchema_K2.h"

/*-----------------------------------------------------------------------------
	UConditionGraphNode_Start implementation.
-----------------------------------------------------------------------------*/

const bool UConditionGraphNode_Start::IsAddPinButtonVisible() const
{
	return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UConditionGraphNode_Start::CanAddInputPin()
{
	return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UConditionGraphNode_Start::CreateInputPins()
{
}

// Copyright (c) 2020 Gil Engel

#pragma once

#include "ConditionEditor/Nodes/ConditionGraphNode.h"

#include "CoreMinimal.h"
#include "EdGraphUtilities.h"
#include "SGraphNode.h"

#include "KismetNodes/SGraphNodeK2Base.h"

class FConditionGraphNodeFactory : public FGraphPanelNodeFactory
{
	TSharedPtr<class SGraphNode> CreateNode(class UEdGraphNode* InNode) const override
	{
		if (UConditionGraphNode* ConditionNode = Cast<UConditionGraphNode>(InNode))
		{
			return SNew(SGraphNodeCondition, ConditionNode);
		}

		return nullptr;
    }
};

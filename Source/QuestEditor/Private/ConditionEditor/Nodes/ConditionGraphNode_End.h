// Copyright (c) 2020 Gil Engel

#pragma once

#include "ConditionGraphNode.h"
#include "ConditionGraphNode_End.generated.h"

/**
 * 
 */
UCLASS()
class UConditionGraphNode_End : public UConditionGraphNode
{
	GENERATED_BODY()

public:
	const bool IsAddPinButtonVisible() const override;

	//~Begin UGenericGraphNode
	void CreateOutputPins() override;
	//~End UGenericGraphNode
};

// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "ConditionGraphNode.h"
#include "GenericEditor/Nodes/GraphNodeLogical.h"
#include "SGraphNode.h"

/**
 * 
 */
class SGraphNodeCondition : public SGraphNodeLogical
{
public:
	SLATE_BEGIN_ARGS(SGraphNodeCondition)
	{
	}
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs, class UConditionGraphNode* InNode);


	void CreateInputSideAddButton(TSharedPtr<SVerticalBox> OutputBox) override;

	

protected:
	//~ Begin SGraphNode Interface
	EVisibility IsAddPinButtonVisible() const override;
	FReply OnAddPin() override;
	//~ End of SGraphNode interface
		
	//~ Begin SGraphNodeWithHighlightedText interface
	//FText GetDescription() const override;
	//~ End SGraphNodeWithHighlightedText interface

protected:
	UConditionGraphNode* ConditionNode;
};

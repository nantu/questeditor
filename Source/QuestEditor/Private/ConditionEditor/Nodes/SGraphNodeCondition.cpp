// Copyright (c) 2020 Gil Engel

#include "SGraphNodeCondition.h" 	
#include "Brushes/SlateBoxBrush.h"
#include "GraphEditorSettings.h"
#include "QuestGraph/QuestGraphNode.h"
#include "QuestStyle.h"

/*-----------------------------------------------------------------------------
	SGraphNodeCondition implementation.
-----------------------------------------------------------------------------*/

void SGraphNodeCondition::Construct(const FArguments& InArgs, class UConditionGraphNode* InNode)
{
	this->GraphNode = InNode;
	this->ConditionNode = InNode;

	this->SetCursor(EMouseCursor::CardinalCross);

	this->UpdateGraphNode();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

EVisibility SGraphNodeCondition::IsAddPinButtonVisible() const
{
	return ConditionNode->IsAddPinButtonVisible() ? EVisibility::Visible : EVisibility::Collapsed;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FReply SGraphNodeCondition::OnAddPin()
{
	CastChecked<UGenericGraphNode>(GraphNode)->AddInputPin();

	return FReply::Handled();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void SGraphNodeCondition::CreateInputSideAddButton(TSharedPtr<SVerticalBox> OutputBox)
{
	/*
	TSharedRef<SWidget> AddPinButton = AddPinButtonContent(NSLOCTEXT("QuestNode", "QuestNodeAddPinButton", ""),
		NSLOCTEXT("QuestNode", "QuestNodeAddPinButton_Tooltip", "Adds an alternative node to the quest node"));

	FMargin AddPinPadding = Settings->GetInputPinPadding();
	AddPinPadding.Top += 6.0f;

	
	OutputBox->AddSlot()
		.AutoHeight()
		.VAlign(VAlign_Center)
		.Padding(AddPinPadding)
		[AddPinButton];
	*/
}
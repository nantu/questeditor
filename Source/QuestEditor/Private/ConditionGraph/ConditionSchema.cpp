// Copyright (c) 2020 Gil Engel

#include "ConditionGraph/ConditionSchema.h"
#include "ConditionEditor/Nodes/ConditionGraphNode.h"
#include "ConditionEditor/Nodes/ConditionGraphNode_Start.h"
#include "ConditionEditor/Nodes/ConditionGraphNode_End.h"
#include "EdGraph/EdGraph.h"

void UConditionSchema::CreateDefaultNodesForGraph(UEdGraph& Graph) const
{
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UConditionSchema::GetPlacableNodes(TArray<TAssetSubclassOf<UObject>>& Nodes, const TArray<UClass*>& IgnoreList) const
{
	Utils::GetAllBlueprintSubclasses(Nodes, UConditionNode::StaticClass(), true, IgnoreList);
	Utils::GetAllNativeSubclasses(Nodes, UConditionNode::StaticClass(), false, IgnoreList);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UConditionSchema::BreakPinLinks(UEdGraphPin& TargetPin, bool bSendsNodeNotifcation) const
{
	const FScopedTransaction Transaction(NSLOCTEXT("UnrealEd", "GraphEd_BreakPinLinks", "Break Pin Links"));

	Super::BreakPinLinks(TargetPin, bSendsNodeNotifcation);
}
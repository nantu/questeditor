// Copyright (c) 2020 Gil Engel

#include "QuestNodeFactory.h"
#include "Task/QuestNodeTask.h"
#include "GenericGraph/GenericGraph.h"

/*-----------------------------------------------------------------------------
	UQuestNodeFactory implementation.
-----------------------------------------------------------------------------*/

UQuestNodeFactory::UQuestNodeFactory()
{
	SupportedClass = UQuestNodeTask::StaticClass();
	bCreateNew = true;
	bEditAfterNew = true;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

UObject* UQuestNodeFactory::FactoryCreateNew(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn)
{
	return NewObject<UQuestNodeTask>(InParent, InClass, InName, Flags);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UQuestNodeFactory::ShouldShowInNewMenu() const
{
	return true;
}
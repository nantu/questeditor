// Copyright (c) 2020 Gil Engel

#include "LocationPolygonVisualizer.h"
#include "LocationPolygonComponent.h"
#include "LocationPolygonTrigger.h"
#include "SceneManagement.h"

	#include "DrawDebugHelpers.h"

#include "Kismet/KismetMathLibrary.h"

IMPLEMENT_HIT_PROXY(HPolygonVisProxy, HComponentVisProxy)
IMPLEMENT_HIT_PROXY(HPolygonKeyProxy, HPolygonVisProxy)
IMPLEMENT_HIT_PROXY(HPolygonSegmentProxy, HPolygonVisProxy)

#define LOCTEXT_NAMESPACE "PolygonComponentVisualizer"

/** Define commands for the polygon component visualizer */
class FLocationPolygonVisaulizerCommands : public TCommands<FLocationPolygonVisaulizerCommands>
{
public: 
	FLocationPolygonVisaulizerCommands(): TCommands<FLocationPolygonVisaulizerCommands>
		(
			"PolygonComponentVisualizer", 
			LOCTEXT("PolygonComponentVisualizer", 
			"Polygon Component Visualizer"), 
			NAME_None, 
			FEditorStyle::GetStyleSetName()
		)
	{
		
	}

	virtual void RegisterCommands() override
	{
		UI_COMMAND(DeleteKey, "Delete Polygon Point", "Delete the currently selected polygon point.",
			EUserInterfaceActionType::Button, FInputChord(EKeys::Delete));
		UI_COMMAND(AddKey, "Add Polygon Point Here", "Add a new polygon point at the cursor location.",
			EUserInterfaceActionType::Button, FInputChord());
	}

	public:
	/** Delete key */
	TSharedPtr<FUICommandInfo> DeleteKey;

	/** Add key */
	TSharedPtr<FUICommandInfo> AddKey;

	/** Select all Key */
	TSharedPtr<FUICommandInfo> SelectAllKey;
};

//----------------------------------------------------------------------------------------------------------------------------------------------------

FLocationPolygonVisualizer::FLocationPolygonVisualizer()
	: FComponentVisualizer() 
	, SelectedKey(INDEX_NONE), SelectedSegment(INDEX_NONE), BoxHeightOffsetVector(0.0, 0.0, 500.0)
{
	FLocationPolygonVisaulizerCommands::Register();
	
	PolygonComponentVisualizerActions = MakeShareable(new FUICommandList);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FLocationPolygonVisualizer::~FLocationPolygonVisualizer()
{
	FLocationPolygonVisaulizerCommands::Unregister();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FLocationPolygonVisualizer::OnRegister()
{
	const auto& Commands = FLocationPolygonVisaulizerCommands::Get();

	PolygonComponentVisualizerActions->MapAction(Commands.DeleteKey, 
		FExecuteAction::CreateSP(this, &FLocationPolygonVisualizer::OnDeleteKey),
		FCanExecuteAction::CreateSP(this, &FLocationPolygonVisualizer::CanDeleteKey));

	PolygonComponentVisualizerActions->MapAction(Commands.AddKey,
		FExecuteAction::CreateSP(this, &FLocationPolygonVisualizer::OnAddKeyToSegment),
		FCanExecuteAction::CreateSP(this, &FLocationPolygonVisualizer::CanAddKeyToSegment));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FLocationPolygonVisualizer::VisProxyHandleClick(
	FEditorViewportClient* InViewportClient, HComponentVisProxy* VisProxy, const FViewportClick& Click)
{
	bool IsEditing = false;

	if (VisProxy && VisProxy->Component.IsValid())
	{
		const ULocationPolygonComponent* PolygonComponent = CastChecked<const ULocationPolygonComponent>(VisProxy->Component.Get());

		AActor* OldPolygonMeshOwningActor = PolygonPropertyPath.GetParentOwningActor();
		PolygonPropertyPath = FComponentPropertyPath(PolygonComponent);
		AActor* NewPolygonMeshOwningActor = PolygonPropertyPath.GetParentOwningActor();

		PolygonPropertyPath = FComponentPropertyPath(PolygonComponent);

		if (PolygonPropertyPath.IsValid())
		{
			if (VisProxy->IsA(HPolygonKeyProxy::StaticGetType()))
			{
				HPolygonKeyProxy* KeyProxy = (HPolygonKeyProxy*)VisProxy;
				SelectedKey = KeyProxy->KeyIndex;

				IsEditing = true;			
			}
			else if (VisProxy->IsA(HPolygonSegmentProxy::StaticGetType()))
			{
				HPolygonSegmentProxy* SegmentProxy = (HPolygonSegmentProxy*)VisProxy;
				SelectedSegment = SegmentProxy->SegmentIndex;

				ULocationPolygonComponent* PolygonMeshComp = GetEditedPolygonComponent();
				if (PolygonMeshComp != nullptr)
				{
					FVector Center = PolygonMeshComp->GetOwner()->GetActorLocation();
					const FVector SubsegmentStart = Center + PolygonMeshComp->Points[SelectedSegment];
					const FVector SubsegmentEnd =
						Center + PolygonMeshComp->Points[SelectedSegment + 1 == PolygonMeshComp->Points.Num() ? 0 : SelectedSegment + 1];

					FVector ClickDirection = Click.GetOrigin() + Click.GetDirection() * 50000.0f;
					FVector LowerSplineClosest;
					FVector LowerRayClosest;
					FMath::SegmentDistToSegmentSafe(
						SubsegmentStart, SubsegmentEnd, Click.GetOrigin(), ClickDirection, LowerSplineClosest, LowerRayClosest);
					
					FVector UpperSplineClosest;
					FVector UpperRayClosest;
					FMath::SegmentDistToSegmentSafe(SubsegmentStart + BoxHeightOffsetVector, SubsegmentEnd + BoxHeightOffsetVector,
						Click.GetOrigin(), ClickDirection, UpperSplineClosest, UpperRayClosest);

					const float LowerDistance = FVector::DistSquared(LowerSplineClosest, LowerRayClosest);
					const float UpperDistance = FVector::DistSquared(UpperSplineClosest, UpperRayClosest);

					SelectionPosition = LowerDistance < UpperDistance ? LowerSplineClosest : UpperSplineClosest - BoxHeightOffsetVector;
				}			

				IsEditing = true;
			}
		}
		else
		{
			PolygonPropertyPath.Reset();
		}
	}

	return IsEditing;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FLocationPolygonVisualizer::HandleInputKey(
	FEditorViewportClient* ViewportClient, FViewport* Viewport, FKey Key, EInputEvent Event)
{
	return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FLocationPolygonVisualizer::GetCustomInputCoordinateSystem(
	const FEditorViewportClient* ViewportClient, FMatrix& OutMatrix) const
{
	if (ViewportClient->GetWidgetCoordSystemSpace() == COORD_Local)
	{
		ULocationPolygonComponent* PolygonMeshComp = GetEditedPolygonComponent();
		if (PolygonMeshComp != nullptr)
		{
			int32 Index = SelectedKey;

			if (Index != INDEX_NONE)
			{				
				const auto& Point = PolygonMeshComp->Points[Index];

				OutMatrix = FQuatRotationTranslationMatrix::Make(PolygonMeshComp->GetComponentTransform().GetRotation(),
					PolygonMeshComp->GetComponentTransform().TransformPosition(Point));
				return true;
			}
		}
	}

	return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FLocationPolygonVisualizer::DrawPolygonSegment(
	FPrimitiveDrawInterface* PDI, const ULocationPolygonComponent* Polygon, int StartIndex, int EndIndex)
{
	check(StartIndex >= 0 && StartIndex <= Polygon->Points.Num());
	check(EndIndex >= 0 && EndIndex <= Polygon->Points.Num());

	FVector Center = Polygon->GetOwner()->GetActorLocation();
	FVector StartBottom = Center + Polygon->Points[StartIndex];
	FVector StartTop = StartBottom + BoxHeightOffsetVector;
	
	FVector EndBottom = Center + Polygon->Points[EndIndex];
	FVector EndTop = EndBottom + BoxHeightOffsetVector;

	auto DrawHandle = [PDI, Polygon](FVector Position, int32 HitProxyKeyIndex) {
		PDI->SetHitProxy(new HPolygonKeyProxy(Polygon, HitProxyKeyIndex));
		PDI->DrawPoint(Position, ALocationTrigger::LineColor, ALocationTrigger::LineThickness * 2.0, SDPG_Foreground);
	};

	auto DrawSegment = [PDI, Polygon](FVector Start, FVector End, int32 HitProxySegmentIndex) {
		PDI->SetHitProxy(new HPolygonSegmentProxy(Polygon, HitProxySegmentIndex));
		PDI->DrawLine(Start, End, ALocationTrigger::LineColor, SDPG_World, ALocationTrigger::LineThickness, SDPG_Foreground);
	};
	
	// Draw Handles
	DrawHandle(StartBottom, StartIndex);
	DrawHandle(StartTop, StartIndex);
	DrawHandle(EndBottom, EndIndex);
	DrawHandle(EndTop, EndIndex);

	DrawSegment(StartTop, EndTop, StartIndex);
	DrawSegment(StartBottom, EndBottom, StartIndex);

	DrawSegment(StartBottom, StartTop, StartIndex);
	DrawSegment(EndBottom, EndTop, EndIndex);
}

void FLocationPolygonVisualizer::DrawVisualization(
	const UActorComponent* Component, const FSceneView* View, FPrimitiveDrawInterface* PDI)
{
	// Cast the incoming component to our UConnector class
	const ULocationPolygonComponent* Polygon = Cast<const ULocationPolygonComponent>(Component);

	if (Polygon->Points.Num() < 2)
	{
		return;
	}
	
	// Draw the lines	
	for (int32 Step = 1; Step < Polygon->Points.Num(); Step++)
	{
		DrawPolygonSegment(PDI, Polygon, Step - 1, Step);
	}
	DrawPolygonSegment(PDI, Polygon, Polygon->Points.Num() - 1, 0);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FLocationPolygonVisualizer::DrawVisualizationHUD(
	const UActorComponent* Component, const FViewport* Viewport, const FSceneView* View, FCanvas* Canvas)
{
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FLocationPolygonVisualizer::HandleInputDelta(FEditorViewportClient* ViewportClient, FViewport* Viewport,
	FVector& DeltaTranslate,
	FRotator& DeltaRotate, FVector& DeltaScale) 
{
	ULocationPolygonComponent* PolygonComponent = GetEditedPolygonComponent();
	if (PolygonComponent == nullptr)
	{
		EndEditing();
		return false;
	}

	PolygonComponent->Points[SelectedKey] += DeltaTranslate;

	ALocationPolygonTrigger* Actor = CastChecked<ALocationPolygonTrigger>(PolygonComponent->GetOwner());
	Actor->UpdateCollisionComponents(PolygonComponent->Points);


	GEditor->RedrawLevelEditingViewports(true);

	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FLocationPolygonVisualizer::GetWidgetLocation(const FEditorViewportClient* ViewportClient, FVector& OutLocation) const
{
	ULocationPolygonComponent* PolygonComponent = GetEditedPolygonComponent();

	if (SelectedKey == PolygonComponent->Points.Num())
	{
		const auto& Point = PolygonComponent->Points[0];
		OutLocation = PolygonComponent->GetComponentTransform().TransformPosition(Point);

		return true;
	}

	if (SelectedKey != INDEX_NONE) 
	{
		const auto& Point = PolygonComponent->Points[SelectedKey];
		OutLocation = PolygonComponent->GetComponentTransform().TransformPosition(Point);

		return true;
	}
	
	return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

ULocationPolygonComponent* FLocationPolygonVisualizer::GetEditedPolygonComponent() const
{
	return Cast<ULocationPolygonComponent>(PolygonPropertyPath.GetComponent());
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FLocationPolygonVisualizer::EndEditing()
{
	PolygonPropertyPath.Reset();
	SelectedKey = INDEX_NONE;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FLocationPolygonVisualizer::OnDeleteKey()
{
	const FScopedTransaction Transaction(LOCTEXT("DeletePolygonPoint", "Delete Polygon Point"));
	ULocationPolygonComponent* PolygonComponent = GetEditedPolygonComponent();
	check(PolygonComponent != nullptr);


	PolygonComponent->Modify();
	if (AActor* Owner = PolygonComponent->GetOwner())
	{
		Owner->Modify();
	}

	UE_LOG(LogTemp, Warning, TEXT("Delete Key=%i NumPoints=%i"), SelectedKey, PolygonComponent->Points.Num());
	PolygonComponent->Points.RemoveAt(SelectedKey);
	
	ALocationPolygonTrigger* Actor = CastChecked<ALocationPolygonTrigger>(PolygonComponent->GetOwner());
	Actor->UpdateCollisionComponents(PolygonComponent->Points);

	GEditor->RedrawLevelEditingViewports(true);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FLocationPolygonVisualizer::CanDeleteKey() const
{
	ULocationPolygonComponent* PolygonComponent = GetEditedPolygonComponent();
	return (PolygonComponent != nullptr && SelectedKey != INDEX_NONE && PolygonComponent->Points.Num() > 3);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#if WITH_EDITOR
void FLocationPolygonVisualizer::OnAddKeyToSegment()
{
	//const FScopedTransaction Transaction(LOCTEXT("AddSplinePoint", "Add Spline Point"));
	ULocationPolygonComponent* PolygonComponent = GetEditedPolygonComponent();
	check(PolygonComponent != nullptr);
	check(SelectedSegment != INDEX_NONE);

	const auto NextIndex = SelectedSegment < PolygonComponent->Points.Num() - 1 ? SelectedSegment + 1 : 0;

	const FVector Start = PolygonComponent->Points[SelectedSegment];
	const FVector End = PolygonComponent->Points[NextIndex];

	GEditor->BeginTransaction(LOCTEXT("AddSplinePoint", "Add Spline Point"));
		
	if (GUndo)
	{
		GUndo->SaveObject(PolygonComponent);
	}

	 PolygonComponent->Points.Insert(SelectionPosition - FVector(0, 0, PolygonComponent->GetOwner()->GetActorLocation().Z), NextIndex);
	 PolygonComponent->Modify();

	 ALocationPolygonTrigger* Actor = CastChecked<ALocationPolygonTrigger>(PolygonComponent->GetOwner());
	 Actor->UpdateCollisionComponents(PolygonComponent->Points);

	 // Sets the viewport gizmo to the new point
	 SelectedKey = NextIndex;

	 GEditor->EndTransaction();
}
#endif

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool FLocationPolygonVisualizer::CanAddKeyToSegment() const
{
	ULocationPolygonComponent* PolygonComponent = GetEditedPolygonComponent();
	if (PolygonComponent == nullptr)
	{
		return false;
	}

	return SelectedSegment != INDEX_NONE;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

TSharedPtr<SWidget> FLocationPolygonVisualizer::GenerateContextMenu() const
{
	FMenuBuilder MenuBuilder(true, PolygonComponentVisualizerActions);

	GenerateContextMenuSections(MenuBuilder);

	TSharedPtr<SWidget> MenuWidget = MenuBuilder.MakeWidget();
	return MenuWidget;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FLocationPolygonVisualizer::GenerateContextMenuSections(FMenuBuilder& InMenuBuilder) const
{
	InMenuBuilder.BeginSection("PolygonPointEdit", LOCTEXT("PolygonPoint", "Polygon Point"));
	{
		if (SelectedSegment != INDEX_NONE)
		{
			InMenuBuilder.AddMenuEntry(FLocationPolygonVisaulizerCommands::Get().AddKey);
		}
		
		if (SelectedKey != INDEX_NONE)
		{
			InMenuBuilder.AddMenuEntry(FLocationPolygonVisaulizerCommands::Get().DeleteKey);
		}
	}
	InMenuBuilder.EndSection();
}

#undef LOCTEXT_NAMESPACE

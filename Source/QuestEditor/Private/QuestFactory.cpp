// Copyright (c) 2020 Gil Engel

#include "QuestFactory.h"
#include "Quest.h"
#include "GenericGraph/GenericGraph.h"

/*-----------------------------------------------------------------------------
	UQuestFactory implementation.
-----------------------------------------------------------------------------*/

UQuestFactory::UQuestFactory()
{
	bCreateNew = true;
	bEditAfterNew = true;
	SupportedClass = UQuest::StaticClass();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

UObject* UQuestFactory::FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn)
{
	return NewObject<UQuest>(InParent, Class, Name, Flags | RF_Transactional);
}

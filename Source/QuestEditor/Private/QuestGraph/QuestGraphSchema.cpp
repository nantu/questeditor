// Copyright (c) 2020 Gil Engel

#include "QuestGraph/QuestGraphSchema.h"
#include "EdGraphSchema_K2.h"
#include "GenericGraph/GenericGraph.h"
#include "GenericGraph/GenericGraphNode_Root.h"
#include "QuestGraph/QuestGraphNode.h"
#include "QuestGraph/QuestGraphNode_Logic.h"
#include "QuestGraphEditorCommands.h"
#include "QuestNode.h"
#include "QuestGraph/QuestGraphNode_Task.h"


const FName UQuestGraphSchema::PC_OptionalExec(TEXT("optional_exec"));

#define LOCTEXT_NAMESPACE "QuestSchema"

FLinearColor UQuestGraphSchema::GetPinTypeColor(const FEdGraphPinType& PinType) const
{
	if (PinType.PinCategory == PC_OptionalExec)
	{
		return FLinearColor::FromSRGBColor(FColor(83, 92, 104, 255));
	}

	return FLinearColor::White;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphSchema::CreateDefaultNodesForGraph(UEdGraph& Graph) const
{
	const int32 RootNodeHeightOffset = -58;

	// Create the result node
	FGraphNodeCreator<UGenericGraphNode_Root> NodeCreator(Graph);
	UGenericGraphNode_Root* ResultRootNode = NodeCreator.CreateNode();

	ResultRootNode->NodePosY = RootNodeHeightOffset;
	NodeCreator.Finalize();
	SetNodeMetaData(ResultRootNode, FNodeMetadata::DefaultGraphNode);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

const FPinConnectionResponse UQuestGraphSchema::CanCreateConnection(const UEdGraphPin* PinA, const UEdGraphPin* PinB) const
{
	const auto& SuperCanCreateConnectionResult = UGenericGraphSchema::CanCreateConnection(PinA, PinB);
	if (SuperCanCreateConnectionResult.Response != CONNECT_RESPONSE_MAKE)
	{
		return SuperCanCreateConnectionResult;
	}

	// Prevent connections between two parallel nodes
	if (Cast<UQuestGraphNode_Logic>(PinA->GetOwningNode()) && Cast<UQuestGraphNode_Logic>(PinB->GetOwningNode()))
	{
		return FPinConnectionResponse(
			CONNECT_RESPONSE_DISALLOW, LOCTEXT("ConnectionBetweenParallel", "Connection between parallel nodes is not allowed"));
	}
	
	return FPinConnectionResponse(CONNECT_RESPONSE_MAKE, TEXT(""));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UQuestGraphSchema::TryCreateConnection(UEdGraphPin* PinA, UEdGraphPin* PinB) const
{
	bool bModified = UGenericGraphSchema::TryCreateConnection(PinA, PinB);

	if (bModified)
	{
		CastChecked<UGenericGraph>(PinA->GetOwningNode()->GetGraph())->GetModel()->CompileQuestNodesFromGraphNodes();
	}

	return bModified;
}

void UQuestGraphSchema::BreakPinLinks(UEdGraphPin& TargetPin, bool bSendsNodeNotifcation) const
{
	UGenericNode* TargetNode = CastChecked<UGenericGraphNode>(TargetPin.GetOwningNode())->GenericNode;

	if (UQuestNode* QuestNode = Cast<UQuestNode>(TargetNode))
	{
		if (TargetPin.PinType.PinCategory == UEdGraphSchema_K2::PC_Boolean && QuestNode->Condition)
		{
			if (TargetPin.GetFName().IsEqual(UQuestGraphNode_Task::EnableConditionPinName))
			{
				QuestNode->Condition = nullptr;				
			}
			else
			{
				QuestNode->FailureCondition = nullptr;
			}			
		}
	}

	Super::BreakPinLinks(TargetPin, bSendsNodeNotifcation);

	// if this would notify the node then we need to compile the QuestGraph
	if (bSendsNodeNotifcation)
	{
		CastChecked<UGenericGraph>(TargetPin.GetOwningNode()->GetGraph())->GetModel()->CompileQuestNodesFromGraphNodes();
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphSchema::GetContextMenuActions(class UToolMenu* Menu, class UGraphNodeContextMenuContext* Context) const
{
	UGenericGraphSchema::GetContextMenuActions(Menu, Context);
	if (Context->Node)
	{
		const UQuestGraphNode* QuestGraphNode = Cast<const UQuestGraphNode>(Context->Node);
		{
			FToolMenuSection& Section =
				Menu->AddSection("QuestGraphSchemaNodeActions", LOCTEXT("NodeActionsMenuHeader", "Node Actions"));
		}
	}

	Super::GetContextMenuActions(Menu, Context);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestGraphSchema::GetPlacableNodes(TArray<TAssetSubclassOf<UObject>>& Nodes, const TArray<UClass*>& IgnoreList) const
{
	Utils::GetAllBlueprintSubclasses(Nodes, UGenericNode::StaticClass(), true, IgnoreList);
	Utils::GetAllNativeSubclasses(Nodes, UGenericNode::StaticClass(), false, IgnoreList);
}

#undef LOCTEXT_NAMESPACE

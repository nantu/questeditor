// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "GenericNode.h"
#include "GenericNodeDynamicDescription.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class QUESTRUNTIME_API UGenericNodeDynamicDescription : public UGenericNode
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintPure)
	virtual FText GetDescription() const
	{
		return Description;
	}

protected:
	//~ Begin UObject Interface
	void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	//~ End UObject Interface
		

public:
	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta = (DisplayName = "Description"))
	FText RawDescription;

	UPROPERTY(BlueprintReadOnly, BlueprintGetter = GetDescription, meta = (MultiLine = "true"))
	FText Description;
};

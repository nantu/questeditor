// Copyright (c) 2020 Gil Engel
#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GenericNode.generated.h"

class UEdGraphNode;

/**
 * 
 */
UCLASS(Abstract, Blueprintable)
class QUESTRUNTIME_API UGenericNode : public UObject
{
	GENERATED_BODY()
	
public:
	virtual TArray<class UGenericNode*> GetSuccessorNodes()
	{
		return ChildNodes;
	}

#if WITH_EDITOR
	/**
	 * Set the entire Child Node array directly, allows GraphNodes to fully control node layout.
	 * Can be overwritten to set up additional parameters that are tied to children.
	 */
	virtual void SetChildNodes(const TArray<UGenericNode*>& InChildNodes);

	virtual int32 GetMinimumChildNodesCount() const;
	virtual int32 GetMaximumChildNodesCount() const;
	
#endif	  // WITH_EDITOR

	/**
	 * Called by the Quest Editor for nodes which allow children.  The default behaviour is to
	 * attach a single connector. Dervied classes can override to eg add multiple connectors.
	 */
	virtual void CreateStartingConnectors(void);
	virtual void InsertChildNode(int32 Index);
	virtual void RemoveChildNode(int32 Index);

	virtual void RemoveParentNode(int32 Index);

	

public:

#if WITH_EDITORONLY_DATA
	/** Node's Graph representation, used to get position. */
	UPROPERTY()
	UEdGraphNode* GraphNode;
#endif

	//UPROPERTY(BlueprintReadOnly)
	UPROPERTY(EditAnywhere)
	TArray<class UGenericNode*> ParentNodes;

	UPROPERTY(BlueprintReadOnly)
	TArray<class UGenericNode*> ChildNodes;
};

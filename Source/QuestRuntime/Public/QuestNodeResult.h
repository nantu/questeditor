// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "QuestNode.h"
#include "QuestNodeParallel.h"
#include "QuestNodeResult.generated.h"

/**
 *
 */
UCLASS(abstract, hidecategories = Object, editinlinenew, MinimalAPI, meta = (DisplayName = "Result"))
class UQuestNodeResult : public UQuestNode
{
	GENERATED_BODY()

public:
	int32 GetMinimumChildNodesCount() const override
	{
		return 0;
	}

	int32 GetMaximumChildNodesCount() const override
	{
		return 0;
	}
};

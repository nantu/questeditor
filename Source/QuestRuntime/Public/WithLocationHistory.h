// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Navigation/LocationHistory.h"
#include "UObject/NoExportTypes.h"
#include "WithLocationHistory.generated.h"

/**
 * 
 */
UINTERFACE(MinimalAPI, Blueprintable)
class UWithLocationHistory : public UInterface
{
	GENERATED_BODY()
	

};

class IWithLocationHistory
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	ULocationHistory* GetLocationHistory();
};
// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Task/QuestNodeTask.h"

#include "QuestNodeKill.generated.h"

/**
 *
 */
UCLASS(hidecategories = Object, editinlinenew, MinimalAPI, meta = (DisplayName = "Kill"))
class UQuestNodeKill : public UQuestNodeTask
{
    GENERATED_UCLASS_BODY()

    UPROPERTY(EditAnywhere, Category = General)
    bool IsSpecificTarget;

    UPROPERTY(EditAnywhere, Category = Generic, meta = (DisplayName = "Target", EditCondition = "!IsSpecificTarget"))
    TSubclassOf<class ACharacter> GenericTarget;

    UPROPERTY(EditAnywhere, Category = Generic, meta = (DisplayName = "Amount", EditCondition = "!IsSpecificTarget"))
    int32 Amount;

    UPROPERTY(EditAnywhere, Category = Target, meta = (DisplayName = "Target", EditCondition = "IsSpecificTarget"))
    TSoftObjectPtr<ACharacter> SpecificTarget;

public:
#if WITH_EDITOR
    virtual FText GetDisplayNameText() const;
#endif
};

// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Task/QuestNodeTask.h"

#include "QuestNodeGather.generated.h"

/**
 *
 */
UCLASS(hidecategories = Object, editinlinenew, MinimalAPI, meta = (DisplayName = "Gather Item(s)"))
class UQuestNodeGather : public UQuestNodeTask
{
    GENERATED_UCLASS_BODY()

public:
    UPROPERTY(EditAnywhere, Category = Quest)
    TSoftObjectPtr<ABaseItem> Item;

    UPROPERTY(EditAnywhere, Category = Quest)
    int32 Amount;

    void ItemAdded_Implementation(const ABaseItem* InItem) override;

    bool IsFullfilled_Implementation() const override;

    //~ Begin UQuestNode Interface
#if WITH_EDITOR
    FText GetDisplayNameText_Implementation() const override;
#endif
    //~ End UQuestNode Interface
};

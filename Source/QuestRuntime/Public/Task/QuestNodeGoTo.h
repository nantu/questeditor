// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "LocationBoxTrigger.h"
#include "Task/QuestNodeTask.h"

#include "QuestNodeGoTo.generated.h"

/**
 *
 */
UCLASS(hidecategories = Object, editinlinenew, MinimalAPI, meta = (DisplayName = "Go To"))
class UQuestNodeGoTo : public UQuestNodeTask
{
    GENERATED_UCLASS_BODY()

public:
    UPROPERTY(EditAnywhere)
	TSoftObjectPtr<ALocationTrigger> Target;

    void LocationVisited_Implementation(const FName& Location) override;

    bool IsFullfilled_Implementation() const override;

    //~ Begin UQuestNode Interface
#if WITH_EDITOR
    virtual FText GetDisplayNameText_Implementation() const;
#endif
    //~ End UQuestNode Interface
};

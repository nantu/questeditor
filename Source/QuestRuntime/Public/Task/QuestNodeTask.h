// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "QuestNode.h"

#include "QuestNodeTask.generated.h"

class ULocationHistory;
class UItemContainerComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FQuestNodeTaskUpdated);

/**
 *
 */
UCLASS(abstract, /*meta = (DisplayName = "Task"),*/ClassGroup = Quest, Category = "Quest", BlueprintType, Blueprintable)
class QUESTRUNTIME_API UQuestNodeTask : public UQuestNode
{
	GENERATED_BODY()

public:
    virtual bool IsValid() const
    {
        return true;
    }

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    bool IsFullfilled() const;

    virtual bool IsFullfilled_Implementation() const
    {
        return false;
    }

    /**
     *	This event is triggered each time the precondition for the task is fulfilled. Precondition normally is another task but can
     *  also depend on some logical formula like the player talked to a npc and posseses the magical sword.
     */
    UFUNCTION(BlueprintNativeEvent, Category = "Flow")
    void TaskEnabled();

    virtual void TaskEnabled_Implementation()
    {
    }

    /**
     *	This event is triggered if the player fails the current active task. Failing condition is special to each task. Use this to
     *  handle custom behaviour based on the task.
     */
    UFUNCTION(BlueprintNativeEvent, Category = "Flow")
    void TaskFailed();

    virtual void TaskFailed_Implementation()
    {
    }

    void LocationVisited_Implementation(const FName& Location) override;
	void ItemAdded_Implementation(const ABaseItem* Item) override;

    void Deactivate() override
    {
        UQuestNode::Deactivate();

        CompletedEvent.Clear();
        NotCompletedEvent.Clear();
    }

    /**
     *	This event is triggered after it is successfully completed. If there is a following task in the quest it will become the
     *  currently active one. Use this to handle custom behaviour based on the task like play a sound, increase character experience
     *  points, level handling etc.
     */
    DECLARE_EVENT(UQuestNodeTask, FTaskCompletedEvent)
    FTaskCompletedEvent& OnTaskCompleted()
    {
        return CompletedEvent;
    }

    DECLARE_EVENT_OneParam(UQuestNodeTask, FTaskFailedEvent, const UQuestNodeTask*)
	FTaskFailedEvent& OnTaskFailed()
	{
		return FailedEvent;
    }

    /**
     *	This event is triggered if the current task is checked for completeness and the check fails. Use this to handle custom
     *behaviour based on the task like play a sound, update internal game state etc.
     */
    DECLARE_EVENT(UQuestNodeTask, FTaskNotCompletedEvent)
    FTaskNotCompletedEvent& OnTaskNotCompleted()
    {
        return NotCompletedEvent;
    }

    DECLARE_EVENT(UQuestNodeTask, FTaskUpdatedEvent)
    FTaskUpdatedEvent& OnTaskUpdated()
    {
        return UpdatedEvent;
    }

#if WITH_EDITOR
    virtual int32 GetMandatoryChildNodes()
    {
        return 2;
    }
#endif

    FText GetDescription() const override;

    UFUNCTION(BlueprintCallable)
    void BroadcastCompleted()
    {
        CompletedEvent.Broadcast();
    }

    UFUNCTION(BlueprintCallable)
	void BroadcastFailed()
	{
		return FailedEvent.Broadcast(this);
    }

    UFUNCTION(BlueprintCallable)
    void BroadcastNotCompleted()
    {
        NotCompletedEvent.Broadcast();
    }

    UFUNCTION(BlueprintCallable)
    void BroadcastUpdated()
    {
        UpdatedEvent.Broadcast();
    }

protected:
    FTaskCompletedEvent CompletedEvent;
	FTaskFailedEvent FailedEvent;
    FTaskNotCompletedEvent NotCompletedEvent;
    FTaskUpdatedEvent UpdatedEvent;
};

// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "GameFramework/Character.h"
#include "Task/QuestNodeTask.h"

#include "QuestNodeEscort.generated.h"

/**
 *
 */
UCLASS(hidecategories = Object, editinlinenew, MinimalAPI, meta = (DisplayName = "Escort"))
class UQuestNodeEscort : public UQuestNodeTask
{
    GENERATED_UCLASS_BODY()

    UPROPERTY(EditAnywhere)
    TSoftObjectPtr<ACharacter> Character;

    UPROPERTY(EditAnywhere)
    TSoftObjectPtr<ATriggerBox> Target;

public:
#if WITH_EDITOR
    virtual FText GetDisplayNameText_Implementation() const;
#endif
};

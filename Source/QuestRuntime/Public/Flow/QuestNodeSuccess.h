// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "QuestNodeResult.h"

#include "QuestNodeSuccess.generated.h"

/**
 *
 */
UCLASS(hidecategories = Object, editinlinenew, MinimalAPI, meta = (DisplayName = "Success"))
class UQuestNodeSuccess : public UQuestNodeResult
{
    GENERATED_UCLASS_BODY()
};

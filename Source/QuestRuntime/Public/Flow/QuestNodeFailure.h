// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "QuestNodeResult.h"

#include "QuestNodeFailure.generated.h"

/**
 *
 */
UCLASS(hidecategories = Object, editinlinenew, MinimalAPI, meta = (DisplayName = "Failure"))
class UQuestNodeFailure : public UQuestNodeResult
{
    GENERATED_UCLASS_BODY()
};

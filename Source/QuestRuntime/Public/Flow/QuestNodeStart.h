// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "QuestNode.h"

#include "QuestNodeStart.generated.h"

/**
 *
 */
UCLASS(hidecategories = Object, editinlinenew, meta = (DisplayName = "Start"))
class QUESTRUNTIME_API UQuestNodeStart : public UQuestNode
{
    GENERATED_BODY()

public:
#if WITH_EDITOR
    FText GetDisplayNameText_Implementation() const override;
#endif
};

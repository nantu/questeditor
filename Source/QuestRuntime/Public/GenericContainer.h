// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#if WITH_EDITOR
#include "IGenericGraphEditor.h"
#include "IGenericGraphModelContainer.h"
#endif

#include "GenericContainer.generated.h"

/**
 * 
 */
UCLASS()
class QUESTRUNTIME_API UGenericContainer : public UObject, public IGenricGraphModelContainer
{
	GENERATED_BODY()

public:
	//~ Begin UObject Interface.
	FString GetDesc() override;
	bool CanBeClusterRoot() const override;
	bool CanBeInCluster() const override;
#if WITH_EDITOR
	void PostInitProperties() override;
	void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	static void AddReferencedObjects(UObject* InThis, FReferenceCollector& Collector);
	//~ End UObject Interface.

	//~ Begin IGraphModelContainer Interface.
	void CreateGraph() override;
	void ClearGraph() override;
	class UEdGraph* GetGraph() override;
	void SetupGenericNode(UGenericNode*, bool bSelectNewNode = true) override;
	//~ End IGraphModelContainer Interface.

	/** Use the EdGraph representation to compile the Generic Graph */
	void CompileQuestNodesFromGraphNodes();

	/** Sets the generic graph editor implementation.* */
	static void SetGenericEditor(TSharedPtr<IGenericGraphEditor> InQuestEditor);

	/** Gets the generic graph editor implementation. */
	static TSharedPtr<IGenericGraphEditor> GetGenericEditor();

#endif	  // WITH_EDITOR

	/** Construct and initialize a node within this Quest */
	template <class T>
	T* ConstructQuestNode(TSubclassOf<UGenericNode> GenericNodeClass = T::StaticClass(), bool bSelectNewNode = true)
	{
		// Set flag to be transactional so it registers with undo system
		T* GenericNode = NewObject<T>(this, GenericNodeClass, NAME_None, RF_Transactional);
#if WITH_EDITOR
		AllNodes.Add(GenericNode);
		SetupGenericNode(GenericNode, bSelectNewNode);
#endif	  // WITH_EDITORONLY_DATA
		return GenericNode;
	}

public:
	UPROPERTY()
	UGenericNode* FirstNode;

#if WITH_EDITORONLY_DATA
	UPROPERTY(EditAnywhere)
	TArray<UGenericNode*> AllNodes;

	UPROPERTY()
	class UEdGraph* GenericGraph;
#endif

#if WITH_EDITOR
protected:
	/** Ptr to interface to quest editor operations. */
	static TSharedPtr<IGenericGraphEditor> GenericEditor;
#endif	  // WITH_EDITOR
};
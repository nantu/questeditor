// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "UObject/Class.h"
#include "UObject/Object.h"
#include "UObject/ObjectMacros.h"
#include "GenericNodeDynamicDescription.h"

#include "Condition/ConditionNode.h"
 	

#include "Math/NumericLimits.h"
#include "Misc/DateTime.h"

#include "QuestNode.generated.h"

class ABaseItem;
class UGenericContainer;

/**
 *
 */
UCLASS(Abstract, Blueprintable, hidecategories = ("NoCompileValidation"))
class QUESTRUNTIME_API UQuestNode : public UGenericNodeDynamicDescription
{
	GENERATED_UCLASS_BODY()
public:

	UFUNCTION(BlueprintCallable)
	void SetIsOptional(bool InIsOptional);

	UFUNCTION()
	void SetInventory(TSoftObjectPtr<UItemContainerComponent> Inventory);

	UFUNCTION()
	void SetLocationHistory(TSoftObjectPtr<ULocationHistory> LocationHistory);

	UFUNCTION(BlueprintNativeEvent, meta = (DisplayName = "IsValid?"))
	bool IsValid() const;

	UFUNCTION(BlueprintCallable)
	bool IsEnabled() const;

	/**
	 *	Called each time a location was visited by the LocationHistory via a multicast dispatcher. Only if the node
	 *  is activated by the parent Quest the function is registered as a listener. After the quest activates
	 *  another node the function will not be longer called until it is reactivated again.
	 */
	UFUNCTION(BlueprintNativeEvent)
	void LocationVisited(const FName& Location);

	/**
	 *
	 *	Called each time an item is added to the inventory via a multicast dispatcher. Only if the node is 
	 *  activated by the parent Quest the function is registered as a listener. After the quest activates
	 *  another node the function will not be longer called until it is reactivated again. 
	 *
	 */
	UFUNCTION(BlueprintNativeEvent)
	void ItemAdded(const ABaseItem* Item);

	//~ Begin UObject Interface
	void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

#if WITH_EDITOR
	UFUNCTION(BlueprintNativeEvent)
	FText GetDisplayNameText() const;

	void PostLoad() override;
	static void AddReferencedObjects(UObject* InThis, FReferenceCollector& Collector);
#endif	  // WITH_EDITOR

	bool CanBeClusterRoot() const override;
	bool CanBeInCluster() const override;

	UFUNCTION(meta = (WorldContext = "WorldContextObject")) 
	virtual void Activate(const UObject* InWorldContextObject)
	{
		WorldContextObject = InWorldContextObject;

		Active = true;
		ActiveSinceTimestamp = FDateTime::Now();

		Enabled = IsEnabled();
	}

	UFUNCTION()
	virtual void Deactivate()
	{
		Active = false;
		ActiveSinceTimestamp = FDateTime::MaxValue();
	}

public:
	UPROPERTY(EditAnywhere, BlueprintSetter = SetIsOptional, Category = "Flow")
	bool IsOptional;

	UPROPERTY(EditAnywhere, Category = "NoCompileValidation")
	UConditionNode* Condition;

	UPROPERTY(EditAnywhere, Category = "NoCompileValidation")
	UConditionNode* FailureCondition;

protected:
	UPROPERTY(VisibleInstanceOnly, Category = "NoCompileValidation")
	TSoftObjectPtr<ULocationHistory> LocationHistory;

	UPROPERTY(VisibleInstanceOnly, Category = "NoCompileValidation")
	TSoftObjectPtr<UItemContainerComponent> Inventory;

	bool Active;
	bool Enabled;

	FDateTime ActiveSinceTimestamp;

	const UObject* WorldContextObject;
};

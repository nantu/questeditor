// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Task/QuestNodeTask.h"

#include "QuestNodeParallel.generated.h"

/**
 *
 */
UCLASS(hidecategories = Object, editinlinenew, MinimalAPI, meta = (DisplayName = "Parallel"))
class UQuestNodeParallel : public UQuestNodeTask
{
    GENERATED_UCLASS_BODY()

public:
    void ItemAdded_Implementation(const ABaseItem* InItem) override;
	void LocationVisited_Implementation(const FName& Location) override;

    //~ Begin UGenericNode interface
	int32 GetMinimumChildNodesCount() const override;
    int32 GetMaximumChildNodesCount() const override;
    //~ End UGenericNode interface

    bool IsValid() const override
    {
        return true;  // Paralle Nodes are always considered to be valid because they are not depending on any variable
    }

    //~ Begin UQuestNode interface
	void Activate(const UObject* WorldContextObject) override;
	void Deactivate() override;
    //~ End UQuestNode interface

    TArray<class UGenericNode*> GetSuccessorNodes() override
    {
        if (ChildNodes.Num() > 0)
        {
            return ChildNodes[0]->ChildNodes;
        }
        else
        {
            return TArray<class UGenericNode*>();
        }
    }

protected:
    FText GetDescription() const override;

private:
    void UpdateTaskStatus();
};

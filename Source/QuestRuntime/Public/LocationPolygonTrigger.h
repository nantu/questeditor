#pragma once


#include "CoreMinimal.h"
#include "Engine/Scene.h"
#include "GameFramework/Actor.h"
#include "Interfaces/Interface_PostProcessVolume.h"
#include "NavAreas/NavArea.h"
#include "Navigation/LocationHistory.h"

#include "Components/BoxComponent.h"

#include "LocationTrigger.h"

#include "LocationPolygonTrigger.generated.h"

class ULocationSplineComponent;
class ULocationPolygonComponent;
// ----------------------------------------------------------------------------------

// Trigger for complex shapes such as villages, cities or regions
UCLASS(HideCategories = (Physics, Collision, Lighting, Navigation, Activation, Cooking, Replication, Input, Actor, HLOD, Mobile, Rendering))
class QUESTRUNTIME_API ALocationPolygonTrigger : public ALocationTrigger
{
	GENERATED_UCLASS_BODY()

public:

#if WITH_EDITOR
	virtual bool Modify(bool bAlwaysMarkDirty) override;

	
#endif	  // WITH_EDITOR



public:

	void UpdateCollisionComponents(const TArray<FVector>& Points);

protected:

#if WITH_EDITOR
	/** Returns whether icon billboard is visible. */
	virtual bool IsIconVisible() const 
	{
		return true;
	}
#endif	  // WITH_EDITOR

	UFUNCTION()
	void OnCollision(AActor* OverlappedActor, AActor* OtherActor);

	UFUNCTION()
	void OnActorBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

private:
	void CreateCollisionSegment(const FVector& Start, const FVector& End, int32 Index);

// ATTRIBUTES

protected:
	UPROPERTY(Category = Location, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	ULocationPolygonComponent* PolygonComp;
};

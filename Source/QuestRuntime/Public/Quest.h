// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Navigation/LocationHistory.h"
#include "ItemContainerComponent.h"
#include "QuestNode.h"
#include "Task/QuestNodeTask.h"
#include "Templates/SubclassOf.h"
#include "UObject/ObjectMacros.h"

#include "GenericContainer.h"

#include "Quest.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FQuestCompletedDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FQuestNotCompletedDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FQuestFailedDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FQuestTaskSwitchedDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FQuestTaskUpdatedDelegate);

/**
 *
 */
UCLASS(ClassGroup = Quest, Category = "Quest", BlueprintType, Blueprintable, DefaultToInstanced, hidecategories = (Object))
class QUESTRUNTIME_API UQuest : public UGenericContainer
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintSetter)
	void SetLocationHistory(TSoftObjectPtr<ULocationHistory> InLocationHistory);

	UFUNCTION(BlueprintSetter)
	void SetInventory(TSoftObjectPtr<UItemContainerComponent> InLocationHistory);

	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"))
	void Activate(const UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable)
	void Deactivate();

	//~ Begin UBlueprint Interface.
	EDataValidationResult IsDataValid(TArray<FText>& ValidationErrors) override
	{
		return EDataValidationResult::Valid;
	}

	UFUNCTION()
	void HandleTaskCompletedEvent();

	UFUNCTION()
	void HandleTaskFailedEvent(const UQuestNodeTask* Task);

	UFUNCTION()
	void HandleTaskUpdatedEvent();

private:
	void SwitchTask(UQuestNodeTask* Task);

public:
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "TextAsset")
	FText Instruction;

	UPROPERTY(BlueprintReadOnly)
	UQuestNode* CurrentTask;

	UPROPERTY(BlueprintSetter = SetLocationHistory)
	TSoftObjectPtr<ULocationHistory> LocationHistory;

	UPROPERTY(BlueprintSetter = SetInventory)
	TSoftObjectPtr<UItemContainerComponent> Inventory;

	UPROPERTY(BlueprintAssignable)
	FQuestCompletedDelegate OnQuestCompleted;

	UPROPERTY(BlueprintAssignable)
	FQuestNotCompletedDelegate OnQuestNotCompleted;

	UPROPERTY(BlueprintAssignable)
	FQuestFailedDelegate OnQuestFailed;

	UPROPERTY(BlueprintAssignable)
	FQuestTaskSwitchedDelegate OnTaskSwitched;

	UPROPERTY(BlueprintAssignable)
	FQuestTaskUpdatedDelegate OnTaskUpdated;

	UPROPERTY(BlueprintReadOnly)
	bool IsActive;

	UPROPERTY()
	const UObject* WorldContextObject;

#if WITH_EDITOR
	UPROPERTY(transient)
	TEnumAsByte<enum EBlueprintStatus> Status;
#endif
};

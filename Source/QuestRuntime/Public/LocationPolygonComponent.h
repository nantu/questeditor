// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Containers/TransArray.h"

#include "LocationPolygonComponent.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent), hideCategories = (Physics, Lighting, Navigation, Activation, Cooking, HLOD, Mobile, "Asset User Data", Rendering, Replication, Input, Actor, Base, Collision, Shape, "Shape|OverlapCapsule")) //hideCategories = (Physics,Collision, Lighting, Navigation, Activation, Cooking, Replication, Input, Actor, HLOD, Mobile))
class QUESTRUNTIME_API ULocationPolygonComponent : public UPrimitiveComponent
{
	GENERATED_UCLASS_BODY()

public:	
	// Sets default values for this component's properties
	//ULocationPolygonComponent(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

#if !UE_BUILD_SHIPPING
	//~ Begin UPrimitiveComponent Interface.
	virtual void PostInitProperties() override;
	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;

	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
	virtual void PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent) override;
	//~ End UPrimitiveComponent Interface

	//~ Begin USceneComponent Interface
	virtual FBoxSphereBounds CalcBounds(const FTransform& LocalToWorld) const override;
	//~ End USceneComponent Interface

#endif

public:
	UPROPERTY(EditAnywhere)
	TArray<FVector> Points;
};

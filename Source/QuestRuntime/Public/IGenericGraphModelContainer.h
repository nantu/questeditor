// Copyright (c) 2020 Gil Engel

#pragma once

class UGenericNode;
class UEdGraph;

class IGenricGraphModelContainer
{
public:
	/** Create the basic quest graph */
	virtual void CreateGraph() = 0;

	/** Clears all nodes from the graph */
	virtual void ClearGraph() = 0;

	/** Get the EdGraph of QuestNodes */
	virtual class UEdGraph* GetGraph() = 0;

	/** Set up EdGraph parts of a GenericNode */
	virtual void SetupGenericNode(UGenericNode*, bool bSelectNewNode = true) = 0;
};
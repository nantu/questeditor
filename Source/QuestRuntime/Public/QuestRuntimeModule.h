// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FQuestRuntimeModule : public IModuleInterface
{
public:
    /** IModuleInterface implementation */
    void StartupModule() override;
    void ShutdownModule() override;
};

#pragma once

#if WITH_EDITOR
class UGenericContainer;
class UGenericContainer;
class UGenericNode;
class UEdGraph;
class IGenricGraphModelContainer;

/** Interface for generic graph interaction with the corresponding editor module. */
class IGenericGraphEditor
{
public:
	virtual ~IGenericGraphEditor()
	{
	}

	/** Called when creating a new quest graph. */
	virtual UEdGraph* CreateNewGenericGraph(UGenericContainer* InModelContainer) = 0;

	/** Sets up a quest node. */
	virtual void SetupModelNode(UEdGraph* GenericGraph, UGenericNode* ModelNode, bool bSelectNewNode) = 0;

	/** Links graph nodes from quest nodes. */
	virtual void LinkGraphNodesFromModelNodes(UGenericContainer* ModelContainer) = 0;

	/** Removes nodes which are nullptr from the quest graph. */
	virtual void RemovenullptrNodes(UGenericContainer* Container) = 0;

	virtual void CompileModelNodesFromGraphNodes(UGenericContainer* ModelContainer) = 0;
};

#endif
// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "LocationTrigger.h"
#include "Navigation/LocationHistory.h"
#include "Components/BoxComponent.h"
#include "LocationBoxTrigger.generated.h"

class UBoxComponent;

UCLASS()
class ULocationBoxComponent : public UBoxComponent
{
	GENERATED_BODY()

public:
	void SetLineThickness(float Thickness);
};

// Trigger for simple shapes such as rooms, houses etc.
UCLASS(Blueprintable, HideCategories = (Tags, Activation, Cooking, Replication, Input, Actor, AssetUserData))
class QUESTRUNTIME_API ALocationBoxTrigger : public ALocationTrigger
{
	GENERATED_UCLASS_BODY()

public:
	void BeginPlay() override;

	UFUNCTION()
	void OnCollision(AActor* OverlappedActor, AActor* OtherActor);

private:
	UPROPERTY(Category = Location, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	ULocationBoxComponent* BoxCollisionComponent;
};

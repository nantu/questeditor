// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "Navigation/LocationHistory.h"
#include "LocationTrigger.generated.h"

// Trigger for simple shapes such as rooms, houses etc.
UCLASS(ClassGroup = Common, abstract, ConversionRoot)
class QUESTRUNTIME_API ALocationTrigger : public AActor
{
	GENERATED_UCLASS_BODY()
		
public:
	void UpdateActorIcon();

	/** Returns CollisionComponent subobject **/
	TArray<UShapeComponent*> GetCollisionComponent() const
	{
		return CollisionComponents;
	}

		/** AActor Interface */
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void PreInitializeComponents() override;

#if WITH_EDITORONLY_DATA
	/** Returns SpriteComponent subobject **/
	UBillboardComponent* GetSpriteComponent() const
	{
		return ActorIcon;
	}
#endif

protected:
	void CreateActorIcon(const FObjectInitializer& ObjectInitializer);

public:
	// Use this to identify your location within quests. For complex triggers you can reuse the identifier on multiple triggers.
	UPROPERTY(EditAnywhere, Category = "Quest")
	FName Identifier;

	static const float LineThickness;
	static const FColor LineColor;

protected:
	UPROPERTY()
	ULocationHistory* LocationHistory;

	/** Shape component used for collision */
	UPROPERTY(Category = TriggerBase, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<UShapeComponent*> CollisionComponents;

#if WITH_EDITORONLY_DATA
	/** Billboard used to see the trigger in the editor */
	UPROPERTY(Transient)
	UBillboardComponent* ActorIcon;
#endif

};

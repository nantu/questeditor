// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "GenericNodeDynamicDescription.h"
#include "ConditionNode.generated.h"

/**
 * 
 */
UCLASS(Abstract, meta = (DisplayName = "Condition"))
class QUESTRUNTIME_API UConditionNode : public UGenericNodeDynamicDescription
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, meta = (WorldContext = "WorldContextObject")) 
	bool IsTrue(const UObject* WorldContextObject);

	virtual bool IsTrue_Implementation(const UObject* WorldContextObject) const
	{
		return true;
	}
};

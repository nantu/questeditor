// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Condition/ConditionNode.h"
#include "UnaryConditionNode.generated.h"

/**
 * 
 */
UCLASS(Abstract, meta = (DisplayName = "Condition"))
class QUESTRUNTIME_API UUnaryConditionNode : public UConditionNode
{
	GENERATED_BODY()	
};

// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"

#include "GenericContainer.h"

#include "Condition.generated.h"

class UConditionNode;

/**
 * 
 */
UCLASS()
class QUESTRUNTIME_API UCondition : public UGenericContainer
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	FText Description;
};

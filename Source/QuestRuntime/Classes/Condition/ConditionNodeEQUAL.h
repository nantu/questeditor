// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "Condition/ConditionNode.h"
#include "ConditionNodeEQUAL.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, meta = (DisplayName = "EQUAL"))
class QUESTRUNTIME_API UConditionNodeEQUAL : public UConditionNode
{
	GENERATED_UCLASS_BODY()

public:
	bool IsTrue_Implementation(const UObject* WorldContextObject) const override
	{
		check(ParentNodes.Num() == 2);

		UConditionNode* InputCondition1 = CastChecked<UConditionNode>(ParentNodes[0]);
		UConditionNode* InputCondition2 = CastChecked<UConditionNode>(ParentNodes[1]);

		return InputCondition1->IsTrue(WorldContextObject) == InputCondition2->IsTrue(WorldContextObject);
	}
};

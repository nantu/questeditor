// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "ConditionNode.h"
#include "ConditionNodeAND.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, meta = (DisplayName = "AND"))
class UConditionNodeAND : public UConditionNode
{
	GENERATED_UCLASS_BODY()
	
public:
	bool IsTrue_Implementation(const UObject* WorldContextObject) const override
	{
		//check(ParentNodes.Num() > 2);

		for (const auto& Input : ParentNodes)
		{
			if (UConditionNode* InputCondition = Cast<UConditionNode>(Input))
			{
				if (!InputCondition->IsTrue(WorldContextObject))
				{
					return false;
				}			
			}
		}

		return true;
	}
};

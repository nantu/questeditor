// Copyright (c) 2020 Gil Engel

#pragma once

#include "CoreMinimal.h"
#include "ConditionNode.h"
#include "ConditionNodeXOR.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, meta = (DisplayName = "XOR"))
class QUESTRUNTIME_API UConditionNodeXOR : public UConditionNode
{
	GENERATED_UCLASS_BODY()

public:
	bool IsTrue_Implementation(const UObject* WorldContextObject) const override
	{
		int32 InputsNum = ParentNodes.Num();

		if (InputsNum > 2)
		{
			return false;
		}

		if (InputsNum == 1)
		{
			return true;
		}

		UConditionNode* InputCondition1 = CastChecked<UConditionNode>(ParentNodes[0]);
		UConditionNode* InputCondition2 = CastChecked<UConditionNode>(ParentNodes[1]);
		return InputCondition1->IsTrue(WorldContextObject) != InputCondition2->IsTrue(WorldContextObject);
	}
};
// Copyright (c) 2020 Gil Engel

#pragma once

#include "BaseItem.h"
#include "Condition/UnaryConditionNode.h"
#include "QuestConditionNode.generated.h"

class ULocationHistory;
class UItemContainerComponent;

/**
 * 
 */
UCLASS(Abstract, hidecategories = Object, meta = (DisplayName = "Condition"))
class QUESTRUNTIME_API UQuestConditionNode : public UUnaryConditionNode
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly)
	TSoftObjectPtr<ULocationHistory> LocationHistory;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly)
	TSoftObjectPtr<UItemContainerComponent> Inventory;

};

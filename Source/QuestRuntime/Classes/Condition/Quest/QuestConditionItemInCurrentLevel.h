// Copyright (c) 2020 Gil Engel

#pragma once

#include "BaseItem.h"
#include "Condition/Quest/QuestConditionNode.h"
#include "QuestConditionItemInCurrentLevel.generated.h"

/**
 * 
 */
UCLASS(hidecategories = Object, editinlinenew, meta = (DisplayName = "Item In Current Level?"))
class QUESTRUNTIME_API UQuestConditionItemInCurrentLevel : public UQuestConditionNode
{
	GENERATED_UCLASS_BODY()

	bool IsTrue_Implementation(const UObject* WorldContextObject) const override;

public:
	UPROPERTY(EditAnywhere, Category = Quest)
	TSubclassOf<ABaseItem> Item;

	UPROPERTY(EditAnywhere, Category = Quest, meta = (ClampMin = "1", UIMin = "1"))
	int32 Amount;
};

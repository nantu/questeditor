// Copyright (c) 2020 Gil Engel

#pragma once

#include "BaseItem.h"
#include "Condition/Quest/QuestConditionNode.h"
#include "QuestConditionNodeHasItem.generated.h"

/**
 * 
 */
UCLASS(hidecategories = Object, editinlinenew, meta = (DisplayName = "Has Item?"))
class QUESTRUNTIME_API UQuestConditionNodeHasItem : public UQuestConditionNode
{
	GENERATED_UCLASS_BODY()

	bool IsTrue_Implementation(const UObject* WorldContextObject) const override;

public:
	UPROPERTY(EditAnywhere, Category = Quest)
	TSoftObjectPtr<ABaseItem> Item;

	UPROPERTY(EditAnywhere, Category = Quest, meta = (ClampMin = "1", UIMin = "1"))
	int32 Amount;
};

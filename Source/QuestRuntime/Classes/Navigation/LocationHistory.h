// Copyright (c) 2020 Gil Engel

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Engine/World.h"
#include "Templates/SharedPointer.h"

#include "LocationHistory.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FLocationVisited, const FName&, Location);

/**
 *
 *	Component that stores all visited locations. It is primarily designed to work with the quest object but can used for other use
 *cases as well. Locations need to be provided in form of a trigger object (box, sphere or custom trigger). For each location a
 *reference to the game object, is stored. Furthermore for each visit the current game time is saved. The component offers functions
 *to extract the number of visits for each location.
 *
 */
UCLASS(ClassGroup = (Custom), Category = "Location History", meta = (BlueprintSpawnableComponent))
class QUESTRUNTIME_API ULocationHistory : public UActorComponent
{
    GENERATED_BODY()

public:
    /**
     *
     *	Call this if your character has visited a location and you want to save it for quest updating. If you've never visited the
     *location before a new entry in the set of visited locations will be created with the visiting count equal to one othewise, if
     *you've already visited the location, the number of visits will be increased by one
     *
     *	@param Location Can be any game object placed in the current level. Make sure that the location is not deleted during
     *gameplay because we use the unique id of the object to identify the location which can be overriden by the engine if the
     *object was deleted
     *
     */
    UFUNCTION(BlueprintCallable)
    void AddVisitOfLocation(const FName& Location);

    /**
     *
     *	Returns if the player has at least visited the given location once.
     *
     *	@param Location Can be any game object placed in the current level. Make sure that the location is not deleted during
     *gameplay because we use the unique id of the object to identify the location which can be overriden by the engine if the
     *object was deleted
     *
     *	@return True if the given location was at least once visited, False otherwise.
     */
    UFUNCTION(BlueprintCallable)
	bool HasVisitedLocation(const FName& Location);

    UFUNCTION(BlueprintCallable)
	bool HasVisitedLocationAfterDateTime(const FName& Location, const FDateTime& DateTime);

    /**
     *
     *	Determines the last visit of a location and returns it.
     *
     *	@param Location Can be any game object placed in the current level. Make sure that the location is not deleted during
     *  gameplay because we use the unique id of the object to identify the location which can be overriden by the engine if the
     *  object was deleted
     *
     *	@return Timestamp in form of a float of the world time when the location was visited. If the location was not yet visited -1
     *  will be returned
     */
    UFUNCTION(BlueprintCallable)
	FDateTime LastTimestampOfLocationVisit(const FName& Location);

    /**
     *
     *	Determines the first visit of a location and returns it.
     *
     *	@param Location Can be any game object placed in the current level. Make sure that the location is not deleted during
     *  gameplay because we use the unique id of the object to identify the location which can be overriden by the engine if the
     *  object was deleted
     *
     *	@return Timestamp in form of a float of the world time when the location was visited. If the location was not yet visited -1
     *  will be returned
     */
    UFUNCTION(BlueprintCallable)
	FDateTime FirstTimestampOfLocationVisit(const FName& Location);

    /**
     *
     *	Removes all stored visits of a given location by deleting it completly of the stored list. In case the location was not yet
     *visit no data will be changed
     *
     *	@param Location Can be any game object placed in the current level. Make sure that the location is not deleted during
     *  gameplay because we use the unique id of the object to identify the location which can be overriden by the engine if the
     *  object was deleted
     *
     */
    UFUNCTION(BLueprintCallable)
    void ClearVisitsOfLocation(const FName& Location);

    /**
     *
     *	Determines the total number of visits of the given location
     *
     *	@param Location Can be any game object placed in the current level. Make sure that the location is not deleted during
     *  gameplay because we use the unique id of the object to identify the location which can be overriden by the engine if the
     *  object was deleted
     *
     *	@return The number of visits, if the location was not visited yet 0 will be returned
     */
    UFUNCTION(BLueprintCallable)
	int32 NumberOfVisits(const FName& Location);

    /**
     *
     *	Multicast delegate that is executed each time after a location was visited
     *
     */
    FLocationVisited OnLocationVisited;

    // Sets default values for this component's properties
    ULocationHistory();

protected:
    // Called when the game starts
    void BeginPlay() override;

public:
    // Called every frame
    void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
    // Contains the visited locations, key in form of int32 is the unique id of the location, float the world time at the time the
    // location was visited
	TMap<FName, TArray<FDateTime>> VisitedLocations;
};

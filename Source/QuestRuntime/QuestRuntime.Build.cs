// Copyright (c) 2020 Gil Engel

using UnrealBuildTool;

public class QuestRuntime : ModuleRules
{
	public QuestRuntime(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		bEnforceIWYU = true;
		
		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
                "QuestRuntime/Private",
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
                "InventoryRuntime",
                "Core",
                "CoreUObject",
                "Engine",
                "UnrealEd",
				"BlueprintGraph" // for UEdGraphgSchema_K2
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
                "Engine",
                "Slate",
                "SlateCore",
                "EditorStyle",
                "UnrealEd",
                "AssetTools",
                "GraphEditor",
                "ApplicationCore",
                "ToolMenus",
				"KismetWidgets"
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}

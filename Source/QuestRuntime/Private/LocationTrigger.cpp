#include "LocationTrigger.h"

const float ALocationTrigger::LineThickness = 3.0f;
const FColor ALocationTrigger::LineColor = FColor(0, 128, 255, 255);

ALocationTrigger::ALocationTrigger(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ALocationTrigger::CreateActorIcon(const FObjectInitializer& ObjectInitializer)
{
#if WITH_EDITORONLY_DATA
	ActorIcon = ObjectInitializer.CreateEditorOnlyDefaultSubobject<UBillboardComponent>(this, TEXT("Sprite"), true);

	if (ActorIcon)
	{
		ConstructorHelpers::FObjectFinderOptional<UTexture2D> Texture(TEXT("/Engine/EditorResources/S_Trigger"));

		ActorIcon->Sprite = Texture.Get();
		ActorIcon->bHiddenInGame = true;
		ActorIcon->SpriteInfo.Category = TEXT("Triggers");
		ActorIcon->SpriteInfo.DisplayName = NSLOCTEXT("SpriteCategory", "Triggers", "Triggers");
		ActorIcon->SetupAttachment(RootComponent);
	}
#endif
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ALocationTrigger::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	//UpdateActorIcon();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ALocationTrigger::UpdateActorIcon()
{
	if (ActorIcon)
	{
		FQuestIconHelper::UpdateSpriteComponent(this, ActorIcon->Sprite);

		// Move the actor icon to the center of the island
		FVector ZOffset(0.0f, 0.0f, 0.0f);
		ActorIcon->SetWorldLocation(RootComponent->Bounds.Origin + ZOffset);
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ALocationTrigger::PreInitializeComponents()
{
	UpdateActorIcon();
}
// Copyright (c) 2020 Gil Engel

#include "QuestRuntimeModule.h"

#include "AssetToolsModule.h"
#include "IAssetTools.h"

#define LOCTEXT_NAMESPACE "FQuestRuntimeModule"

void FQuestRuntimeModule::StartupModule()
{
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void FQuestRuntimeModule::ShutdownModule()
{
    // This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
    // we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FQuestRuntimeModule, QuestRuntime)

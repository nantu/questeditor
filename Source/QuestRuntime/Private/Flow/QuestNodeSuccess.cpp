// Copyright (c) 2020 Gil Engel

#include "Flow/QuestNodeSuccess.h"

/*-----------------------------------------------------------------------------
    UQuestNodeSuccess implementation.
-----------------------------------------------------------------------------*/


UQuestNodeSuccess::UQuestNodeSuccess(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
    RawDescription = FText::FromString("Success");
    Description = RawDescription;
}

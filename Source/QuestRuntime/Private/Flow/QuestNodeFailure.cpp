// Copyright (c) 2020 Gil Engel

#include "Flow/QuestNodeFailure.h"

/*-----------------------------------------------------------------------------
    UQuestNodeFailure implementation.
-----------------------------------------------------------------------------*/

UQuestNodeFailure::UQuestNodeFailure(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
    RawDescription = FText::FromString("Failure");
    Description = RawDescription;
}

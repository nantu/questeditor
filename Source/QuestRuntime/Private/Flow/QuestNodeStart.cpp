// Copyright (c) 2020 Gil Engel

#include "Flow/QuestNodeStart.h"

/*-----------------------------------------------------------------------------
    UQuestNodeStart implementation.
-----------------------------------------------------------------------------*/

FText UQuestNodeStart::GetDisplayNameText_Implementation() const
{
    return FText::FromString("Start");
}

// Copyright (c) 2020 Gil Engel

#include "Task/QuestNodeKill.h"

/*-----------------------------------------------------------------------------
    UQuestNodeKill implementation.
-----------------------------------------------------------------------------*/

UQuestNodeKill::UQuestNodeKill(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
    RawDescription = FText::FromString(" Kill {{Amount}} {{SpecificTarget}} or {{GenericTarget}}");
    Description = RawDescription;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#if WITH_EDITOR
FText UQuestNodeKill::GetDisplayNameText() const
{
    if (IsSpecificTarget)
    {
        const FString SpecificTargetName = SpecificTarget ? SpecificTarget->GetClass()->GetDescription() : "NONE";
        return FText::FromString("Kill {{" + SpecificTargetName + "}}");
    }

    const FString GenericTargetName = GenericTarget ? GenericTarget->GetDescription() : "NONE";
    return FText::FromString("Kill {{" + FString::FromInt(Amount) + "}} {{" + GenericTargetName + "}}.");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#endif

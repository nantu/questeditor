// Copyright (c) 2020 Gil Engel

#include "Task/QuestNodeEscort.h"

/*-----------------------------------------------------------------------------
    UQuestNodeEscort implementation.
-----------------------------------------------------------------------------*/

UQuestNodeEscort::UQuestNodeEscort(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
    RawDescription = FText::FromString("Escort {{Character}} To {{Target}}");
    Description = RawDescription;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#if WITH_EDITOR

FText UQuestNodeEscort::GetDisplayNameText_Implementation() const
{
    const FString CharacterName = Character ? Character->GetName() : "NONE";
    const FString TargetName = Target ? Target->GetName() : "NONE";

    return FText::FromString("Escort {{" + CharacterName + "}} To {{" + TargetName + "}}");
}
#endif

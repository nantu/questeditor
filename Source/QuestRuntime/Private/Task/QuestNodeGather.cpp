// Copyright (c) 2020 Gil Engel

#include "Task/QuestNodeGather.h"
#include "ItemContainerComponent.h"

#define LOCTEXT_NAMESPACE "QuestGather"

/*-----------------------------------------------------------------------------
    UQuestNodeKill implementation.
-----------------------------------------------------------------------------*/

UQuestNodeGather::UQuestNodeGather(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
    RawDescription = FText::FromString(" Gather {{Amount}} {{Item}}");
    Description = RawDescription;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#if WITH_EDITOR
FText UQuestNodeGather::GetDisplayNameText_Implementation() const
{
    return RawDescription;
}
#endif

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestNodeGather::ItemAdded_Implementation(const ABaseItem* InItem)
{
	UQuestNodeTask::ItemAdded_Implementation(InItem);

    if (InItem->GetUniqueID() == Item->GetUniqueID())
    {
        CompletedEvent.Broadcast();
        return;
    }

    NotCompletedEvent.Broadcast();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UQuestNodeGather::IsFullfilled_Implementation() const
{
	if (Condition && !Condition->IsTrue(WorldContextObject))
	{
		return false;
	}

    if (Inventory)
    {
        return Inventory->ContainsItem(Item.Get(), Amount);
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#undef LOCTEXT_NAMESPACE

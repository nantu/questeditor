// Copyright (c) 2020 Gil Engel

#include "Task/QuestNodeGoTo.h"
#include "Navigation/LocationHistory.h"

#include "Engine/Engine.h"

/*-----------------------------------------------------------------------------
    UQuestNodeGoTo implementation.
-----------------------------------------------------------------------------*/

UQuestNodeGoTo::UQuestNodeGoTo(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    RawDescription = FText::FromString("Go to {{Target}}");
    Description = RawDescription;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#if WITH_EDITOR
void UQuestNodeGoTo::LocationVisited_Implementation(const FName& Location)
{
    // Call super to handle failure condition checking
	UQuestNodeTask::LocationVisited_Implementation(Location);
    
    if (!Active)
    {
        return;
    }

    if (Target->Identifier == Location)
    {
        CompletedEvent.Broadcast();
        return;
    }

    NotCompletedEvent.Broadcast();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UQuestNodeGoTo::IsFullfilled_Implementation() const
{
	if (Condition && !Condition->IsTrue(WorldContextObject))
	{
		return false;
    }

    const FName Identifier = Target.Get()->Identifier;
    if (LocationHistory && LocationHistory->HasVisitedLocation(Identifier))
    {
		FDateTime DateTime = LocationHistory->LastTimestampOfLocationVisit(Identifier);
		UE_LOG(LogTemp, Warning, TEXT("ActiveTS: %s \t LocationTS: %s"), *(ActiveSinceTimestamp.ToIso8601()), *(DateTime.ToIso8601()));

		return DateTime > ActiveSinceTimestamp;
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText UQuestNodeGoTo::GetDisplayNameText_Implementation() const
{
    const FString TargetName = Target ? Target->GetName() : "NONE";

    return FText::FromString("Go to {{" + TargetName + "}}.");
}
#endif

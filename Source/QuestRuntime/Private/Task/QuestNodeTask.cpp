// Copyright (c) 2020 Gil Engel

#include "Task/QuestNodeTask.h"

/*-----------------------------------------------------------------------------
    UQuestNodeTask implementation.
-----------------------------------------------------------------------------*/

void UQuestNodeTask::LocationVisited_Implementation(const FName& Location)
{
	if (!Enabled)
	{
		return;
	}

	if (Condition && !Condition->IsTrue(WorldContextObject))
	{
		Enabled = false;
		BroadcastUpdated();
	}

	if (FailureCondition && FailureCondition->IsTrue(WorldContextObject))
	{
		BroadcastFailed();
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestNodeTask::ItemAdded_Implementation(const ABaseItem* Item)
{
	if (!Active || !Enabled)
	{
		return;
	}

	if (Condition && Condition->IsTrue(WorldContextObject))
	{
		Enabled = false;
		BroadcastUpdated();
	}

	if (FailureCondition && FailureCondition->IsTrue(WorldContextObject))
	{
		BroadcastFailed();
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText UQuestNodeTask::GetDescription() const
{
	if (Condition && !Condition->IsTrue(WorldContextObject))
	{
		return FText::FromString("REQ NOT MET: Quest not available\n");
	}

	return UQuestNode::GetDescription();
}

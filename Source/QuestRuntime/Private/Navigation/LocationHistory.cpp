// Copyright (c) 2020 Gil Engel

#include "Navigation/LocationHistory.h"
#include "Engine/Engine.h"

/*-----------------------------------------------------------------------------
    ULocationHistory implementation.
-----------------------------------------------------------------------------*/

void ULocationHistory::AddVisitOfLocation(const FName& Location)
{
	TArray<FDateTime>& VisitTimestamps = VisitedLocations.FindOrAdd(Location);
	VisitTimestamps.Emplace(FDateTime::Now());

    if (OnLocationVisited.IsBound())
    {
        OnLocationVisited.Broadcast(Location);
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool ULocationHistory::HasVisitedLocation(const FName& Location)
{
    return VisitedLocations.Find(Location) != nullptr;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool ULocationHistory::HasVisitedLocationAfterDateTime(const FName& Location, const FDateTime& DateTime)
{
	if (HasVisitedLocation(Location))
	{
		return false;
    }

	FDateTime LastTimestamp = LastTimestampOfLocationVisit(Location);

	return DateTime > LastTimestamp;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FDateTime ULocationHistory::LastTimestampOfLocationVisit(const FName& Location)
{
    return VisitedLocations.Find(Location)->Last();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FDateTime ULocationHistory::FirstTimestampOfLocationVisit(const FName& Location)
{
    return VisitedLocations.Find(Location)->GetData()[0];
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ULocationHistory::ClearVisitsOfLocation(const FName& Location)
{
    VisitedLocations.Remove(Location);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

int32 ULocationHistory::NumberOfVisits(const FName& Location)
{
    const auto LocationHistory = VisitedLocations.Find(Location);

    return (LocationHistory != nullptr) ? LocationHistory->Num() : 0;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

// Sets default values for this component's properties
ULocationHistory::ULocationHistory()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = true;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

// Called when the game starts
void ULocationHistory::BeginPlay()
{
    Super::BeginPlay();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

// Called every frame
void ULocationHistory::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

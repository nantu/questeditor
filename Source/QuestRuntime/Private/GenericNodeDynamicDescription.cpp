// Copyright (c) 2020 Gil Engel

#include "GenericNodeDynamicDescription.h"

/*-----------------------------------------------------------------------------
	UGenericNodeDynamicDescription implementation.
-----------------------------------------------------------------------------*/

void UGenericNodeDynamicDescription::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	FString DescriptionStr = RawDescription.ToString();
	for (TFieldIterator<FProperty> Property(GetClass()); Property; ++Property)
	{
		FString RawKey = "{{" + Property->GetName() + "}}";
		FString Key;

		// Replace all keys in the description that represents a property
		if (RawDescription.ToString().Find(RawKey) != -1)
		{
			if (FBoolProperty* BoolProperty = Cast<FBoolProperty>(*Property))
			{
				Key = BoolProperty->GetPropertyValue(this) ? "true" : "false";
			}
			else if (FNumericProperty* NumericProperty = Cast<FNumericProperty>(*Property))
			{
				void* ptr = NumericProperty->ContainerPtrToValuePtr<void>(this);

				// Make sure to differentiate between int and float
				if (NumericProperty->IsInteger())
				{
					Key = FString::FromInt(NumericProperty->GetSignedIntPropertyValue(ptr));
				}
				else if (NumericProperty->IsFloatingPoint())
				{
					Key = FString::SanitizeFloat(NumericProperty->GetFloatingPointPropertyValue(ptr));
				}
			}
			else if (FSoftObjectProperty* SoftObjectProperty = Cast<FSoftObjectProperty>(*Property))
			{
				UObject* Object = SoftObjectProperty->GetObjectPropertyValue_InContainer(this);

				Key = (Object != nullptr) ? Object->GetName() : "";
			}

			if (!Key.IsEmpty())
			{
				Key = "{{" + Key + "}}";

				DescriptionStr.ReplaceInline(*RawKey, *Key);
			}
		}
	}

	Description = FText::FromString(DescriptionStr);
}

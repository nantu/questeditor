// Copyright (c) 2020 Gil Engel

#include "LocationPolygonTrigger.h"
#include "QuestIconHelper.h"
#include "WithLocationHistory.h"

#if WITH_EDITOR
#include "Components/BillboardComponent.h"
#endif

// ----------------------------------------------------------------------------------

ALocationPolygonTrigger::ALocationPolygonTrigger(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PolygonComp = ObjectInitializer.CreateDefaultSubobject<ULocationPolygonComponent>(this, TEXT("LocationPolygon"));
	RootComponent = PolygonComp;

	CreateActorIcon(ObjectInitializer);
	UpdateActorIcon();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ALocationPolygonTrigger::CreateCollisionSegment(const FVector& Start, const FVector& End, int32 Index)
{
	auto SegmentVector = (End - Start);
	auto Center = Start + SegmentVector.GetSafeNormal() * SegmentVector.Size() / 2.0f;

	auto RotationVector = FVector(SegmentVector.X, SegmentVector.Y, 0.0);
	FQuat BetweenQuat = FQuat::FindBetweenVectors(FVector(-1.0, 0.0, 0.0), RotationVector.GetSafeNormal());

	
	UBoxComponent* CollisionComponent = NewObject<UBoxComponent>(this, UBoxComponent::StaticClass(), *FString("CollisionComp_" + FString::FromInt(Index))); //TEXT("CollisionComp_" + Index));
	check(CollisionComponent);

	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	// CollisionComponent->SetCollisionResponseToChannel()
	CollisionComponent->SetBoxExtent(FVector(SegmentVector.Size() / 2.0, 20.0, 250.0));
	CollisionComponent->SetWorldRotation(BetweenQuat);
	CollisionComponent->SetCollisionProfileName(TEXT("Trigger"));

	CollisionComponent->bAutoActivate = true;

	CollisionComponent->SetRelativeLocation(FVector(Center.X, Center.Y, 250));
	CollisionComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform, NAME_None);

	// CollisionComponent->OnComponentCreated();
	CollisionComponent->RegisterComponent();
	AddInstanceComponent(CollisionComponent);

	CollisionComponents.Add(CollisionComponent);

	FScriptDelegate Delegate;
	Delegate.BindUFunction(this, "OnCollision");
	CollisionComponent->OnComponentBeginOverlap.AddUnique(Delegate);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ALocationPolygonTrigger::UpdateCollisionComponents(const TArray<FVector>& Points)
{
	CollisionComponents.Empty();
	for(const auto& BoxComponent : this->GetComponentsByClass(UBoxComponent::StaticClass())) 
	{
		this->RemoveInstanceComponent(BoxComponent);
	}	

	auto ActorLocation = GetActorLocation();
	const int32 PointsNum = Points.Num();
	for (int32 Step = 1; Step < PointsNum; Step++)
	{
		CreateCollisionSegment(Points[Step - 1], Points[Step], Step - 1);
	}
	CreateCollisionSegment(Points[PointsNum - 1], Points[0], PointsNum);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ALocationPolygonTrigger::OnActorBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("HAUSMEISTER 2"));
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ALocationPolygonTrigger::OnCollision(AActor* OverlappedActor, AActor* OtherActor)
{
	if (OtherActor->GetClass()->ImplementsInterface(UWithLocationHistory::StaticClass()))
	{
		ULocationHistory* A = IWithLocationHistory::Execute_GetLocationHistory(OtherActor);
		A->AddVisitOfLocation(Identifier);
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#if WITH_EDITOR

bool ALocationPolygonTrigger::Modify(bool bAlwaysMarkDirty) 
{
	return Super::Modify(bAlwaysMarkDirty);
}

#endif

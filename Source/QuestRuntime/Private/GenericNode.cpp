// Copyright (c) 2020 Gil Engel

#include "GenericNode.h"

/*-----------------------------------------------------------------------------
	UGenericNode implementation.
-----------------------------------------------------------------------------*/

void UGenericNode::CreateStartingConnectors()
{
	int32 ConnectorsToMake = FMath::Max(GetMinimumChildNodesCount(), GetMaximumChildNodesCount());
	while (ConnectorsToMake > 0)
	{
		InsertChildNode(ChildNodes.Num());
		--ConnectorsToMake;
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericNode::InsertChildNode(int32 Index)
{
	check(Index >= 0 && Index <= ChildNodes.Num());
	if (GetMaximumChildNodesCount() > ChildNodes.Num())
	{
		ChildNodes.InsertZeroed(Index);
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericNode::RemoveChildNode(int32 Index)
{
	check(Index >= 0 && Index < ChildNodes.Num());
	if (ChildNodes.Num() > GetMaximumChildNodesCount())
	{
		ChildNodes.RemoveAt(Index);
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericNode::RemoveParentNode(int32 Index)
{
	//check(Index >= 0 && Index < ParentNodes.Num());
	if (ParentNodes.Num() > 0)
	{
		ParentNodes.RemoveAt(Index);
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#if WITH_EDITOR
void UGenericNode::SetChildNodes(const TArray<UGenericNode*>& InChildNodes)
{
	ChildNodes = InChildNodes;
}
#endif

//----------------------------------------------------------------------------------------------------------------------------------------------------

int32 UGenericNode::GetMinimumChildNodesCount() const
{
	return 1;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

int32 UGenericNode::GetMaximumChildNodesCount() const
{
	return 1;
}

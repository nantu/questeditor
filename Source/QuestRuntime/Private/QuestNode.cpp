// Copyright (c) 2020 Gil Engel

#include "QuestNode.h"

#if WITH_EDITOR
#include "EdGraphSchema_K2.h"
#include "Logging/TokenizedMessage.h"
#endif

#include "EngineUtils.h"
#include "Misc/App.h"
#include "Quest.h"
#include "Condition/Condition.h"
#include "Condition/Quest/QuestConditionNode.h"

/*-----------------------------------------------------------------------------
    UQuestNode implementation.
-----------------------------------------------------------------------------*/


namespace Util
{
const void FindAllParentQuestConditionNodes(const UGenericNode* Node, TArray<UQuestConditionNode*>& FoundNodes)
{
	for (const auto& ParentNode : Node->ParentNodes)
	{
		if (UQuestConditionNode* ConditionNodeParent = Cast<UQuestConditionNode>(ParentNode))
		{
			FoundNodes.Add(ConditionNodeParent);
		}

        if (ParentNode)
		{
			FindAllParentQuestConditionNodes(ParentNode, FoundNodes);
        }	
	}
}
};	  // namespace Util

//----------------------------------------------------------------------------------------------------------------------------------------------------

UQuestNode::UQuestNode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	//Condition = ObjectInitializer.CreateEditorOnlyDefaultSubobject<UCondition>(this, UCondition::StaticClass()->GetFName());
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UQuestNode::CanBeClusterRoot() const
{
    return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UQuestNode::CanBeInCluster() const
{
    return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#if WITH_EDITOR
void UQuestNode::AddReferencedObjects(UObject* InThis, FReferenceCollector& Collector)
{
    UQuestNode* This = CastChecked<UQuestNode>(InThis);

    Collector.AddReferencedObject(This->GraphNode, This);

    Super::AddReferencedObjects(InThis, Collector);
}
#endif	  // WITH_EDITOR

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestNode::SetIsOptional(bool InIsOptional)
{
	bool HasMandatoryParent = ParentNodes.ContainsByPredicate([](const UGenericNode* ParentNode) -> bool {
		if (const auto& QuestNode = Cast<UQuestNode>(ParentNode))
		{
			return !QuestNode->IsOptional;
		}

        return false;
	});
      
    if (InIsOptional && HasMandatoryParent)
    {
        return;
    }

    IsOptional = InIsOptional;

    auto ValidChildNodesArray =
		ChildNodes.FilterByPredicate([](const UGenericNode* ChildNode) -> bool { return ChildNode != nullptr; });


    for (const auto& ValidChildNode : ValidChildNodesArray)
    {
        CastChecked<UQuestNode>(ValidChildNode)->SetIsOptional(InIsOptional);
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UQuestNode::IsValid_Implementation() const
{
    for (TFieldIterator<FProperty> Property(GetClass()); Property; ++Property)
    {
        auto ErrorFunction = [this, &Property]() {
            GraphNode->ErrorMsg = "Property " + Property->GetDisplayNameText().ToString() + " is not set.";
            GraphNode->bHasCompilerMessage = true;
            GraphNode->ErrorType = EMessageSeverity::Error;
        };

        const FString& CategoryName = Property->GetMetaData("Category");
        if (CategoryName == "NoCompileValidation")
        {
            continue;
        }

        if (FSoftObjectProperty* SoftObjectProperty = Cast<FSoftObjectProperty>(*Property))
        {
            UObject* Object = SoftObjectProperty->GetObjectPropertyValue_InContainer(this);
            if (Object == nullptr)
            {
                ErrorFunction();
                return false;
            }
        }
        else if (FWeakObjectProperty* WeakObjectProperty = Cast<FWeakObjectProperty>(*Property))
        {
            UObject* Object = WeakObjectProperty->GetObjectPropertyValue_InContainer(this);
            if (Object == nullptr)
            {
                ErrorFunction();
                return false;
            }
        }
        else if (FLazyObjectProperty* LazyObjectProperty = Cast<FLazyObjectProperty>(*Property))
        {
            UObject* Object = LazyObjectProperty->GetObjectPropertyValue_InContainer(this);
            if (Object == nullptr)
            {
                ErrorFunction();
                return false;
            }
        }
        else if (FObjectProperty* ObjectProperty = Cast<FObjectProperty>(*Property))
        {
            UObject* Object = ObjectProperty->GetObjectPropertyValue_InContainer(this);
            if (Object == nullptr)
            {
                ErrorFunction();
                return false;
            }
        }
    }

    GraphNode->bHasCompilerMessage = false;

    return true;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestNode::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	UGenericNodeDynamicDescription::PostEditChangeProperty(PropertyChangedEvent);

    MarkPackageDirty();

	if (PropertyChangedEvent.Property == nullptr)
	{
		return;
	}

	if (PropertyChangedEvent.GetPropertyName() == "IsOptional")
	{
		GraphNode->PostEditChangeProperty(PropertyChangedEvent);

		// Propagate the change to all valid children in order to update the editor
		TArray<UGenericNode*> ValidChildNodesArray =
			ChildNodes.FilterByPredicate([](const UGenericNode* ChildNode) -> bool { return ChildNode != nullptr; });

		for (const auto& ValidChildNode : ValidChildNodesArray)
		{
			if (UQuestNode* QuestNode = Cast<UQuestNode>(ValidChildNode))
			{
				QuestNode->SetIsOptional(IsOptional);
				ValidChildNode->PostEditChangeProperty(PropertyChangedEvent);
			}
		}

		return;
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#if WITH_EDITOR
bool UQuestNode::IsEnabled() const
{
	if (FailureCondition && FailureCondition->IsTrue(WorldContextObject))
	{
		return false;
	}

    if (Condition && Condition->IsTrue(WorldContextObject))
	{
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestNode::PostLoad()
{
    Super::PostLoad();
    // Make sure quest nodes are transactional (so they work with undo system)
    SetFlags(RF_Transactional);
}
#endif

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestNode::SetLocationHistory(TSoftObjectPtr<ULocationHistory> InLocationHistory)
{
	LocationHistory = InLocationHistory;

    if (Condition)
	{
		TArray<UQuestConditionNode*> ParentConditionNodes;
		Util::FindAllParentQuestConditionNodes(Condition, ParentConditionNodes);

		for (UQuestConditionNode* Node : ParentConditionNodes)
		{
			Node->LocationHistory = InLocationHistory;
		}

        if (UQuestConditionNode* ConditionNode = Cast<UQuestConditionNode>(Condition))
		{
			ConditionNode->LocationHistory = InLocationHistory;
        }
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestNode::SetInventory(TSoftObjectPtr<UItemContainerComponent> InInventory)
{
	Inventory = InInventory;
        
    if (Condition)
	{
		TArray<UQuestConditionNode*> ParentConditionNodes;
		Util::FindAllParentQuestConditionNodes(Condition, ParentConditionNodes);

        for (UQuestConditionNode* Node : ParentConditionNodes)
		{
			Node->Inventory = InInventory;
        }

		if (UQuestConditionNode* ConditionNode = Cast<UQuestConditionNode>(Condition))
		{
			ConditionNode->Inventory = InInventory;
		}
    }

    if (FailureCondition)
	{
		TArray<UQuestConditionNode*> ParentConditionNodes;
		Util::FindAllParentQuestConditionNodes(FailureCondition, ParentConditionNodes);

		for (UQuestConditionNode* Node : ParentConditionNodes)
		{
			Node->Inventory = InInventory;
		}

		if (UQuestConditionNode* ConditionNode = Cast<UQuestConditionNode>(FailureCondition))
		{
			ConditionNode->Inventory = InInventory;
		}
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------------


void UQuestNode::LocationVisited_Implementation(const FName& Location)
{
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestNode::ItemAdded_Implementation(const ABaseItem* Item)
{
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText UQuestNode::GetDisplayNameText_Implementation() const
{
    return GetClass()->GetDisplayNameText();
}

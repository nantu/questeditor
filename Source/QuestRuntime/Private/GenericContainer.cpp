// Copyright (c) 2020 Gil Engel

#include "GenericContainer.h"
#include "EdGraph/EdGraphSchema.h"

/*-----------------------------------------------------------------------------
	GenericContainer implementation.
-----------------------------------------------------------------------------*/

#if WITH_EDITOR
TSharedPtr<IGenericGraphEditor> UGenericContainer::GenericEditor = nullptr;
#endif	  // WITH_EDITOR

#if WITH_EDITOR
void UGenericContainer::PostInitProperties()
{
	Super::PostInitProperties();
	if (!HasAnyFlags(RF_ClassDefaultObject | RF_NeedLoad))
	{
		CreateGraph();
	}
}

void UGenericContainer::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	UE_LOG(LogTemp, Warning, TEXT("Property Changed"));

	UObject::PostEditChangeProperty(PropertyChangedEvent);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericContainer::AddReferencedObjects(UObject* InThis, FReferenceCollector& Collector)
{
	UGenericContainer* This = CastChecked<UGenericContainer>(InThis);

	Collector.AddReferencedObject(This->GenericGraph, This);

	Super::AddReferencedObjects(InThis, Collector);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

UEdGraph* UGenericContainer::GetGraph()
{
	return GenericGraph;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericContainer::CreateGraph()
{
	if (GenericGraph == nullptr)
	{
		GenericGraph = UGenericContainer::GetGenericEditor()->CreateNewGenericGraph(this);
		GenericGraph->bAllowDeletion = false;

		// Give the schema a chance to fill out any required nodes (like the results node)
		const UEdGraphSchema* Schema = GenericGraph->GetSchema();
		Schema->CreateDefaultNodesForGraph(*GenericGraph);
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericContainer::ClearGraph()
{
	if (GenericGraph)
	{
		GenericGraph->Nodes.Empty();
		// Give the schema a chance to fill out any required nodes (like the results node)
		const UEdGraphSchema* Schema = GenericGraph->GetSchema();
		Schema->CreateDefaultNodesForGraph(*GenericGraph);
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FString UGenericContainer::GetDesc()
{
	FString Description = TEXT("");

	return Description;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UGenericContainer::CanBeClusterRoot() const
{
	return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UGenericContainer::CanBeInCluster() const
{
	return false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericContainer::CompileQuestNodesFromGraphNodes()
{
	UGenericContainer::GetGenericEditor()->CompileModelNodesFromGraphNodes(this);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericContainer::SetupGenericNode(UGenericNode* InGenericNode, bool bSelectNewNode)
{
	// Create the graph node
	check(InGenericNode->GraphNode == nullptr);

	UGenericContainer::GetGenericEditor()->SetupModelNode(GenericGraph, InGenericNode, bSelectNewNode);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UGenericContainer::SetGenericEditor(TSharedPtr<IGenericGraphEditor> InGenericEditor)
{
	GenericEditor = InGenericEditor;
}

TSharedPtr<IGenericGraphEditor> UGenericContainer::GetGenericEditor()
{
	return GenericEditor;
}
#endif	  // WITH_EDITOR
// Fill out your copyright notice in the Description page of Project Settings.


#include "LocationPolygonComponent.h"
#include "LocationPolygonTrigger.h"
#include "LocationBoxTrigger.h"

#include "Components/BoxComponent.h"
#include "UObject/ConstructorHelpers.h"

#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
ULocationPolygonComponent::ULocationPolygonComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Add default spline points
	Points.Empty();

	const auto Size = 2048;
	Points.Emplace(-Size, -Size, 0);
	Points.Emplace(-Size,  Size, 0);
	Points.Emplace( Size,  Size, 0);
	Points.Emplace( Size, -Size, 0);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ULocationPolygonComponent::PostInitProperties()
{
	Super::PostInitProperties();
		
	ALocationPolygonTrigger* Owner = Cast<ALocationPolygonTrigger>(GetOwner());
	if (Owner && Owner->GetWorld())
	{
		Owner->UpdateCollisionComponents(Points);
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

// Called when the game starts
void ULocationPolygonComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

// Called every frame
void ULocationPolygonComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ULocationPolygonComponent::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	if (IsSelected())
	{
		return;
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ULocationPolygonComponent::PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent)
{
	if (IsSelected())
	{
		return;
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

#if !UE_BUILD_SHIPPING
FPrimitiveSceneProxy* ULocationPolygonComponent::CreateSceneProxy()
{	
	/** Represents a UBoxComponent to the scene manager. */
	class FBoxSceneProxy final : public FPrimitiveSceneProxy
	{
	public:
		SIZE_T GetTypeHash() const override
		{
			static size_t UniquePointer;
			return reinterpret_cast<size_t>(&UniquePointer);
		}

		FBoxSceneProxy(const ULocationPolygonComponent* InComponent)
			: FPrimitiveSceneProxy(InComponent)
			, bDrawOnlyIfSelected(false)
		{
			bWillEverBeLit = false;

			Component = InComponent;
		}

        void DrawPolygonSegment(
			FPrimitiveDrawInterface* PDI, int32 StartIndex, int32 EndIndex) const
		{
			check(StartIndex >= 0 && StartIndex < Component->Points.Num());
			check(EndIndex >= 0 && EndIndex < Component->Points.Num());

			float Height = 500.0f;
			FVector ZOffset(0.0f, 0.0f, Height);

			FVector Center = Component->GetOwner()->GetActorLocation();
			FVector StartBottom = Center + Component->Points[StartIndex];
			FVector StartTop = StartBottom + ZOffset;

			FVector EndBottom = Center + Component->Points[EndIndex];
			FVector EndTop = EndBottom + ZOffset;

			auto DrawSegment = [PDI](FVector Start, FVector End) {
				PDI->DrawLine(Start, End, ALocationTrigger::LineColor, SDPG_World, ALocationTrigger::LineThickness, SDPG_Foreground, true);
			};

			DrawSegment(StartTop, EndTop);
			DrawSegment(StartBottom, EndBottom);

			DrawSegment(StartBottom, StartTop);
		}

		virtual void GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily,
			uint32 VisibilityMap, FMeshElementCollector& Collector) const override
		{
			QUICK_SCOPE_CYCLE_COUNTER(STAT_BoxSceneProxy_GetDynamicMeshElements);
			
			if (IsSelected())
			{
				return;
			}

			const FMatrix& LocalToWorld = GetLocalToWorld();

			for (int32 ViewIndex = 0; ViewIndex < Views.Num(); ViewIndex++)
			{
				if (VisibilityMap & (1 << ViewIndex))
				{
					const FSceneView* View = Views[ViewIndex];

					// Taking into account the min and maximum drawing distance
					const float DistanceSqr = (View->ViewMatrices.GetViewOrigin() - LocalToWorld.GetOrigin()).SizeSquared();
					if (DistanceSqr < FMath::Square(GetMinDrawDistance()) || DistanceSqr > FMath::Square(GetMaxDrawDistance()))
					{
						continue;
					}

					FPrimitiveDrawInterface* PDI = Collector.GetPDI(ViewIndex);

					// Draw the lines
					for (int32 Step = 1; Step < Component->Points.Num(); Step++)
					{
						DrawPolygonSegment(PDI, Step - 1, Step);
					}
					DrawPolygonSegment(PDI, Component->Points.Num() - 1, 0);
				}
			}
		}

		virtual FPrimitiveViewRelevance GetViewRelevance(const FSceneView* View) const override
		{
			const bool bVisibleForSelection = !bDrawOnlyIfSelected || IsSelected();
			const bool bVisibleForShowFlags = true;	   // @TODO

			// Should we draw this because collision drawing is enabled, and we have collision
			const bool bShowForCollision = View->Family->EngineShowFlags.Collision && IsCollisionEnabled();

			FPrimitiveViewRelevance Result;
			Result.bDrawRelevance = (IsShown(View) && bVisibleForSelection && bVisibleForShowFlags) || bShowForCollision;
			Result.bDynamicRelevance = true;
			Result.bShadowRelevance = IsShadowCast(View);
			Result.bEditorPrimitiveRelevance = UseEditorCompositing(View);
			return Result;
		}
		virtual uint32 GetMemoryFootprint(void) const override
		{
			return (sizeof(*this) + GetAllocatedSize());
		}
		uint32 GetAllocatedSize(void) const
		{
			return (FPrimitiveSceneProxy::GetAllocatedSize());
		}

	private:
		const uint32 bDrawOnlyIfSelected : 1;
		const ULocationPolygonComponent* Component;
	};

	return new FBoxSceneProxy(this);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FBoxSphereBounds ULocationPolygonComponent::CalcBounds(const FTransform& LocalToWorld) const
{
	FVector Min(WORLD_MAX);
	FVector Max(-WORLD_MAX);
	return FBoxSphereBounds(FBox(Min, Max).TransformBy(LocalToWorld));
}
#endif
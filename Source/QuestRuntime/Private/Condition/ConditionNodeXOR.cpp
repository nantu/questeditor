// Copyright (c) 2020 Gil Engel

#include "Condition/ConditionNodeXOR.h"

/*-----------------------------------------------------------------------------
	UConditionNodeXOR implementation.
-----------------------------------------------------------------------------*/

UConditionNodeXOR::UConditionNodeXOR(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	RawDescription = FText::FromString("XOR");
	Description = RawDescription;
}

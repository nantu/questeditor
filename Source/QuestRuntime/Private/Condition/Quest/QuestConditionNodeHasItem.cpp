// Copyright (c) 2020 Gil Engel

#include "Condition/Quest/QuestConditionNodeHasItem.h"

/*-----------------------------------------------------------------------------
	UQuestNodeResult implementation.
-----------------------------------------------------------------------------*/

UQuestConditionNodeHasItem::UQuestConditionNodeHasItem(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	RawDescription = FText::FromString("Has {{Amount}} {{Item}}?");
	Description = RawDescription;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UQuestConditionNodeHasItem::IsTrue_Implementation(const UObject* WorldContextObject) const
{
	if (!Inventory)
	{
		return false;
	}

	return Inventory->ContainsItem(Item.Get(), Amount);
}

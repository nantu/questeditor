// Copyright (c) 2020 Gil Engel

#include "Condition/Quest/QuestConditionItemInCurrentLevel.h"

/*-----------------------------------------------------------------------------
	QuestConditionItemInCurrentLevel implementation.
-----------------------------------------------------------------------------*/

UQuestConditionItemInCurrentLevel::UQuestConditionItemInCurrentLevel(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	RawDescription = FText::FromString("Has {{Amount}} {{Item}}?");
	Description = RawDescription;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

bool UQuestConditionItemInCurrentLevel::IsTrue_Implementation(const UObject* WorldContextObject) const
{
	UWorld* World = WorldContextObject->GetWorld();
	if (World == nullptr)
	{
		return false;
	}

	ULevel* Level = World->GetCurrentLevel();
	if (Level == nullptr)
	{
		return false;
	}

	TArray<AActor*> FilteredActors = Level->Actors.FilterByPredicate([this](const AActor* Actor) { return Actor->IsA(Item); });

	return FilteredActors.Num() == Amount;
}

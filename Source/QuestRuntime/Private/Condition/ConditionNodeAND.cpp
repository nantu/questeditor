// Copyright (c) 2020 Gil Engel

#include "Condition/ConditionNodeAND.h"

/*-----------------------------------------------------------------------------
	UQuestConditionNodeAnd implementation.
-----------------------------------------------------------------------------*/

UConditionNodeAND::UConditionNodeAND(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	RawDescription = FText::FromString("AND");
	Description = RawDescription;
}

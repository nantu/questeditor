// Copyright (c) 2020 Gil Engel

#include "Condition/ConditionNodeEQUAL.h"

/*-----------------------------------------------------------------------------
	UConditionNodeEQUAL implementation.
-----------------------------------------------------------------------------*/

UConditionNodeEQUAL::UConditionNodeEQUAL(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	RawDescription = FText::FromString("==");
	Description = RawDescription;
}

// Copyright (c) 2020 Gil Engel

#include "Condition/ConditionNodeOR.h"

/*-----------------------------------------------------------------------------
	UConditionNodeOR implementation.
-----------------------------------------------------------------------------*/

UConditionNodeOR::UConditionNodeOR(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	RawDescription = FText::FromString("OR");
	Description = RawDescription;
}

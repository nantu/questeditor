// Copyright (c) 2020 Gil Engel

#include "Condition/ConditionNodeUNEQUAL.h"

/*-----------------------------------------------------------------------------
	UConditionNodeEQUAL implementation.
-----------------------------------------------------------------------------*/

UConditionNodeUNEQUAL::UConditionNodeUNEQUAL(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	RawDescription = FText::FromString("!=");
	Description = RawDescription;
}

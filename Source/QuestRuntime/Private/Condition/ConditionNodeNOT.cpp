// Copyright (c) 2020 Gil Engel

#include "Condition/ConditionNodeNOT.h"

/*-----------------------------------------------------------------------------
	UConditionNodeNot implementation.
-----------------------------------------------------------------------------*/

UConditionNodeNOT::UConditionNodeNOT(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	RawDescription = FText::FromString("NOT");
	Description = RawDescription;
}

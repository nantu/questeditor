// Copyright (c) 2020 Gil Engel

#include "QuestNodeParallel.h"

/*-----------------------------------------------------------------------------
    UQuestNodeParallel implementation.
-----------------------------------------------------------------------------*/

UQuestNodeParallel::UQuestNodeParallel(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
    RawDescription = FText::FromString("Parallel");
    Description = RawDescription;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestNodeParallel::ItemAdded_Implementation(const ABaseItem* InItem)
{
    UpdateTaskStatus();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestNodeParallel::LocationVisited_Implementation(const FName& Location)
{
    UpdateTaskStatus();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

int32 UQuestNodeParallel::GetMinimumChildNodesCount() const
{
	return 2;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

int32 UQuestNodeParallel::GetMaximumChildNodesCount() const
{
	return 10;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestNodeParallel::UpdateTaskStatus()
{
	const TArray<UGenericNode*> OptionalNodes =
        ChildNodes.FilterByPredicate([](const UGenericNode* InQuestNode) { 

        if (const auto& QuestNode = Cast<UQuestNode>(InQuestNode))
		{
			return QuestNode->IsOptional; 
        }
        
        return false;
    });
    const bool HasOptionalNodes = OptionalNodes.Num() > 0;

    const TArray<UGenericNode*> MandatoryNodes = ChildNodes.FilterByPredicate([](const UGenericNode* InQuestNode) {
		if (const auto& QuestNode = Cast<UQuestNode>(InQuestNode))
		{			
			return !QuestNode->IsOptional;
		}

		return false;
	});
    const bool HasMandatoryNodes = MandatoryNodes.Num() > 0;

    const bool HasUnfulfilledMandatoryNode = MandatoryNodes.ContainsByPredicate([this](UGenericNode* InQuestNode) {
        if (UQuestNodeTask* Task = Cast<UQuestNodeTask>(InQuestNode))
        {
			if (Task->Condition && !Task->Condition->IsTrue(WorldContextObject))
			{
				return true;
            }

            return (!Task->IsOptional && !Task->IsFullfilled());
        }

        return false;
    });

    bool HasFulfilledOptionalNode = OptionalNodes.ContainsByPredicate([](UGenericNode* InQuestNode) {
        if (UQuestNodeTask* Task = Cast<UQuestNodeTask>(InQuestNode))
        {
            return (Task->IsOptional && Task->IsFullfilled());
        }

        return false;
    });

    if (!HasMandatoryNodes && !HasOptionalNodes)
    {
        CompletedEvent.Broadcast();
        return;
    }

    if (HasMandatoryNodes)
    {
        if (!HasUnfulfilledMandatoryNode)
        {
            CompletedEvent.Broadcast();
            return;
        }
        else
        {
            UpdatedEvent.Broadcast();
            NotCompletedEvent.Broadcast();
            return;
        }
    }

    if (HasOptionalNodes)
    {
        if (HasFulfilledOptionalNode)
        {
            CompletedEvent.Broadcast();
            return;
        }
        else
        {
            UpdatedEvent.Broadcast();
            NotCompletedEvent.Broadcast();
            return;
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestNodeParallel::Activate(const UObject* InWorldContextObject)
{
	WorldContextObject = InWorldContextObject;
	UQuestNode::Activate(WorldContextObject);

    for (const auto& Child : ChildNodes)
	{
		if (UQuestNodeTask* Task = Cast<UQuestNodeTask>(Child))
		{
			Task->Activate(WorldContextObject);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuestNodeParallel::Deactivate()
{
	UQuestNode::Deactivate();

	for (const auto& Child : ChildNodes)
	{
		if (UQuestNodeTask* Task = Cast<UQuestNodeTask>(Child))
		{
			Task->Deactivate();
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

FText UQuestNodeParallel::GetDescription() const
{
    FString AccumulatedDescription;
    for (const auto& Child : ChildNodes)
    {
        if (UQuestNodeTask* Task = Cast<UQuestNodeTask>(Child))
        {
			if (Task->Condition && !Task->Condition->IsTrue(WorldContextObject))
			{
				AccumulatedDescription.Append("REQ NOT MET: Quest not available\n");
				continue;
            }

            AccumulatedDescription.Append(Task->IsFullfilled() ? "[X] " : "[ ] ");
            AccumulatedDescription.Append(Cast<UQuestNode>(Child)->GetDescription().ToString());
            AccumulatedDescription.Append("\n");
        }
    }

    return FText::FromString(AccumulatedDescription);
}

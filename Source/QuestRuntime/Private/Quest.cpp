// Copyright (c) 2020 Gil Engel

#include "Quest.h"


 #include "Editor.h"

#include "Engine/Engine.h"
#include "EngineDefines.h"
#include "EngineGlobals.h"
#include "EngineUtils.h"
#include "Misc/App.h"
#include "Misc/CoreDelegates.h"
#include "UObject/UObjectIterator.h"
#include "Task/QuestNodeTask.h"
#include "Task/QuestNodeGoTo.h"
#include "Flow/QuestNodeSuccess.h"
#include "Flow/QuestNodeFailure.h"
#include "EditorFramework/ThumbnailInfo.h"

/*-----------------------------------------------------------------------------
    UQuest implementation.
-----------------------------------------------------------------------------*/

void UQuest::HandleTaskCompletedEvent()
{
    if (LocationHistory != nullptr)
    {
        LocationHistory->OnLocationVisited.Remove(CurrentTask, "LocationVisited");
    }

    if (Inventory != nullptr)
    {
        Inventory->OnItemAdded.Remove(CurrentTask, "ItemAdded");
    }

    int32 NotEmptyChildNodes = 0;
    for (const auto& ChildNode : CurrentTask->GetSuccessorNodes())
    {
        if (ChildNode != nullptr)
        {
            NotEmptyChildNodes++;
        }
    }

    // Corner case if this is the last node in the quest and no Successfull or Failure node have been added
    // set the quest do fulfilled but with a "warning"
    if (NotEmptyChildNodes == 0)
    {
        Deactivate();
        SwitchTask(nullptr);
        OnQuestCompleted.Broadcast();
        return;
    }

    for (const auto& ChildNode : CurrentTask->GetSuccessorNodes())
    {
        if (ChildNode != nullptr)
        {
            if (UQuestNodeSuccess* Success = Cast<UQuestNodeSuccess>(ChildNode))
            {
                Deactivate();
                SwitchTask(nullptr);
                OnQuestCompleted.Broadcast();
            }
            else if (UQuestNodeFailure* Failure = Cast<UQuestNodeFailure>(ChildNode))
            {
                Deactivate();
                SwitchTask(nullptr);
                OnQuestFailed.Broadcast();
            }
            else if (UQuestNodeTask* Task = Cast<UQuestNodeTask>(ChildNode))
            {
                SwitchTask(Task);
            }
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuest::HandleTaskFailedEvent(const UQuestNodeTask* Task)
{
	OnQuestFailed.Broadcast();

    Deactivate();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuest::HandleTaskUpdatedEvent()
{
    OnTaskUpdated.Broadcast();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuest::SwitchTask(UQuestNodeTask* Task)
{
    if (CurrentTask)
    {
		CurrentTask->Deactivate();

        if (UQuestNodeTask* QuestNodeTask = Cast<UQuestNodeTask>(CurrentTask))
		{
			QuestNodeTask->OnTaskCompleted().RemoveAll(this);
			QuestNodeTask->OnTaskFailed().RemoveAll(this);
			QuestNodeTask->OnTaskUpdated().RemoveAll(this);
        }
        
        CurrentTask->Deactivate();
        LocationHistory->OnLocationVisited.Remove(CurrentTask, "LocationVisited");

        Inventory->OnItemAdded.Remove(CurrentTask, "ItemAdded");
    }

    CurrentTask = Task;

    if (!Task)
    {
        return;
    }

    // Activate the task as the new task
    Task->Activate(WorldContextObject);
    Task->OnTaskCompleted().AddUObject(this, &UQuest::HandleTaskCompletedEvent);
	Task->OnTaskFailed().AddUObject(this, &UQuest::HandleTaskFailedEvent);
	Task->OnTaskUpdated().AddUObject(this, &UQuest::HandleTaskUpdatedEvent);

    if (LocationHistory != nullptr)
    {
        LocationHistory->OnLocationVisited.AddDynamic(CurrentTask, &UQuestNode::LocationVisited);
    }

    if (Inventory != nullptr)
    {
        Inventory->OnItemAdded.AddDynamic(CurrentTask, &UQuestNode::ItemAdded);
    }

    OnTaskSwitched.Broadcast();
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuest::SetLocationHistory(TSoftObjectPtr<ULocationHistory> InLocationHistory)
{
    LocationHistory = InLocationHistory;

    for (auto& Node : AllNodes)
    {
        if (UQuestNodeTask* Task = Cast<UQuestNodeTask>(Node))
        {
            Task->SetLocationHistory(InLocationHistory);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuest::SetInventory(TSoftObjectPtr<UItemContainerComponent> InInventory)
{
    Inventory = InInventory;

    for (auto& Node : AllNodes)
    {
        if (UQuestNodeTask* Task = Cast<UQuestNodeTask>(Node))
        {
            Task->SetInventory(Inventory);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuest::Activate(const UObject* InWorldContextObject)
{
	WorldContextObject = InWorldContextObject;

    IsActive = true;

    if (!FirstNode)
    {
        return;
    }

    if (UQuestNodeTask* Task = Cast<UQuestNodeTask>(FirstNode))
    {
		SwitchTask(Task);
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void UQuest::Deactivate()
{
    // Removes any listener of the current task and sets it to a nullptr
    SwitchTask(nullptr);

    IsActive = false;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

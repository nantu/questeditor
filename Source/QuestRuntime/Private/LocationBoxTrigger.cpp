// Copyright (c) 2020 Gil Engel

#include "LocationBoxTrigger.h"
#include "WithLocationHistory.h"

/*-----------------------------------------------------------------------------
	ULocationBoxComponent implementation.
-----------------------------------------------------------------------------*/

void ULocationBoxComponent::SetLineThickness(float Thickness) 
{
	this->LineThickness = Thickness;
}

/*-----------------------------------------------------------------------------
	ALocationBoxTrigger implementation.
-----------------------------------------------------------------------------*/

ALocationBoxTrigger::ALocationBoxTrigger(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	BoxCollisionComponent = 
		ObjectInitializer.CreateDefaultSubobject<ULocationBoxComponent>(this, TEXT("CollisionComp"));
	static const FName TriggerCollisionProfileName(TEXT("Trigger"));

	BoxCollisionComponent->ShapeColor = ALocationTrigger::LineColor;
	BoxCollisionComponent->InitBoxExtent(FVector(80.0f, 80.0f, 80.0f));
	BoxCollisionComponent->SetCollisionProfileName(TriggerCollisionProfileName);
	BoxCollisionComponent->SetLineThickness(ALocationTrigger::LineThickness);

	RootComponent = BoxCollisionComponent;

	CreateActorIcon(ObjectInitializer);
	UpdateActorIcon();
}


void ALocationBoxTrigger::BeginPlay() 
{
    FScriptDelegate Delegate;
	Delegate.BindUFunction(this, "OnCollision");	
	this->OnActorBeginOverlap.AddUnique(Delegate);
}

//----------------------------------------------------------------------------------------------------------------------------------------------------

void ALocationBoxTrigger::OnCollision(AActor* OverlappedActor, AActor* OtherActor)
{
	if (OtherActor->GetClass()->ImplementsInterface(UWithLocationHistory::StaticClass()))
	{
		ULocationHistory* A = IWithLocationHistory::Execute_GetLocationHistory(OtherActor);
		A->AddVisitOfLocation(Identifier);
	}
}

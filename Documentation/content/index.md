---
title: "Parallel"
date: 2020-05-26T10:09:15+02:00
draft: true
---

# Complex tasks

Quester offers you the ability to create tasks for your quest that the player needs to perform in parallel. 
To create a parallel task you use the `Parallel` node.

![Example image](/static/image.png)

Per default this node has only one output pin. To add more use the **+** button. You can add as many
outputs as you like. After you created the outputs add the tasks that you want the player to perform.

For these subtasks you have two options: **optional** and **mandatory**. There are three scenarios:

1. Only mandatory (sub)tasks: By declaring all subtasks as mandatory the player needs to fulfill all 
the tasks to continue with the quest. The order is not relevant. Currently there is no feature
to define a fulfillment order for these tasks.
2. Only optional (sub)tasks: By declaring all subtasks as optional the player needs to fulfill only
one task to continue with the quest. Use this if you want to show the player different alternatives
for the quest to fulfill
3. Mix of optional and mandatory tasks: Of course you can mix optional and mandatory tasks at the 
same time. By doing so the player needs only to fulfill the mandatory tasks. All optinal tasks
are not needed to continue with the quest. Use this if you want to offer the player an optional
side quest that may affect later tasks.